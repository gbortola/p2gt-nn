library IEEE;
use IEEE.STD_LOGIC_1164.all;

package NN_scaler_pkg is
    
    type data_coll_scaled is array(11 downto 0) of std_logic_vector(15 downto 0);
    
end package NN_scaler_pkg;

package body NN_scaler_pkg is
    
end package body NN_scaler_pkg;
