library ieee;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.object_defs.all;
use work.NN_scaler_pkg.all;

entity framer is
    port(
        -- 40MHz domain
        lhc_clk     : in std_logic;
        lhc_rst     : in std_logic; -- res 40 MHz
        -- 480Mhz domain
        clk_algo         : in std_logic;
        rst_algo         : in std_logic;
        -- P2GT objects
        objects_valid       : in  std_logic;
        objects             : in  t_obj_array(26 - 1 downto 0);
        -- output
        input_V_o        : out std_logic_vector(271 downto 0);
        input_V_vld_o    : out std_logic;
        -- debug
        ele_pt           : out data_coll_scaled;
        ele_eta          : out data_coll_scaled;
        ele_phi          : out data_coll_scaled;
        
        mu_pt            : out data_coll_scaled;
        mu_eta           : out data_coll_scaled;
        mu_phi           : out data_coll_scaled;
        
        jet_pt           : out data_coll_scaled;
        jet_eta          : out data_coll_scaled;
        jet_phi          : out data_coll_scaled;
        
        MET_pt           : out std_logic_vector(15 downto 0);
        MET_phi          : out std_logic_vector(15 downto 0)

    );
end framer;

architecture arch of framer is

    -- 1 for preadd
    -- 1 for multiplication
    -- 1 for register the output
    constant SCALER_LATENCY : integer := 4;
    --SLR 0
    constant GTT_PROMPT_JETS_SLOT       : natural := 0;
    constant GTT_DISPLACED_JETS_SLOT    : natural := 1;
    constant GTT_HT_MISS_PROMPT_SLOT    : natural := 2;
    constant GTT_HT_MISS_DISPLACED_SLOT : natural := 3;
    constant GTT_ET_MISS_SLOT           : natural := 4;
    constant GTT_TAUS_SLOT              : natural := 5;
    constant GTT_PHI_CANDIDATE_SLOT     : natural := 6;
    constant GTT_RHO_CANDIDATE_SLOT     : natural := 7;
    constant GTT_BS_CANDIDATE_SLOT      : natural := 8;
    constant GTT_PRIM_VERT_SLOT         : natural := 9;

    --SLR 1
    constant CL2_JET_SLOT      : natural := 10;
    constant CL2_HT_MISS_SLOT  : natural := 11;
    constant CL2_ET_MISS_SLOT  : natural := 12;
    constant CL2_TAU_SLOT      : natural := 13;
    constant CL2_ELECTRON_SLOT : natural := 14;
    constant CL2_PHOTON_SLOT   : natural := 15;

    --SLR 2
    constant GCT_NON_ISO_EG_SLOT : natural := 16;
    constant GCT_ISO_EG_SLOT     : natural := 17;
    constant GCT_JETS_SLOT       : natural := 18;
    constant GCT_TAUS_SLOT       : natural := 19;
    constant GCT_HT_MISS_SLOT    : natural := 20;
    constant GCT_ET_MISS_SLOT    : natural := 21;


    constant GMT_SA_PROMPT_SLOT    : natural := 22;
    constant GMT_SA_DISPLACED_SLOT : natural := 23;
    constant GMT_TK_MUON_SLOT      : natural := 24;
    constant GMT_TOPO_SLOT         : natural := 25;



    signal ele_obj  : t_obj;
    signal mu_obj   : t_obj;
    signal jet_obj  : t_obj;
    signal MET_obj  : t_obj;

    signal ele_vld_in : std_logic;
    signal mu_vld_in  : std_logic;
    signal jet_vld_in : std_logic;
    signal MET_vld_in : std_logic;


    -- scaler values
    signal ele_pt_mean   : signed(16 downto 0);
    signal ele_pt_factor : signed(15 downto 0);
    signal mu_pt_mean    : signed(16 downto 0);
    signal mu_pt_factor  : signed(15 downto 0);
    signal jet_pt_mean   : signed(16 downto 0);
    signal jet_pt_factor : signed(15 downto 0);
    signal MET_pt_mean   : signed(16 downto 0);
    signal MET_pt_factor : signed(15 downto 0);

    signal ele_eta_mean   : signed(13 downto 0);
    signal ele_eta_factor : signed(15 downto 0);
    signal mu_eta_mean    : signed(13 downto 0);
    signal mu_eta_factor  : signed(15 downto 0);
    signal jet_eta_mean   : signed(13 downto 0);
    signal jet_eta_factor : signed(15 downto 0);

    signal ele_phi_mean   : signed(12 downto 0);
    signal ele_phi_factor : signed(15 downto 0);
    signal mu_phi_mean    : signed(12 downto 0);
    signal mu_phi_factor  : signed(15 downto 0);
    signal jet_phi_mean   : signed(12 downto 0);
    signal jet_phi_factor : signed(15 downto 0);
    signal MET_phi_mean   : signed(12 downto 0);
    signal MET_phi_factor : signed(15 downto 0);

    -- scaled values
    signal ele_pt_scaled   : signed(15 downto 0);
    signal mu_pt_scaled    : signed(15 downto 0);
    signal jet_pt_scaled   : signed(15 downto 0);
    signal MET_pt_scaled   : signed(15 downto 0);
    signal ele_eta_scaled  : signed(15 downto 0);
    signal mu_eta_scaled   : signed(15 downto 0);
    signal jet_eta_scaled  : signed(15 downto 0);
    signal ele_phi_scaled  : signed(15 downto 0);
    signal mu_phi_scaled   : signed(15 downto 0);
    signal jet_phi_scaled  : signed(15 downto 0);
    signal MET_phi_scaled  : signed(15 downto 0);

    signal ele_pt_scaled_vld   : std_logic;
    signal mu_pt_scaled_vld    : std_logic;
    signal jet_pt_scaled_vld   : std_logic;
    signal MET_pt_scaled_vld   : std_logic;
    signal ele_eta_scaled_vld  : std_logic;
    signal mu_eta_scaled_vld   : std_logic;
    signal jet_eta_scaled_vld  : std_logic;
    signal ele_phi_scaled_vld  : std_logic;
    signal mu_phi_scaled_vld   : std_logic;
    signal jet_phi_scaled_vld  : std_logic;
    signal MET_phi_scaled_vld  : std_logic;

    signal input_V_int     : std_logic_vector(271 downto 0);
    signal input_V_vld_int : std_logic;


    type frame_cntr_arr_t is array(SCALER_LATENCY downto 0) of unsigned (3 downto 0);
    --counter for frame mux: 0 to 11
    signal frame_cntr  , frame_cntr_o  : unsigned (3 downto 0); --counter for frame mux: 0 to 11
    signal frame_cntr_arr              : frame_cntr_arr_t; --delayed version of frame counter

begin

    -- frame counter
    frame_counter: process (clk_algo, objects_valid)
    begin
        --if (lhc_rst = '1') then
        --    frame_cntr <= "0000";      -- async. res
        --elsif (rising_edge(clk_algo)) then
        --    if (frame_cntr = "1011") then -- 11
        --        frame_cntr <= "0000";
        --    else
        --        frame_cntr <= frame_cntr + 1;
        --    end if;
        --end if;
        -- async reset 
        if objects_valid = '0' then
            frame_cntr <= "0000";
        elsif (rising_edge(clk_algo)) then
            if frame_cntr = "1011" then
                frame_cntr <= "0000";
            else
                frame_cntr <= frame_cntr + 1;
            end if;
        end if;
    end process frame_counter;
    
    frame_cntr_arr(0) <= frame_cntr;
    
    frame_counter_delay_p: process (clk_algo)
    begin
        if (rising_edge(clk_algo)) then
            frame_cntr_arr(SCALER_LATENCY downto 1) <= frame_cntr_arr(SCALER_LATENCY - 1 downto 0);
        end if;
    end process frame_counter_delay_p;
    
    frame_cntr_o <= frame_cntr_arr(SCALER_LATENCY);


    demux_p : process (objects_valid, objects)
    begin
        --if rising_edge(clk_algo) then
        if objects_valid = '1' then
            if objects(CL2_ELECTRON_SLOT).valid = '1' then
                ele_obj <= objects(CL2_ELECTRON_SLOT);
            else
                ele_obj <= NULL_OBJECT;
            end if;
            if objects(GMT_TK_MUON_SLOT).valid = '1' then
                mu_obj <= objects(GMT_TK_MUON_SLOT);
            else
                mu_obj <= NULL_OBJECT;
            end if;
            if objects(CL2_JET_SLOT).valid = '1' then
                jet_obj <= objects(CL2_JET_SLOT);
            else
                jet_obj <= NULL_OBJECT;
            end if;
            if objects(CL2_ET_MISS_SLOT).valid = '1' then
                MET_obj <= objects(CL2_ET_MISS_SLOT);
            else
                MET_obj <= NULL_OBJECT;
            end if;
        else
            mu_obj  <= NULL_OBJECT;
            ele_obj <= NULL_OBJECT;
            jet_obj <= NULL_OBJECT;
            MET_obj <= NULL_OBJECT;
        end if;
        --end if;
    end process;

    -- scaler has 2 clock cycles of latency
    scaler_p : process (frame_cntr)
    begin
        case (frame_cntr) is
            when "0000" =>
                ele_vld_in <= '1';
                mu_vld_in  <= '1';
                jet_vld_in <= '1';
                MET_vld_in <= '1';

                -- pt mean and factor values
                ele_pt_mean   <= to_signed(208  ,17);
                ele_pt_factor <= to_signed(1720 ,16);
                mu_pt_mean    <= to_signed(85   ,17);
                mu_pt_factor  <= to_signed(8281 ,16);
                jet_pt_mean   <= to_signed(1282 ,17);
                jet_pt_factor <= to_signed(641  ,16);
                MET_pt_mean   <= to_signed(1046 ,17);
                MET_pt_factor <= to_signed(1058 ,16);

                -- eta mean and factor values
                ele_eta_mean   <= to_signed(-5  ,14);
                ele_eta_factor <= to_signed(58  ,16);
                mu_eta_mean    <= to_signed(726 ,14);
                mu_eta_factor  <= to_signed(15  ,16);
                jet_eta_mean   <= to_signed(-8  ,14);
                jet_eta_factor <= to_signed(17  ,16);
                
                ele_phi_mean   <= to_signed(-69 ,13);
                ele_phi_factor <= to_signed(30  ,16);
                mu_phi_mean    <= to_signed(195 ,13);
                mu_phi_factor  <= to_signed(15  ,16);
                jet_phi_mean   <= to_signed(2   ,13);
                jet_phi_factor <= to_signed(14  ,16);
                MET_phi_mean   <= to_signed(-1  ,13);
                MET_phi_factor <= to_signed(13  ,16);
            when "0001" =>
                ele_vld_in <= '1';
                mu_vld_in  <= '1';
                jet_vld_in <= '1';
                MET_vld_in <= '0';

                -- pt mean and factor values
                ele_pt_mean   <= to_signed(128  ,17);
                ele_pt_factor <= to_signed(2127 ,16);
                mu_pt_mean    <= to_signed(54   ,17);
                mu_pt_factor  <= to_signed(12803,16);
                jet_pt_mean   <= to_signed(784  ,17);
                jet_pt_factor <= to_signed(944  ,16);

                -- eta mean and factor values
                ele_eta_mean   <= to_signed(1   ,14);
                ele_eta_factor <= to_signed(63  ,16);
                mu_eta_mean    <= to_signed(76  ,14);
                mu_eta_factor  <= to_signed(16  ,16);
                jet_eta_mean   <= to_signed(4   ,14);
                jet_eta_factor <= to_signed(19  ,16);
                
                ele_phi_mean   <= to_signed(17  ,13);
                ele_phi_factor <= to_signed(39  ,16);
                mu_phi_mean    <= to_signed(26  ,13);
                mu_phi_factor  <= to_signed(17  ,16);
                jet_phi_mean   <= to_signed(13  ,13);
                jet_phi_factor <= to_signed(15  ,16);
            when "0010" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '1';
                MET_vld_in <= '0';

                -- pt mean and factor values
                jet_pt_mean   <= to_signed(486  ,17);
                jet_pt_factor <= to_signed(1339 ,16);

                -- eta mean and factor values
                jet_eta_mean   <= to_signed(-1  ,14);
                jet_eta_factor <= to_signed(22  ,16);
                
                jet_phi_mean   <= to_signed(0   ,13);
                jet_phi_factor <= to_signed(17  ,16);
            when "0011" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '1';
                MET_vld_in <= '0';

                -- pt mean and factor values
                jet_pt_mean   <= to_signed(300  ,17);
                jet_pt_factor <= to_signed(1836 ,16);

                -- eta mean and factor values
                jet_eta_mean   <= to_signed(0   ,14);
                jet_eta_factor <= to_signed(26  ,16);
                
                jet_phi_mean   <= to_signed(-1  ,13);
                jet_phi_factor <= to_signed(21  ,16);
            when "0100" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when "0101" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when "0110" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when "0111" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when "1000" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when "1001" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when "1010" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when "1011" =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
            when others =>
                ele_vld_in <= '0';
                mu_vld_in  <= '0';
                jet_vld_in <= '0';
                MET_vld_in <= '0';
        end case;
    end process;

    -- scaler has 2 clock cycles of latency
    load_data_p : process (frame_cntr_o, MET_pt_scaled, ele_eta_scaled, ele_pt_scaled, jet_eta_scaled,
        jet_pt_scaled, mu_eta_scaled, mu_pt_scaled)
    begin
        --if rising_edge(clk_algo) then
        case (frame_cntr_o) is
            when "0000" =>
                -- e0
                input_V_int((16*1)-1 downto 16*0)   <= std_logic_vector(ele_pt_scaled);  -- input 0
                input_V_int((16*2)-1 downto 16*1)   <= std_logic_vector(ele_eta_scaled); -- input 1
                -- jet0
                input_V_int((16*5)-1 downto 16*4)   <= std_logic_vector(jet_pt_scaled);  -- input 4   
                input_V_int((16*6)-1 downto 16*5)   <= std_logic_vector(jet_eta_scaled); -- input 5
                -- MET
                input_V_int((16*13)-1 downto 16*12) <= std_logic_vector(MET_pt_scaled);  -- input 12
                -- mu0
                input_V_int((16*14)-1 downto 16*13) <= std_logic_vector(mu_eta_scaled);  -- input 13
                input_V_int((16*15)-1 downto 16*14) <= std_logic_vector(mu_pt_scaled);   -- input 14
                input_V_vld_int <= '0';
            when "0001" =>
                -- e1
                input_V_int((16*3)-1 downto 16*2)   <= std_logic_vector(ele_pt_scaled);  -- input 2
                input_V_int((16*4)-1 downto 16*3)   <= std_logic_vector(ele_eta_scaled); -- input 3
                -- jet1
                input_V_int((16*7)-1 downto 16*6)   <= std_logic_vector(jet_pt_scaled);  -- input 6  
                input_V_int((16*8)-1 downto 16*7)   <= std_logic_vector(jet_eta_scaled); -- input 7
                -- mu1
                input_V_int((16*16)-1 downto 16*15) <= std_logic_vector(mu_eta_scaled);  -- input 15
                input_V_int((16*17)-1 downto 16*16) <= std_logic_vector(mu_pt_scaled);   -- input 16
                input_V_vld_int <= '0';
            when "0010" =>
                -- jet2
                input_V_int((16*9)-1 downto 16*8)   <= std_logic_vector(jet_pt_scaled);   -- input 8  
                input_V_int((16*10)-1 downto 16*9)   <= std_logic_vector(jet_eta_scaled); -- input 9
                input_V_vld_int <= '0';
            when "0011" =>
                -- jet3
                input_V_int((16*11)-1 downto 16*10)   <= std_logic_vector(jet_pt_scaled);  -- input 10  
                input_V_int((16*12)-1 downto 16*11)   <= std_logic_vector(jet_eta_scaled); -- input 11
                input_V_vld_int <= '1';
            when "0100" =>
                input_V_vld_int <= '0';
            when "0101" =>
                input_V_vld_int <= '0';
            when "0110" =>
                input_V_vld_int <= '0';
            when "0111" =>
                input_V_vld_int <= '0';
            when "1000" =>
                input_V_vld_int <= '0';
            when "1001" =>
                input_V_vld_int <= '0';
            when "1010" =>
                input_V_vld_int <= '0';
            when "1011" =>
                input_V_vld_int <= '0';
            when others =>
                input_V_vld_int <= '0';
        end case;
        --end if;
    end process;

    -----------------------------------------------------------------------------------------------------------------
    ------------------------------------------------SCALERS----------------------------------------------------------
    -----------------------------------------------------------------------------------------------------------------
    --scalers have 2 clk latency 
    scaler_ele_pt : entity work.Scaler_unsigned
        generic map(
            DATA_IN_WIDTH      => 16,
            DATA_IN_INT_WIDTH  => 11,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => ele_obj.pt,
            vld_in   => ele_vld_in,
            mean     => ele_pt_mean,
            factor   => ele_pt_factor,
            data_out => ele_pt_scaled,
            vld_out  => ele_pt_scaled_vld
        );

    scaler_ele_eta : entity work.Scaler_signed
        generic map(
            DATA_IN_WIDTH      => 14,
            DATA_IN_INT_WIDTH  => 14,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => ele_obj.eta,
            vld_in   => ele_vld_in,
            mean     => ele_eta_mean,
            factor   => ele_eta_factor,
            data_out => ele_eta_scaled,
            vld_out  => ele_eta_scaled_vld
        );
        
        scaler_ele_phi : entity work.Scaler_signed
        generic map(
            DATA_IN_WIDTH      => 13,
            DATA_IN_INT_WIDTH  => 13,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => ele_obj.phi,
            vld_in   => ele_vld_in,
            mean     => ele_phi_mean,
            factor   => ele_phi_factor,
            data_out => ele_phi_scaled,
            vld_out  => ele_phi_scaled_vld
        );

    scaler_mu_pt : entity work.Scaler_unsigned
        generic map(
            DATA_IN_WIDTH      => 16,
            DATA_IN_INT_WIDTH  => 11,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => mu_obj.pt,
            vld_in   => mu_vld_in,
            mean     => mu_pt_mean,
            factor   => mu_pt_factor,
            data_out => mu_pt_scaled,
            vld_out  => mu_pt_scaled_vld
        );

    scaler_mu_eta : entity work.Scaler_signed
        generic map(
            DATA_IN_WIDTH      => 14,
            DATA_IN_INT_WIDTH  => 14,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => mu_obj.eta,
            vld_in   => mu_vld_in,
            mean     => mu_eta_mean,
            factor   => mu_eta_factor,
            data_out => mu_eta_scaled,
            vld_out  => mu_eta_scaled_vld
        );
        
    scaler_mu_phi : entity work.Scaler_signed
        generic map(
            DATA_IN_WIDTH      => 13,
            DATA_IN_INT_WIDTH  => 13,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => mu_obj.phi,
            vld_in   => mu_vld_in,
            mean     => mu_phi_mean,
            factor   => mu_phi_factor,
            data_out => mu_phi_scaled,
            vld_out  => mu_phi_scaled_vld
        );

    scaler_jet_pt : entity work.Scaler_unsigned
        generic map(
            DATA_IN_WIDTH      => 16,
            DATA_IN_INT_WIDTH  => 11,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => jet_obj.pt,
            vld_in   => jet_vld_in,
            mean     => jet_pt_mean,
            factor   => jet_pt_factor,
            data_out => jet_pt_scaled,
            vld_out  => jet_pt_scaled_vld
        );

    scaler_jet_eta : entity work.Scaler_signed
        generic map(
            DATA_IN_WIDTH      => 14,
            DATA_IN_INT_WIDTH  => 14,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => jet_obj.eta,
            vld_in   => jet_vld_in,
            mean     => jet_eta_mean,
            factor   => jet_eta_factor,
            data_out => jet_eta_scaled,
            vld_out  => jet_eta_scaled_vld
        );
        
    scaler_jet_phi : entity work.Scaler_signed
        generic map(
            DATA_IN_WIDTH      => 13,
            DATA_IN_INT_WIDTH  => 13,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => jet_obj.phi,
            vld_in   => jet_vld_in,
            mean     => jet_phi_mean,
            factor   => jet_phi_factor,
            data_out => jet_phi_scaled,
            vld_out  => jet_phi_scaled_vld
        );

    scaler_MET_pt : entity work.Scaler_unsigned
        generic map(
            DATA_IN_WIDTH      => 16,
            DATA_IN_INT_WIDTH  => 11,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => MET_obj.pt,
            vld_in   => MET_vld_in,
            mean     => MET_pt_mean,
            factor   => MET_pt_factor,
            data_out => MET_pt_scaled,
            vld_out  => MET_pt_scaled_vld
        );
        
    scaler_MET_phi : entity work.Scaler_signed
        generic map(
            DATA_IN_WIDTH      => 13,
            DATA_IN_INT_WIDTH  => 13,
            FACTOR_WIDTH       => 16,
            FACTOR_INT_WIDTH   => 1,
            DATA_OUT_WIDTH     => 16,
            DATA_OUT_INT_WIDTH => 8
        )
        port map(
            clk      => clk_algo,
            rst      => rst_algo,
            data_in  => MET_obj.phi,
            vld_in   => MET_vld_in,
            mean     => MET_phi_mean,
            factor   => MET_phi_factor,
            data_out => MET_phi_scaled,
            vld_out  => MET_phi_scaled_vld
        );

    output_p : process (clk_algo)
    begin
        if rising_edge(clk_algo) then
            input_V_o     <= input_V_int;
            input_V_vld_o <= input_V_vld_int;
        end if;
    end process;
    
    -- debug out
    debug_out_p: process (frame_cntr_o, MET_pt_scaled, ele_eta_scaled, ele_pt_scaled, jet_eta_scaled,
        jet_pt_scaled, mu_eta_scaled, mu_pt_scaled)
    begin
        --if rising_edge(clk_algo) then
        case (frame_cntr_o) is
            when "0000" =>
                -- electrons
                ele_pt (0)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(0)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(0)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (0)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(0)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(0)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (0)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(0)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(0)  <= std_logic_vector(jet_phi_scaled);
                -- MET
                MET_pt      <= std_logic_vector(MET_pt_scaled );
                MET_phi     <= std_logic_vector(MET_phi_scaled);
            when "0001" =>
                -- electrons
                ele_pt (1)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(1)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(1)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (1)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(1)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(1)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (1)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(1)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(1)  <= std_logic_vector(jet_phi_scaled);
            when "0010" =>
                -- electrons
                ele_pt (2)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(2)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(2)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (2)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(2)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(2)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (2)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(2)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(2)  <= std_logic_vector(jet_phi_scaled);
            when "0011" =>
                -- electrons
                ele_pt (3)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(3)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(3)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (3)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(3)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(3)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (3)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(3)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(3)  <= std_logic_vector(jet_phi_scaled);
            when "0100" =>
               -- electrons
                ele_pt (4)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(4)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(4)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (4)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(4)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(4)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (4)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(4)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(4)  <= std_logic_vector(jet_phi_scaled);
            when "0101" =>
                -- electrons
                ele_pt (5)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(5)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(5)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (5)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(5)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(5)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (5)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(5)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(5)  <= std_logic_vector(jet_phi_scaled);
            when "0110" =>
               -- electrons
                ele_pt (6)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(6)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(6)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (6)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(6)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(6)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (6)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(6)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(6)  <= std_logic_vector(jet_phi_scaled);
            when "0111" =>
                -- electrons
                ele_pt (7)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(7)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(7)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (7)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(7)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(7)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (7)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(7)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(7)  <= std_logic_vector(jet_phi_scaled);
            when "1000" =>
                -- electrons
                ele_pt (8)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(8)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(8)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (8)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(8)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(8)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (8)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(8)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(8)  <= std_logic_vector(jet_phi_scaled);
            when "1001" =>
                -- electrons
                ele_pt (9)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(9)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(9)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (9)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(9)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(9)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (9)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(9)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(9)  <= std_logic_vector(jet_phi_scaled);
            when "1010" =>
                -- electrons
                ele_pt (10)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(10)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(10)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (10)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(10)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(10)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (10)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(10)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(10)  <= std_logic_vector(jet_phi_scaled);
            when "1011" =>
                -- electrons
                ele_pt (11)  <= std_logic_vector(ele_pt_scaled );
                ele_eta(11)  <= std_logic_vector(ele_eta_scaled);
                ele_phi(11)  <= std_logic_vector(ele_phi_scaled);
                -- muons
                mu_pt (11)  <= std_logic_vector(mu_pt_scaled );
                mu_eta(11)  <= std_logic_vector(mu_eta_scaled);
                mu_phi(11)  <= std_logic_vector(mu_phi_scaled);
                -- jets
                jet_pt (11)  <= std_logic_vector(jet_pt_scaled );
                jet_eta(11)  <= std_logic_vector(jet_eta_scaled);
                jet_phi(11)  <= std_logic_vector(jet_phi_scaled);
            when others =>
        end case;
        --end if;
    end process;


end architecture;
