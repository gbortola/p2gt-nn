library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity Scaler_unsigned is
    generic(
        DATA_IN_WIDTH        : integer := 16;
        DATA_IN_INT_WIDTH    : integer := 11;
        FACTOR_WIDTH         : integer := 16;
        FACTOR_INT_WIDTH     : integer := 4;
        DATA_OUT_WIDTH       : integer := 16;
        DATA_OUT_INT_WIDTH   : integer := 6
    );
    port(
        clk     : in std_logic;
        rst     : in std_logic;
        data_in   : in unsigned(DATA_IN_WIDTH-1 downto 0);
        vld_in    : in std_logic;
        ---------------------------------
        -- z = (x-mean)*factor
        mean      : signed(DATA_IN_WIDTH  downto 0);
        factor    : signed(FACTOR_WIDTH-1 downto 0); --sqrt(1/var)
        ---------------------------------
        data_out  : out signed(DATA_OUT_WIDTH-1 downto 0);
        vld_out   : out std_logic
    );
end entity Scaler_unsigned;

architecture RTL of Scaler_unsigned is

    
    -- DSP arch (a=b)*c
    -- (a,b,c) registered
    -- preadd result registered
    -- mult result resistered
    -- result registered
    --signal a   : signed(DATA_IN_WIDTH downto 0);
    --signal b   : signed(DATA_IN_WIDTH downto 0);
    --signal c   : signed(FACTOR_WIDTH - 1  downto 0);
    signal p   : signed(DATA_IN_WIDTH + FACTOR_WIDTH + 1 downto 0);

    signal data_signed  : signed(DATA_IN_WIDTH       downto 0);
    signal add          : signed(DATA_IN_WIDTH + 1  downto 0) := (others => '0');
    signal mult         : signed(DATA_IN_WIDTH + FACTOR_WIDTH + 1  downto 0);
    
    signal mean_int1    : signed(DATA_IN_WIDTH downto 0);
    
    signal factor_int1  : signed(FACTOR_WIDTH - 1 downto 0) := (others => '0');
    signal factor_int2  : signed(FACTOR_WIDTH - 1 downto 0) := (others => '0');
    -- put these on array maybe?
    signal vld_int      : std_logic_vector(4 downto 0);
    
    signal data_out_int : signed(DATA_OUT_WIDTH-1 downto 0);
    signal data_out_o   : signed(DATA_OUT_WIDTH-1 downto 0);
    
    constant DATA_IN_FRAC_WIDTH  : integer := DATA_IN_WIDTH  - DATA_IN_INT_WIDTH; 
    constant FACTOR_FRAC_WIDTH   : integer := FACTOR_WIDTH   - FACTOR_INT_WIDTH; 
    constant DATA_OUT_FRAC_WIDTH : integer := DATA_OUT_WIDTH - DATA_OUT_INT_WIDTH;  
    
    constant shift_val : integer  := DATA_IN_FRAC_WIDTH+FACTOR_FRAC_WIDTH-DATA_OUT_FRAC_WIDTH;
    constant test_index : integer := shift_val+data_out'high;
    --attribute use_dsp : string;
    --attribute use_dsp of RTL : signal is "yes";

begin
    
    assert FACTOR_INT_WIDTH < DATA_IN_WIDTH
    report "Factor integer part cannot be greater than input data width"
    severity failure;


    process(clk)
    begin
        if rising_edge(clk) then
            if (rst = '1') then
                --vld_int1 <= '0';
                --vld_int2 <= '0';
                --mult     <= (others => '0');
            else
            
                -- add sign bit to the input signal
                data_signed <= signed(resize(data_in, DATA_IN_WIDTH+1));
                -- resgister mean and factor values
                mean_int1   <= mean;
                factor_int1 <= factor;

                -- pipeline operation
                -- data -> preadder --> mult -> final_reg
                -- total of 4 clk cycles latency;
                
                -- need the factor delayed version
                factor_int2 <= factor_int1;
                -- it should use the preadder in the DSP
                add      <= resize(data_signed, add'length) - resize(mean_int1, add'length);
                -- it should infere a DSP
                -- TODO check the overflow egde cases
                mult     <= add * factor_int2;
                p        <= mult;
            end if;
        end if;
    end process;
    
    
    -- this process will take care of eventual overflow (saturate the value to max, most positive)
    -- or underlflow (saturate the value to min, most negative)
    -- the porcess will also resize the data to match the requested one
    process(p)
    begin
        if p(p'high) = '1' then
            if ( and p(p'high downto test_index) = '1') then
                -- OK
                data_out_int <= resize (shift_right(p, (shift_val)), data_out_int'length);
            else
                -- underflow, set data out to min (negative)
                data_out_int(data_out_int'high)   <= ('1');
                data_out_int(data_out_int'high-1 downto 0) <= (others => '0');
            end if;
        elsif p(p'high) = '0' then
            if (or p(p'high downto test_index) = '0') then
                -- OK
                data_out_int <= resize (shift_right(p, (shift_val)), data_out_int'length);    
            else
                -- overflow, set data_out to max (positive)
                data_out_int(data_out_int'high)   <= ('0');
                data_out_int(data_out_int'high-1 downto 0) <= (others => '1');
            end if;
        end if;
    end process;
    
    vld_int(0) <= vld_in;
    
    process(clk)
    begin
        if rising_edge(clk) then
            vld_int(4 downto 1) <= vld_int(3 downto 0);
        end if;
    end process;
    
    vld_out  <= vld_int(4);
    
    -- register output
    --process(clk)
    --begin
    --    if rising_edge(clk) then
    --        data_out <= data_out_int;
    --    end if;
    --end process;
    data_out <= data_out_int;
    
end architecture RTL;
