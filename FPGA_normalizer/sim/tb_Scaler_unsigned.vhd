library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity tb_Scaler_unsigned is
end entity tb_Scaler_unsigned;

architecture RTL of tb_Scaler_unsigned is

    constant DATA_IN_WIDTH      : integer := 16;
    constant DATA_IN_INT_WIDTH  : integer := 11;
    constant FACTOR_WIDTH       : integer := 16;
    constant FACTOR_INT_WIDTH   : integer := 1 ;
    constant DATA_OUT_WIDTH     : integer := 16;
    constant DATA_OUT_INT_WIDTH : integer := 7 ;
    
    constant CLK_PERIOD  : time :=  2.778 ns;


    signal clk : std_logic;
    signal rst : std_logic;

    signal data_in, data_in_int  : unsigned(DATA_IN_WIDTH-1 downto 0);
    signal vld_in, vld_in_int    : std_logic;
    
    signal mean, mean_int     : signed(DATA_IN_WIDTH  downto 0)  := to_signed(15789, DATA_IN_WIDTH+1);  -- 15789/32 
    signal factor, factor_int : signed(FACTOR_WIDTH-1 downto 0)  := to_signed(280  , FACTOR_WIDTH);  -- 280/2**15 

    signal data_out  : signed(DATA_OUT_WIDTH-1 downto 0);
    signal vld_out   : std_logic;

    --*********************************Main Body of Code**********************************
begin

    -- Clock
    process
    begin
        clk  <=  '1';
        wait for CLK_PERIOD/2;
        clk  <=  '0';
        wait for CLK_PERIOD/2;
    end process;


    process
    begin
        rst <= '0';
        wait for CLK_PERIOD/2;
        rst <= '0';
        wait for CLK_PERIOD;
        rst <= '0';
        wait for CLK_PERIOD;
        rst <= '1';
        wait for 5*CLK_PERIOD;
        rst <= '0';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(3482, data_in'length); --108.8125
        mean     <= to_signed(10000, mean'length);
        factor   <= to_signed(3482 , factor'length);
        vld_in <= '0';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(3482, data_in'length);
        vld_in <= '1';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(29800, data_in'length); --16393.5938
        vld_in <= '0';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(29800, data_in'length);
        vld_in <= '1';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(15023, data_in'length); --1396.219
        vld_in <= '0';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(15023, data_in'length);
        vld_in <= '1';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(100, data_in'length); --11280.3125
        vld_in <= '0';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(100, data_in'length);
        vld_in <= '1';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(100, data_in'length); 
        vld_in <= '0';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(242, data_in'length); 
        vld_in <= '1';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(6521, data_in'length); 
        mean     <= to_signed(322 , mean'length);
        factor   <= to_signed(9833, factor'length);
        vld_in <= '1';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(6733, data_in'length); 
        mean     <= to_signed(22   , mean'length);
        factor   <= to_signed(100  , factor'length);
        vld_in <= '1';
        wait for CLK_PERIOD;
        data_in  <= to_unsigned(1100, data_in'length); 
        vld_in <= '0';
        wait;
    end process;
    
    
    
    process(clk)
    begin
        if rising_edge(clk) then
            data_in_int <= data_in;
            vld_in_int  <= vld_in;
            mean_int    <= mean;
            factor_int  <= factor;
        end if;
    end process;

    ------------------- Instantiate  modules  -----------------

    dut : entity work.Scaler_unsigned
        generic map(
            DATA_IN_WIDTH      => DATA_IN_WIDTH,
            DATA_IN_INT_WIDTH  => DATA_IN_INT_WIDTH,
            FACTOR_WIDTH       => FACTOR_WIDTH,
            FACTOR_INT_WIDTH   => FACTOR_INT_WIDTH,
            DATA_OUT_WIDTH     => DATA_OUT_WIDTH,
            DATA_OUT_INT_WIDTH => DATA_OUT_INT_WIDTH
        )
        port map(
            clk      => clk,
            rst      => rst,
            data_in  => data_in_int,
            vld_in   => vld_in_int,
            mean     => mean_int,
            factor   => factor_int,
            data_out => data_out,
            vld_out  => vld_out
        );
end architecture RTL;
