library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.emp_data_types.all;
use work.object_defs.all;
use work.NN_scaler_pkg.all;

use std.textio.all;


entity framer_tb is
end entity framer_tb;

architecture RTL of framer_tb is


    --SLR 0
    constant GTT_PROMPT_JETS_SLOT       : natural := 0;
    constant GTT_DISPLACED_JETS_SLOT    : natural := 1;
    constant GTT_HT_MISS_PROMPT_SLOT    : natural := 2;
    constant GTT_HT_MISS_DISPLACED_SLOT : natural := 3;
    constant GTT_ET_MISS_SLOT           : natural := 4;
    constant GTT_TAUS_SLOT              : natural := 5;
    constant GTT_PHI_CANDIDATE_SLOT     : natural := 6;
    constant GTT_RHO_CANDIDATE_SLOT     : natural := 7;
    constant GTT_BS_CANDIDATE_SLOT      : natural := 8;
    constant GTT_PRIM_VERT_SLOT         : natural := 9;

    --SLR 1
    constant CL2_JET_SLOT      : natural := 10;
    constant CL2_HT_MISS_SLOT  : natural := 11;
    constant CL2_ET_MISS_SLOT  : natural := 12;
    constant CL2_TAU_SLOT      : natural := 13;
    constant CL2_ELECTRON_SLOT : natural := 14;
    constant CL2_PHOTON_SLOT   : natural := 15;

    --SLR 2
    constant GCT_NON_ISO_EG_SLOT : natural := 16;
    constant GCT_ISO_EG_SLOT     : natural := 17;
    constant GCT_JETS_SLOT       : natural := 18;
    constant GCT_TAUS_SLOT       : natural := 19;
    constant GCT_HT_MISS_SLOT    : natural := 20;
    constant GCT_ET_MISS_SLOT    : natural := 21;


    constant GMT_SA_PROMPT_SLOT    : natural := 22;
    constant GMT_SA_DISPLACED_SLOT : natural := 23;
    constant GMT_TK_MUON_SLOT      : natural := 24;
    constant GMT_TOPO_SLOT         : natural := 25;


    constant CLK_ALGO_PERIOD : time :=  2.082 ns;
    constant CLK_LHC_PERIOD  : time :=  CLK_ALGO_PERIOD*12;

    signal clk_algo : std_logic;
    signal rst_algo : std_logic;

    signal lhc_clk : std_logic;
    signal lhc_rst, lhc_rst_int: std_logic;

    signal objects, objects_int         :t_obj_array(26 - 1 downto 0);
    signal objects_vld, objects_vld_int : std_logic;

    signal input_V     : std_logic_vector(271 downto 0);
    signal input_V_vld : std_logic;

    signal ele0_pt_i  :  unsigned(15 downto 0);
    signal ele0_phi_i :  signed  (12 downto 0);
    signal ele0_eta_i :  signed  (13 downto 0);
    signal ele1_pt_i  :  unsigned(15 downto 0);
    signal ele1_phi_i :  signed  (12 downto 0);
    signal ele1_eta_i :  signed  (13 downto 0);

    signal mu0_pt_i   :  unsigned(15 downto 0);
    signal mu0_phi_i  :  signed  (12 downto 0);
    signal mu0_eta_i  :  signed  (13 downto 0);
    signal mu1_pt_i   :  unsigned(15 downto 0);
    signal mu1_phi_i  :  signed  (12 downto 0);
    signal mu1_eta_i  :  signed  (13 downto 0);

    signal jet0_pt_i  :  unsigned(15 downto 0);
    signal jet0_phi_i :  signed  (12 downto 0);
    signal jet0_eta_i :  signed  (13 downto 0);
    signal jet1_pt_i  :  unsigned(15 downto 0);
    signal jet1_phi_i :  signed  (12 downto 0);
    signal jet1_eta_i :  signed  (13 downto 0);
    signal jet2_pt_i  :  unsigned(15 downto 0);
    signal jet2_phi_i :  signed  (12 downto 0);
    signal jet2_eta_i :  signed  (13 downto 0);
    signal jet3_pt_i  :  unsigned(15 downto 0);
    signal jet3_phi_i :  signed  (12 downto 0);
    signal jet3_eta_i :  signed  (13 downto 0);

    signal MET_pt_i   :  unsigned(15 downto 0);
    signal MET_phi_i  :  signed  (12 downto 0);


    signal ele0_pt_o  :  signed(15 downto 0);
    signal ele0_phi_o :  signed(15 downto 0);
    signal ele0_eta_o :  signed(15 downto 0);
    signal ele1_pt_o  :  signed(15 downto 0);
    signal ele1_phi_o :  signed(15 downto 0);
    signal ele1_eta_o :  signed(15 downto 0);

    signal mu0_pt_o   :  signed(15 downto 0);
    signal mu0_phi_o  :  signed(15 downto 0);
    signal mu0_eta_o  :  signed(15 downto 0);
    signal mu1_pt_o   :  signed(15 downto 0);
    signal mu1_phi_o  :  signed(15 downto 0);
    signal mu1_eta_o  :  signed(15 downto 0);

    signal jet0_pt_o  :  signed(15 downto 0);
    signal jet0_phi_o :  signed(15 downto 0);
    signal jet0_eta_o :  signed(15 downto 0);
    signal jet1_pt_o  :  signed(15 downto 0);
    signal jet1_phi_o :  signed(15 downto 0);
    signal jet1_eta_o :  signed(15 downto 0);
    signal jet2_pt_o  :  signed(15 downto 0);
    signal jet2_phi_o :  signed(15 downto 0);
    signal jet2_eta_o :  signed(15 downto 0);
    signal jet3_pt_o  :  signed(15 downto 0);
    signal jet3_phi_o :  signed(15 downto 0);
    signal jet3_eta_o :  signed(15 downto 0);

    signal MET_pt_o   :  signed(15 downto 0);
    signal MET_phi_o  :  signed(15 downto 0);

    signal ele0_pt_1_o  :  signed(15 downto 0);
    signal ele0_phi_1_o :  signed(15 downto 0);
    signal ele0_eta_1_o :  signed(15 downto 0);
    signal ele1_pt_1_o  :  signed(15 downto 0);
    signal ele1_phi_1_o :  signed(15 downto 0);
    signal ele1_eta_1_o :  signed(15 downto 0);

    signal mu0_pt_1_o   :  signed(15 downto 0);
    signal mu0_phi_1_o  :  signed(15 downto 0);
    signal mu0_eta_1_o  :  signed(15 downto 0);
    signal mu1_pt_1_o   :  signed(15 downto 0);
    signal mu1_phi_1_o  :  signed(15 downto 0);
    signal mu1_eta_1_o  :  signed(15 downto 0);

    signal jet0_pt_1_o  :  signed(15 downto 0);
    signal jet0_phi_1_o :  signed(15 downto 0);
    signal jet0_eta_1_o :  signed(15 downto 0);
    signal jet1_pt_1_o  :  signed(15 downto 0);
    signal jet1_phi_1_o :  signed(15 downto 0);
    signal jet1_eta_1_o :  signed(15 downto 0);
    signal jet2_pt_1_o  :  signed(15 downto 0);
    signal jet2_phi_1_o :  signed(15 downto 0);
    signal jet2_eta_1_o :  signed(15 downto 0);
    signal jet3_pt_1_o  :  signed(15 downto 0);
    signal jet3_phi_1_o :  signed(15 downto 0);
    signal jet3_eta_1_o :  signed(15 downto 0);

    signal MET_pt_1_o   :  signed(15 downto 0);
    signal MET_phi_1_o  :  signed(15 downto 0);


    signal ele0_pt_r  :  signed(15 downto 0);
    signal ele0_phi_r :  signed(15 downto 0);
    signal ele0_eta_r :  signed(15 downto 0);
    signal ele1_pt_r  :  signed(15 downto 0);
    signal ele1_phi_r :  signed(15 downto 0);
    signal ele1_eta_r :  signed(15 downto 0);

    signal mu0_pt_r   :  signed(15 downto 0);
    signal mu0_phi_r  :  signed(15 downto 0);
    signal mu0_eta_r  :  signed(15 downto 0);
    signal mu1_pt_r   :  signed(15 downto 0);
    signal mu1_phi_r  :  signed(15 downto 0);
    signal mu1_eta_r  :  signed(15 downto 0);

    signal jet0_pt_r  :  signed(15 downto 0);
    signal jet0_phi_r :  signed(15 downto 0);
    signal jet0_eta_r :  signed(15 downto 0);
    signal jet1_pt_r  :  signed(15 downto 0);
    signal jet1_phi_r :  signed(15 downto 0);
    signal jet1_eta_r :  signed(15 downto 0);
    signal jet2_pt_r  :  signed(15 downto 0);
    signal jet2_phi_r :  signed(15 downto 0);
    signal jet2_eta_r :  signed(15 downto 0);
    signal jet3_pt_r  :  signed(15 downto 0);
    signal jet3_phi_r :  signed(15 downto 0);
    signal jet3_eta_r :  signed(15 downto 0);

    signal MET_pt_r   :  signed(15 downto 0);
    signal MET_phi_r  :  signed(15 downto 0);

    signal ele_pt           :  data_coll_scaled;
    signal ele_eta          :  data_coll_scaled;
    signal ele_phi          :  data_coll_scaled;

    signal mu_pt            :  data_coll_scaled;
    signal mu_eta           :  data_coll_scaled;
    signal mu_phi           :  data_coll_scaled;

    signal jet_pt           :  data_coll_scaled;
    signal jet_eta          :  data_coll_scaled;
    signal jet_phi          :  data_coll_scaled;

    signal MET_pt           :  std_logic_vector(15 downto 0);
    signal MET_phi          :  std_logic_vector(15 downto 0);

    -- signals for file read-write
    signal ena : std_logic := '0';
    signal file_in_end : boolean := false;

    signal enb : std_logic := '0';
    signal file_out_end : boolean := false;

    signal frame_cntr : integer := 0;

    signal err_cnt : unsigned(15 downto 0) := ( others => '0');

    constant tolerance : signed(15 downto 0) := to_signed(2,16);

    signal enb_del : std_logic;



    --*********************************Main Body of Code**********************************
begin

    -- Clock
    process
    begin
        clk_algo  <=  '1';
        wait for CLK_ALGO_PERIOD/2;
        clk_algo  <=  '0';
        wait for CLK_ALGO_PERIOD/2;
    end process;

    process
    begin
        lhc_clk  <=  '1';
        wait for CLK_LHC_PERIOD/2;
        lhc_clk  <=  '0';
        wait for CLK_LHC_PERIOD/2;
    end process;

    process
    begin
        lhc_rst  <=  '0';
        wait for CLK_LHC_PERIOD;
        lhc_rst  <=  '1';
        wait for 5*CLK_LHC_PERIOD;
        lhc_rst  <=  '0';
        wait for 4*CLK_LHC_PERIOD;
        wait;
    end process;

    frame_cntr_p : process(clk_algo) is
    begin
        if rising_edge(clk_algo)then
            if frame_cntr = 11 then
                frame_cntr <= 0;
            else
                frame_cntr <= frame_cntr + 1;
            end if;
        end if;
    end process;



    read_in_p : process (clk_algo, rst_algo) is
        ---------------------------------------------------------------------------------------------------------

        constant NUM_COL                : integer := 26;   -- number of column of file (27 inputs)

        type t_integer_array       is array(integer range <> )  of integer;
        file test_vector                : text open read_mode is "stimulus.txt";
        variable row                    : line;
        variable v_data_read            : t_integer_array(1 to NUM_COL);
        variable v_data_row_counter     : integer := 0;

        -----------------------------------------------------------------------------------------------------------
    begin
        if(rst_algo='1') then
            v_data_row_counter     := 0;
            v_data_read            := (others=> -1);
        ------------------------------------
        elsif(rising_edge(clk_algo)) then

            if (endfile(test_vector)) then
                file_in_end <= true;
            end if;
            if frame_cntr = 10 then
                if(ena = '1') then  -- external enable signal

                    -- read from input file in "row" variable
                    if(not endfile(test_vector)) then
                        v_data_row_counter := v_data_row_counter + 1;
                        readline(test_vector,row);
                    end if;

                    -- read integer number from "row" variable in integer array
                    for kk in 1 to NUM_COL loop
                        read(row,v_data_read(kk));
                    end loop;

                    ele0_pt_i  <= to_unsigned(v_data_read(1),16);
                    ele0_eta_i <= to_signed  (v_data_read(2),14);
                    ele0_phi_i <= to_signed  (v_data_read(3),13);
                    ele1_pt_i  <= to_unsigned(v_data_read(4),16);
                    ele1_eta_i <= to_signed  (v_data_read(5),14);
                    ele1_phi_i <= to_signed  (v_data_read(6),13);

                    mu0_eta_i  <= to_signed  (v_data_read(21),14);
                    mu0_phi_i  <= to_signed  (v_data_read(22),13);
                    mu0_pt_i   <= to_unsigned(v_data_read(23),16);
                    mu1_eta_i  <= to_signed  (v_data_read(24),14);
                    mu1_phi_i  <= to_signed  (v_data_read(25),13);
                    mu1_pt_i   <= to_unsigned(v_data_read(26),16);

                    jet0_pt_i  <= to_unsigned(v_data_read(7 ),16);
                    jet0_eta_i <= to_signed  (v_data_read(8 ),14);
                    jet0_phi_i <= to_signed  (v_data_read(9 ),13);
                    jet1_pt_i  <= to_unsigned(v_data_read(10),16);
                    jet1_eta_i <= to_signed  (v_data_read(11),14);
                    jet1_phi_i <= to_signed  (v_data_read(12),13);
                    jet2_pt_i  <= to_unsigned(v_data_read(13),16);
                    jet2_eta_i <= to_signed  (v_data_read(14),14);
                    jet2_phi_i <= to_signed  (v_data_read(15),13);
                    jet3_pt_i  <= to_unsigned(v_data_read(16),16);
                    jet3_eta_i <= to_signed  (v_data_read(17),14);
                    jet3_phi_i <= to_signed  (v_data_read(18),13);

                    MET_pt_i   <= to_unsigned(v_data_read(19),16);
                    MET_phi_i  <= to_signed  (v_data_read(20),13);
                end if;
            end if;
        end if;
    end process read_in_p;

    read_out_p : process(lhc_clk, lhc_rst) is
        ---------------------------------------------------------------------------------------------------------

        constant NUM_COL                : integer := 26;   -- number of column of file

        type t_integer_array       is array(integer range <> )  of integer;
        file test_vector                : text open read_mode is "output_file.txt";
        variable row                    : line;
        variable v_data_read            : t_integer_array(1 to NUM_COL);
        variable v_data_row_counter     : integer := 0;

        -----------------------------------------------------------------------------------------------------------
    begin
        if(lhc_rst='1') then
            v_data_row_counter     := 0;
            v_data_read            := (others=> -1);
        ------------------------------------
        elsif(rising_edge(lhc_clk)) then

            if(enb = '1') then  -- external enable signal

                -- read from input file in "row" variable
                if(not endfile(test_vector)) then
                    v_data_row_counter := v_data_row_counter + 1;
                    readline(test_vector,row);
                else
                    file_out_end <= true;
                end if;

                -- read integer number from "row" variable in integer array
                for kk in 1 to NUM_COL loop
                    read(row,v_data_read(kk));
                end loop;
                ele0_pt_o  <= to_signed(v_data_read(1),16);
                ele0_eta_o <= to_signed(v_data_read(2),16);
                ele1_pt_o  <= to_signed(v_data_read(4),16);
                ele1_eta_o <= to_signed(v_data_read(5),16);

                mu0_pt_o   <= to_signed(v_data_read(23),16);
                mu0_eta_o  <= to_signed(v_data_read(21),16);
                mu1_pt_o   <= to_signed(v_data_read(26),16);
                mu1_eta_o  <= to_signed(v_data_read(24),16);

                jet0_pt_o  <= to_signed(v_data_read(7),16);
                jet0_eta_o <= to_signed(v_data_read(8),16);
                jet1_pt_o  <= to_signed(v_data_read(10),16);
                jet1_eta_o <= to_signed(v_data_read(11),16);
                jet2_pt_o  <= to_signed(v_data_read(13),16);
                jet2_eta_o <= to_signed(v_data_read(14),16);
                jet3_pt_o  <= to_signed(v_data_read(16),16);
                jet3_eta_o <= to_signed(v_data_read(17),16);

                MET_pt_o   <= to_signed(v_data_read(19),16);
            end if;

        end if;
    end process read_out_p;

    process (lhc_clk)
    begin
        if rising_edge(lhc_clk) then

            enb_del <= enb;


            ele0_pt_1_o  <= ele0_pt_o ;
            ele0_eta_1_o <= ele0_eta_o;
            ele1_pt_1_o  <= ele1_pt_o ;
            ele1_eta_1_o <= ele1_eta_o;

            mu0_pt_1_o   <= mu0_pt_o  ;
            mu0_eta_1_o  <= mu0_eta_o ;
            mu1_pt_1_o   <= mu1_pt_o  ;
            mu1_eta_1_o  <= mu1_eta_o ;

            jet0_pt_1_o  <= jet0_pt_o ;
            jet0_eta_1_o <= jet0_eta_o;
            jet1_pt_1_o  <= jet1_pt_o ;
            jet1_eta_1_o <= jet1_eta_o;
            jet2_pt_1_o  <= jet2_pt_o ;
            jet2_eta_1_o <= jet2_eta_o;
            jet3_pt_1_o  <= jet3_pt_o ;
            jet3_eta_1_o <= jet3_eta_o;

            MET_pt_1_o   <= MET_pt_o  ;
        end if;
    end process;

    process
    begin
        rst_algo  <=  '0';
        objects     <= (others => NULL_OBJECT);
        objects_vld <= '0';
        wait for CLK_ALGO_PERIOD;
        rst_algo  <=  '1';
        wait for 70*CLK_ALGO_PERIOD;
        rst_algo  <=  '0';
        ena <= '0';
        enb <= '0';
        wait for 12*CLK_ALGO_PERIOD;
        ena <= '1';
        enb <= '1';
        wait for CLK_ALGO_PERIOD;
        while file_in_end = false loop
            objects(CL2_ELECTRON_SLOT).valid <= '1';
            objects(CL2_ELECTRON_SLOT).pt    <= ele0_pt_i;
            objects(CL2_ELECTRON_SLOT).eta   <= ele0_eta_i;
            objects(CL2_ELECTRON_SLOT).phi   <= ele0_phi_i;
            objects(GMT_TK_MUON_SLOT).valid  <= '1';
            objects(GMT_TK_MUON_SLOT).pt     <= mu0_pt_i;
            objects(GMT_TK_MUON_SLOT).eta    <= mu0_eta_i;
            objects(GMT_TK_MUON_SLOT).phi    <= mu0_phi_i;
            objects(CL2_JET_SLOT).valid      <= '1';
            objects(CL2_JET_SLOT).pt         <= jet0_pt_i;
            objects(CL2_JET_SLOT).eta        <= jet0_eta_i;
            objects(CL2_JET_SLOT).phi        <= jet0_phi_i;
            objects(CL2_ET_MISS_SLOT).valid  <= '1';
            objects(CL2_ET_MISS_SLOT).pt     <= MET_pt_i;
            objects(CL2_ET_MISS_SLOT).phi    <= MET_phi_i;
            objects_vld <= '1';
            wait for CLK_ALGO_PERIOD;
            objects(CL2_ELECTRON_SLOT).valid <= '1';
            objects(CL2_ELECTRON_SLOT).pt    <= ele1_pt_i;
            objects(CL2_ELECTRON_SLOT).eta   <= ele1_eta_i;
            objects(CL2_ELECTRON_SLOT).phi   <= ele1_phi_i;
            objects(GMT_TK_MUON_SLOT).valid  <= '1';
            objects(GMT_TK_MUON_SLOT).pt     <= mu1_pt_i;
            objects(GMT_TK_MUON_SLOT).eta    <= mu1_eta_i;
            objects(GMT_TK_MUON_SLOT).phi    <= mu1_phi_i;
            objects(CL2_JET_SLOT).valid      <= '1';
            objects(CL2_JET_SLOT).pt         <= jet1_pt_i;
            objects(CL2_JET_SLOT).eta        <= jet1_eta_i;
            objects(CL2_JET_SLOT).phi        <= jet1_phi_i;
            objects(CL2_ET_MISS_SLOT).valid  <= '0';
            objects_vld <= '1';
            wait for CLK_ALGO_PERIOD;
            objects(CL2_ELECTRON_SLOT).valid <= '0';
            objects(GMT_TK_MUON_SLOT).valid  <= '0';
            objects(CL2_JET_SLOT).valid      <= '1';
            objects(CL2_JET_SLOT).pt         <= jet2_pt_i;
            objects(CL2_JET_SLOT).eta        <= jet2_eta_i;
            objects(CL2_JET_SLOT).phi        <= jet2_phi_i;
            objects(CL2_ET_MISS_SLOT).valid  <= '0';
            objects_vld <= '1';
            wait for CLK_ALGO_PERIOD;
            objects(CL2_ELECTRON_SLOT).valid <= '0';
            objects(GMT_TK_MUON_SLOT).valid  <= '0';
            objects(CL2_JET_SLOT).valid      <= '1';
            objects(CL2_JET_SLOT).pt         <= jet3_pt_i;
            objects(CL2_JET_SLOT).eta        <= jet3_eta_i;
            objects(CL2_JET_SLOT).phi        <= jet3_phi_i;
            objects(CL2_ET_MISS_SLOT).valid  <= '0';
            objects_vld <= '1';
            wait for CLK_ALGO_PERIOD;
            objects(CL2_ELECTRON_SLOT).valid <= '0';
            objects(GMT_TK_MUON_SLOT).valid  <= '0';
            objects(CL2_JET_SLOT).valid      <= '1';
            objects(CL2_ET_MISS_SLOT).valid  <= '0';
            objects_vld <= '1';
            wait for 8*CLK_ALGO_PERIOD;
        end loop;
        ena <= '0';
        wait;
    end process;

    process(lhc_clk)
    begin
        if rising_edge(lhc_clk) then
            lhc_rst_int     <= lhc_rst;
        end if;
    end process;


    process(clk_algo)
    begin
        if rising_edge(clk_algo) then
            objects_int     <= objects;
            objects_vld_int <= objects_vld;
        end if;
    end process;

    --ele0_pt_r   <= signed(input_V(15 downto 0));
    --ele0_eta_r  <= signed(input_V(31 downto 16));
    --ele1_pt_r   <= signed(input_V(47 downto 32));
    --ele1_eta_r  <= signed(input_V(63 downto 48));
    --
    --mu0_pt_r    <= signed(input_V(239 downto 224));
    --mu0_eta_r   <= signed(input_V(223 downto 208));
    --mu1_pt_r    <= signed(input_V(271 downto 256));
    --mu1_eta_r   <= signed(input_V(255 downto 240));
    --
    --jet0_pt_r   <= signed(input_V(79 downto 64));
    --jet0_eta_r  <= signed(input_V(95 downto 80));
    --jet1_pt_r   <= signed(input_V(111 downto 96));
    --jet1_eta_r  <= signed(input_V(127 downto 112));
    --jet2_pt_r   <= signed(input_V(143 downto 128));
    --jet2_eta_r  <= signed(input_V(159 downto 144));
    --jet3_pt_r   <= signed(input_V(175 downto 160));
    --jet3_eta_r  <= signed(input_V(191 downto 176));
    --
    --MET_pt_r    <= signed(input_V(207 downto 192));

    ele0_pt_r   <= signed(ele_pt (0));
    ele0_eta_r  <= signed(ele_eta(0));
    ele0_phi_r  <= signed(ele_phi(0));
    ele1_pt_r   <= signed(ele_pt (1));
    ele1_eta_r  <= signed(ele_eta(1));
    ele1_phi_r  <= signed(ele_phi(1));
    
    mu0_pt_r   <= signed(mu_pt (0));
    mu0_eta_r  <= signed(mu_eta(0));
    mu0_phi_r  <= signed(mu_phi(0));
    mu1_pt_r   <= signed(mu_pt (1));
    mu1_eta_r  <= signed(mu_eta(1));
    mu1_phi_r  <= signed(mu_phi(1));
    
    jet0_pt_r   <= signed(jet_pt (0));
    jet0_eta_r  <= signed(jet_eta(0));
    jet0_phi_r  <= signed(jet_phi(0));
    jet1_pt_r   <= signed(jet_pt (1));
    jet1_eta_r  <= signed(jet_eta(1));
    jet1_phi_r  <= signed(jet_phi(1));
    jet2_pt_r   <= signed(jet_pt (2));
    jet2_eta_r  <= signed(jet_eta(2));
    jet2_phi_r  <= signed(jet_phi(2));
    jet3_pt_r   <= signed(jet_pt (3));
    jet3_eta_r  <= signed(jet_eta(3));
    jet3_phi_r  <= signed(jet_phi(3));
    
    MET_pt_r    <= signed(MET_pt );
    MET_phi_r   <= signed(MET_phi);



    p_dump  : process(lhc_clk, lhc_rst)
        file test_vector      : text open write_mode is "output_file_scaled.txt";
        variable row          : line;
    begin

        if(lhc_rst='1') then
        ------------------------------------
        elsif(rising_edge(lhc_clk)) then

            if(enb_del = '1') then

                write(row, to_integer(ele0_pt_r), right , 6);
                write(row, to_integer(ele0_eta_r), right, 6);
                write(row, to_integer(ele0_phi_r), right, 6);
                write(row, to_integer(ele1_pt_r), right , 6);
                write(row, to_integer(ele1_eta_r), right, 6);
                write(row, to_integer(ele1_phi_r), right, 6);
                write(row, to_integer(jet0_pt_r), right , 6);
                write(row, to_integer(jet0_eta_r), right, 6);
                write(row, to_integer(jet0_phi_r), right, 6);
                write(row, to_integer(jet1_pt_r), right , 6);
                write(row, to_integer(jet1_eta_r), right, 6);
                write(row, to_integer(jet1_phi_r), right, 6);
                write(row, to_integer(jet2_pt_r), right , 6);
                write(row, to_integer(jet2_eta_r), right, 6);
                write(row, to_integer(jet2_phi_r), right, 6);
                write(row, to_integer(jet3_pt_r), right , 6);
                write(row, to_integer(jet3_eta_r), right, 6);
                write(row, to_integer(jet3_phi_r), right, 6);
                write(row, to_integer(MET_pt_r)  , right, 6);
                write(row, to_integer(MET_phi_r) , right, 6);
                write(row, to_integer(mu0_eta_r), right , 6);
                write(row, to_integer(mu0_phi_r), right , 6);
                write(row, to_integer(mu0_pt_r) , right , 6);
                write(row, to_integer(mu1_eta_r), right , 6);
                write(row, to_integer(mu1_phi_r), right , 6);
                write(row, to_integer(mu1_pt_r), right  , 6);


                writeline(test_vector,row);

            end if;
        end if;
    end process p_dump;


    -------------------- Check data ---------------------------
    --check_data_p : process (lhc_clk) is
    --    ---------------------------------------------------------------------------------------------------------
    --
    --    file test_vector                : text open write_mode is "output_file_fir.txt";
    --    variable row                    : line;
    --    -----------------------------------------------------------------------------------------------------------
    --begin
    --
    --    if(rising_edge(lhc_clk)) then
    --        if (enb = '1') then
    --            if (signed(ele0_pt_r) < signed(ele0_pt_1_o))then
    --                report "Left output does not match, expected " & integer'image(to_integer(signed(value1_std_logic_24_bit_out))) 
    --                & " got " & integer'image(to_integer(signed(m_axis_tdata(23 downto 0)))) severity warning;
    --                err_cnt <= err_cnt + X"0001";
    --            elsif (signed(m_axis_tdata(23 downto 0)) > signed(value1_up_out)) then
    --                report "Left output does not match, expected " & integer'image(to_integer(signed(value1_std_logic_24_bit_out))) 
    --                & " got " & integer'image(to_integer(signed(m_axis_tdata(23 downto 0)))) severity warning;
    --                err_cnt <= err_cnt + X"0001";
    --            end if;
    --        elsif (m_axis_tvalid = '1' and m_axis_tlast = '1') then
    --            write(row, to_integer(signed(value1_fir_24_bit_out))    , right, 15);
    --            write(row, to_integer(signed(m_axis_tdata(23 downto 0))), right, 15);
    --            writeline(test_vector,row);
    --            value2_fir_24_bit_out <= m_axis_tdata(23 downto 0);
    --            if (signed(m_axis_tdata(23 downto 0)) < signed(value2_down_out))then
    --                report "Right output does not match, expected " & integer'image(to_integer(signed(value2_std_logic_24_bit_out))) 
    --                & " got " & integer'image(to_integer(signed(m_axis_tdata(23 downto 0)))) severity warning;
    --                err_cnt <= err_cnt + X"0001";
    --            elsif (signed(m_axis_tdata(23 downto 0)) > signed(value2_up_out)) then
    --                report "Right output does not match, expected " & integer'image(to_integer(signed(value2_std_logic_24_bit_out))) 
    --                & " got " & integer'image(to_integer(signed(m_axis_tdata(23 downto 0)))) severity warning;
    --                err_cnt <= err_cnt + X"0001";
    --            end if;
    --        end if;
    --    end if;
    --
    --end process;
    ------------------- Instantiate  modules  -----------------

    dut : entity work.framer
        port map(
            lhc_clk       => lhc_clk,
            lhc_rst       => lhc_rst_int,
            clk_algo      => clk_algo,
            rst_algo      => rst_algo,
            objects_valid => objects_vld_int,
            objects       => objects_int,
            input_V_o     => input_V,
            input_V_vld_o => input_V_vld,
            ele_pt        => ele_pt,
            ele_eta       => ele_eta,
            ele_phi       => ele_phi,
            mu_pt         => mu_pt,
            mu_eta        => mu_eta,
            mu_phi        => mu_phi,
            jet_pt        => jet_pt,
            jet_eta       => jet_eta,
            jet_phi       => jet_phi,
            MET_pt        => MET_pt,
            MET_phi       => MET_phi
        ) ;


end architecture RTL;
