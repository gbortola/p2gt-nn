// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2020.1
// Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _dense_latency_ap_ufixed_ap_fixed_config5_0_0_HH_
#define _dense_latency_ap_ufixed_ap_fixed_config5_0_0_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct dense_latency_ap_ufixed_ap_fixed_config5_0_0 : public sc_module {
    // Port declarations 7
    sc_in_clk ap_clk;
    sc_in< sc_logic > ap_rst;
    sc_in< sc_lv<4> > data_0_V_read;
    sc_in< sc_lv<4> > data_2_V_read;
    sc_in< sc_lv<4> > data_3_V_read;
    sc_out< sc_lv<15> > ap_return;
    sc_in< sc_logic > ap_ce;


    // Module declarations
    dense_latency_ap_ufixed_ap_fixed_config5_0_0(sc_module_name name);
    SC_HAS_PROCESS(dense_latency_ap_ufixed_ap_fixed_config5_0_0);

    ~dense_latency_ap_ufixed_ap_fixed_config5_0_0();

    sc_trace_file* mVcdFile;

    sc_signal< sc_lv<12> > mul_ln1118_fu_72_p2;
    sc_signal< sc_lv<12> > mul_ln1118_reg_105;
    sc_signal< bool > ap_block_state1_pp0_stage0_iter0;
    sc_signal< bool > ap_block_state2_pp0_stage0_iter1;
    sc_signal< bool > ap_block_pp0_stage0_11001;
    sc_signal< sc_lv<11> > mul_ln728_fu_70_p2;
    sc_signal< sc_lv<11> > mul_ln728_reg_109;
    sc_signal< sc_lv<4> > data_0_V_read_3_reg_208;
    sc_signal< sc_lv<4> > mul_ln728_fu_70_p0;
    sc_signal< bool > ap_block_pp0_stage0;
    sc_signal< sc_lv<4> > mul_ln1118_fu_72_p0;
    sc_signal< sc_lv<10> > shl_ln_fu_127_p3;
    sc_signal< sc_lv<11> > zext_ln1118_1_fu_134_p1;
    sc_signal< sc_lv<11> > sub_ln1118_fu_138_p2;
    sc_signal< sc_lv<11> > zext_ln1118_fu_124_p1;
    sc_signal< sc_lv<11> > sub_ln1118_1_fu_144_p2;
    sc_signal< sc_lv<13> > shl_ln1_fu_150_p3;
    sc_signal< sc_lv<14> > shl_ln728_1_fu_162_p3;
    sc_signal< sc_lv<13> > shl_ln728_2_fu_174_p3;
    sc_signal< sc_lv<15> > sext_ln728_fu_158_p1;
    sc_signal< sc_lv<15> > sext_ln728_1_fu_170_p1;
    sc_signal< sc_lv<14> > zext_ln728_1_fu_182_p1;
    sc_signal< sc_lv<14> > add_ln703_1_fu_192_p2;
    sc_signal< sc_lv<15> > sext_ln703_fu_198_p1;
    sc_signal< sc_lv<15> > add_ln703_fu_186_p2;
    sc_signal< sc_lv<15> > add_ln703_2_fu_202_p2;
    sc_signal< sc_logic > ap_ce_reg;
    sc_signal< sc_lv<15> > ap_return_int_reg;
    sc_signal< sc_lv<12> > mul_ln1118_fu_72_p00;
    sc_signal< sc_lv<11> > mul_ln728_fu_70_p00;
    static const sc_logic ap_const_logic_1;
    static const sc_logic ap_const_logic_0;
    static const bool ap_const_boolean_1;
    static const bool ap_const_boolean_0;
    static const sc_lv<11> ap_const_lv11_64;
    static const sc_lv<12> ap_const_lv12_FB7;
    static const sc_lv<6> ap_const_lv6_0;
    static const sc_lv<11> ap_const_lv11_0;
    static const sc_lv<2> ap_const_lv2_0;
    static const sc_lv<14> ap_const_lv14_3E60;
    // Thread declarations
    void thread_ap_clk_no_reset_();
    void thread_add_ln703_1_fu_192_p2();
    void thread_add_ln703_2_fu_202_p2();
    void thread_add_ln703_fu_186_p2();
    void thread_ap_block_pp0_stage0();
    void thread_ap_block_pp0_stage0_11001();
    void thread_ap_block_state1_pp0_stage0_iter0();
    void thread_ap_block_state2_pp0_stage0_iter1();
    void thread_ap_return();
    void thread_mul_ln1118_fu_72_p0();
    void thread_mul_ln1118_fu_72_p00();
    void thread_mul_ln1118_fu_72_p2();
    void thread_mul_ln728_fu_70_p0();
    void thread_mul_ln728_fu_70_p00();
    void thread_mul_ln728_fu_70_p2();
    void thread_sext_ln703_fu_198_p1();
    void thread_sext_ln728_1_fu_170_p1();
    void thread_sext_ln728_fu_158_p1();
    void thread_shl_ln1_fu_150_p3();
    void thread_shl_ln728_1_fu_162_p3();
    void thread_shl_ln728_2_fu_174_p3();
    void thread_shl_ln_fu_127_p3();
    void thread_sub_ln1118_1_fu_144_p2();
    void thread_sub_ln1118_fu_138_p2();
    void thread_zext_ln1118_1_fu_134_p1();
    void thread_zext_ln1118_fu_124_p1();
    void thread_zext_ln728_1_fu_182_p1();
};

}

using namespace ap_rtl;

#endif
