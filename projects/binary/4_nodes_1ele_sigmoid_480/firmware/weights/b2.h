//Numpy array shape [4]
//Min -0.250000000000
//Max 0.875000000000
//Number of zeros 0

#ifndef B2_H_
#define B2_H_

#ifndef __SYNTHESIS__
bias2_t b2[4];
#else
bias2_t b2[4] = {0.87500, -0.25000, 0.68750, -0.25000};
#endif

#endif
