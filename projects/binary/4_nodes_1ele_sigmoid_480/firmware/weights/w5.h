//Numpy array shape [4, 1]
//Min -2.281250000000
//Max 3.125000000000
//Number of zeros 1

#ifndef W5_H_
#define W5_H_

#ifndef __SYNTHESIS__
weight5_t w5[4];
#else
weight5_t w5[4] = {-2.03125, 0.00000, -2.28125, 3.12500};
#endif

#endif
