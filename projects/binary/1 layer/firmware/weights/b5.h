//Numpy array shape [2]
//Min 0.000000000000
//Max 0.000000000000
//Number of zeros 2

#ifndef B5_H_
#define B5_H_

#ifndef __SYNTHESIS__
bias5_t b5[2];
#else
bias5_t b5[2] = {0.0, 0.0};
#endif

#endif
