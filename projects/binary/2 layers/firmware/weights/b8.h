//Numpy array shape [2]
//Min 0.000000000000
//Max 0.000000000000
//Number of zeros 2

#ifndef B8_H_
#define B8_H_

#ifndef __SYNTHESIS__
bias8_t b8[2];
#else
bias8_t b8[2] = {0.0, 0.0};
#endif

#endif
