//
//    rfnoc-hls-neuralnet: Vivado HLS code for neural-net building blocks
//
//    Copyright (C) 2017 EJ Kreinar
//
//    This program is free software: you can redistribute it and/or modify
//    it under the terms of the GNU General Public License as published by
//    the Free Software Foundation, either version 3 of the License, or
//    (at your option) any later version.
//
//    This program is distributed in the hope that it will be useful,
//    but WITHOUT ANY WARRANTY; without even the implied warranty of
//    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//    GNU General Public License for more details.
//
//    You should have received a copy of the GNU General Public License
//    along with this program.  If not, see <http://www.gnu.org/licenses/>.
//
#include <iostream>

#include "myproject.h"
#include "parameters.h"

void myproject(
    input_t qfc1_input[N_INPUT_1_1],
    result_t layer17_out[N_LAYER_14],
    unsigned short &const_size_in_1,
    unsigned short &const_size_out_1
) {

    //hls-fpga-machine-learning insert IO
    #pragma HLS ARRAY_RESHAPE variable=qfc1_input complete dim=0
    #pragma HLS ARRAY_PARTITION variable=layer17_out complete dim=0
    #pragma HLS INTERFACE ap_vld port=qfc1_input,layer17_out 
    #pragma HLS PIPELINE 

    const_size_in_1 = N_INPUT_1_1;
    const_size_out_1 = N_LAYER_14;

#ifndef __SYNTHESIS__
    static bool loaded_weights = false;
    if (!loaded_weights) {
        //hls-fpga-machine-learning insert load weights
        nnet::load_weights_from_txt<weight2_t, 1024>(w2, "w2.txt");
        nnet::load_weights_from_txt<bias2_t, 64>(b2, "b2.txt");
        nnet::load_weights_from_txt<model_default_t, 64>(s4, "s4.txt");
        nnet::load_weights_from_txt<model_default_t, 64>(b4, "b4.txt");
        nnet::load_weights_from_txt<weight6_t, 2048>(w6, "w6.txt");
        nnet::load_weights_from_txt<bias6_t, 32>(b6, "b6.txt");
        nnet::load_weights_from_txt<model_default_t, 32>(s8, "s8.txt");
        nnet::load_weights_from_txt<model_default_t, 32>(b8, "b8.txt");
        nnet::load_weights_from_txt<weight10_t, 1024>(w10, "w10.txt");
        nnet::load_weights_from_txt<bias10_t, 32>(b10, "b10.txt");
        nnet::load_weights_from_txt<model_default_t, 32>(s12, "s12.txt");
        nnet::load_weights_from_txt<model_default_t, 32>(b12, "b12.txt");
        nnet::load_weights_from_txt<weight14_t, 32>(w14, "w14.txt");
        nnet::load_weights_from_txt<bias14_t, 1>(b14, "b14.txt");
        nnet::load_weights_from_txt<model_default_t, 1>(s16, "s16.txt");
        nnet::load_weights_from_txt<model_default_t, 1>(b16, "b16.txt");
        loaded_weights = true;
    }
#endif

    // ****************************************
    // NETWORK INSTANTIATION
    // ****************************************

    //hls-fpga-machine-learning insert layers

    layer2_t layer2_out[N_LAYER_2];
    #pragma HLS ARRAY_PARTITION variable=layer2_out complete dim=0
    nnet::dense<input_t, layer2_t, config2>(qfc1_input, layer2_out, w2, b2); // qfc1
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer2_t>(layer2_out, "qfc1", N_LAYER_2);
#endif

    layer4_t layer4_out[N_LAYER_2];
    #pragma HLS ARRAY_PARTITION variable=layer4_out complete dim=0
    nnet::normalize<layer2_t, layer4_t, config4>(layer2_out, layer4_out, s4, b4); // bn0
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer4_t>(layer4_out, "bn0", N_LAYER_2);
#endif

    layer5_t layer5_out[N_LAYER_2];
    #pragma HLS ARRAY_PARTITION variable=layer5_out complete dim=0
    nnet::binary_tanh<layer4_t, layer5_t, binary_tanh_config5>(layer4_out, layer5_out); // q_act_1
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer5_t>(layer5_out, "q_act_1", N_LAYER_2);
#endif

    layer6_t layer6_out[N_LAYER_6];
    #pragma HLS ARRAY_PARTITION variable=layer6_out complete dim=0
    nnet::dense<layer5_t, layer6_t, config6>(layer5_out, layer6_out, w6, b6); // qfc2
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer6_t>(layer6_out, "qfc2", N_LAYER_6);
#endif

    layer8_t layer8_out[N_LAYER_6];
    #pragma HLS ARRAY_PARTITION variable=layer8_out complete dim=0
    nnet::normalize<layer6_t, layer8_t, config8>(layer6_out, layer8_out, s8, b8); // bn1
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer8_t>(layer8_out, "bn1", N_LAYER_6);
#endif

    layer9_t layer9_out[N_LAYER_6];
    #pragma HLS ARRAY_PARTITION variable=layer9_out complete dim=0
    nnet::binary_tanh<layer8_t, layer9_t, binary_tanh_config9>(layer8_out, layer9_out); // q_act_2
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer9_t>(layer9_out, "q_act_2", N_LAYER_6);
#endif

    layer10_t layer10_out[N_LAYER_10];
    #pragma HLS ARRAY_PARTITION variable=layer10_out complete dim=0
    nnet::dense<layer9_t, layer10_t, config10>(layer9_out, layer10_out, w10, b10); // qfc3
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer10_t>(layer10_out, "qfc3", N_LAYER_10);
#endif

    layer12_t layer12_out[N_LAYER_10];
    #pragma HLS ARRAY_PARTITION variable=layer12_out complete dim=0
    nnet::normalize<layer10_t, layer12_t, config12>(layer10_out, layer12_out, s12, b12); // bn2
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer12_t>(layer12_out, "bn2", N_LAYER_10);
#endif

    layer13_t layer13_out[N_LAYER_10];
    #pragma HLS ARRAY_PARTITION variable=layer13_out complete dim=0
    nnet::binary_tanh<layer12_t, layer13_t, binary_tanh_config13>(layer12_out, layer13_out); // q_act_3
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer13_t>(layer13_out, "q_act_3", N_LAYER_10);
#endif

    layer14_t layer14_out[N_LAYER_14];
    #pragma HLS ARRAY_PARTITION variable=layer14_out complete dim=0
    nnet::dense<layer13_t, layer14_t, config14>(layer13_out, layer14_out, w14, b14); // output
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer14_t>(layer14_out, "output", N_LAYER_14);
#endif

    layer16_t layer16_out[N_LAYER_14];
    #pragma HLS ARRAY_PARTITION variable=layer16_out complete dim=0
    nnet::normalize<layer14_t, layer16_t, config16>(layer14_out, layer16_out, s16, b16); // bnf
#ifndef __SYNTHESIS__
    nnet::save_layer_output<layer16_t>(layer16_out, "bnf", N_LAYER_14);
#endif

    nnet::sigmoid<layer16_t, result_t, sigmoid_config17>(layer16_out, layer17_out); // sigmoid
#ifndef __SYNTHESIS__
    nnet::save_layer_output<result_t>(layer17_out, "sigmoid", N_LAYER_14);
#endif

}
