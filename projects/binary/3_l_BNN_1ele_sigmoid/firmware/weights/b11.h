//Numpy array shape [1]
//Min -2.000000000000
//Max -2.000000000000
//Number of zeros 0

#ifndef B11_H_
#define B11_H_

#ifndef __SYNTHESIS__
bias11_t b11[1];
#else
bias11_t b11[1] = {-2.0};
#endif

#endif
