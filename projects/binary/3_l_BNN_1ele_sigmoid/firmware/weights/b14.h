//Numpy array shape [1]
//Min 0.000000000000
//Max 0.000000000000
//Number of zeros 1

#ifndef B14_H_
#define B14_H_

#ifndef __SYNTHESIS__
bias14_t b14[1];
#else
bias14_t b14[1] = {0};
#endif

#endif
