//Numpy array shape [32]
//Min 0.000000000000
//Max 0.000000000000
//Number of zeros 32

#ifndef B10_H_
#define B10_H_

#ifndef __SYNTHESIS__
bias10_t b10[32];
#else
bias10_t b10[32] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
#endif

#endif
