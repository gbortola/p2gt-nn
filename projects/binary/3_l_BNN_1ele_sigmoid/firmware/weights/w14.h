//Numpy array shape [32, 1]
//Min 0.000000000000
//Max 1.000000000000
//Number of zeros 14

#ifndef W14_H_
#define W14_H_

#ifndef __SYNTHESIS__
weight14_t w14[32];
#else
weight14_t w14[32] = {1, 0, 0, 0, 1, 0, 1, 0, 1, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 1, 1, 0, 0, 0, 0, 1};
#endif

#endif
