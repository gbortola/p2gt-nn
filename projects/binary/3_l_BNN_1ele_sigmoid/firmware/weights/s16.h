//Numpy array shape [1]
//Min 1.150898933411
//Max 1.150898933411
//Number of zeros 0

#ifndef S16_H_
#define S16_H_

#ifndef __SYNTHESIS__
model_default_t s16[1];
#else
model_default_t s16[1] = {1.1508989334106};
#endif

#endif
