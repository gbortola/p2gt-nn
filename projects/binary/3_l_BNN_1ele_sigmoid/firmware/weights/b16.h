//Numpy array shape [1]
//Min -4.474177360535
//Max -4.474177360535
//Number of zeros 0

#ifndef B16_H_
#define B16_H_

#ifndef __SYNTHESIS__
model_default_t b16[1];
#else
model_default_t b16[1] = {-4.4741773605347};
#endif

#endif
