#ifndef DEFINES_H_
#define DEFINES_H_

#include "ap_int.h"
#include "ap_fixed.h"
#include "nnet_utils/nnet_types.h"
#include <cstddef>
#include <cstdio>

//hls-fpga-machine-learning insert numbers
#define N_INPUT_1_1 16
#define N_LAYER_2 64
#define N_LAYER_6 32
#define N_LAYER_10 32
#define N_LAYER_14 1

//hls-fpga-machine-learning insert layer-precision
typedef ap_fixed<18,5> model_default_t;
typedef ap_fixed<16,6> input_t;
typedef ap_fixed<18,5> layer2_t;
typedef ap_uint<1> weight2_t;
typedef ap_uint<1> bias2_t;
typedef ap_fixed<18,5> layer4_t;
typedef ap_int<2> layer5_t;
typedef ap_fixed<18,5> layer6_t;
typedef ap_uint<1> weight6_t;
typedef ap_uint<1> bias6_t;
typedef ap_fixed<18,5> layer8_t;
typedef ap_int<2> layer9_t;
typedef ap_fixed<18,5> layer10_t;
typedef ap_uint<1> weight10_t;
typedef ap_uint<1> bias10_t;
typedef ap_fixed<18,5> layer12_t;
typedef ap_int<2> layer13_t;
typedef ap_fixed<18,5> layer14_t;
typedef ap_uint<1> weight14_t;
typedef ap_uint<1> bias14_t;
typedef ap_fixed<18,5> layer16_t;
typedef ap_ufixed<10,0> sigmoid_default_t;
typedef ap_ufixed<10,0> result_t;

#endif
