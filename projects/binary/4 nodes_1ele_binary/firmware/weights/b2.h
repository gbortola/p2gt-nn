//Numpy array shape [4]
//Min -0.390625000000
//Max 0.992187500000
//Number of zeros 1

#ifndef B2_H_
#define B2_H_

#ifndef __SYNTHESIS__
bias2_t b2[4];
#else
bias2_t b2[4] = {-0.3906250, 0.0000000, -0.2890625, 0.9921875};
#endif

#endif
