//Numpy array shape [4, 1]
//Min -2.000000000000
//Max 1.000000000000
//Number of zeros 1

#ifndef W5_H_
#define W5_H_

#ifndef __SYNTHESIS__
weight5_t w5[4];
#else
weight5_t w5[4] = {1.00, 0.00, 1.00, -2.00};
#endif

#endif
