//Numpy array shape [1]
//Min -1.000000000000
//Max -1.000000000000
//Number of zeros 0

#ifndef B5_H_
#define B5_H_

#ifndef __SYNTHESIS__
bias5_t b5[1];
#else
bias5_t b5[1] = {-1.0};
#endif

#endif
