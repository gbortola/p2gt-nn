//Numpy array shape [4, 1]
//Min -2.000000000000
//Max 2.000000000000
//Number of zeros 1

#ifndef W5_H_
#define W5_H_

#ifndef __SYNTHESIS__
weight5_t w5[4];
#else
weight5_t w5[4] = {-2.0, 0.0, 2.0, -1.5};
#endif

#endif
