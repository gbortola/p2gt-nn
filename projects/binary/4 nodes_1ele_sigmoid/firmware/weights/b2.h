//Numpy array shape [4]
//Min -0.640625000000
//Max 0.992187500000
//Number of zeros 0

#ifndef B2_H_
#define B2_H_

#ifndef __SYNTHESIS__
bias2_t b2[4];
#else
bias2_t b2[4] = {0.9921875, -0.2343750, -0.6406250, 0.9218750};
#endif

#endif
