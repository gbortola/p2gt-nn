#ifndef DEFINES_H_
#define DEFINES_H_

#include "ap_int.h"
#include "ap_fixed.h"
#include "nnet_utils/nnet_types.h"
#include <cstddef>
#include <cstdio>

//hls-fpga-machine-learning insert numbers
#define N_INPUT_1_1 26
#define N_LAYER_2 4
#define N_LAYER_5 1

//hls-fpga-machine-learning insert layer-precision
typedef ap_fixed<16,6> model_default_t;
typedef ap_fixed<16,6> input_t;
typedef ap_fixed<16,6> layer2_t;
typedef ap_fixed<8,1> weight2_t;
typedef ap_fixed<8,1> bias2_t;
typedef ap_ufixed<4,1> layer4_t;
typedef ap_fixed<16,6> layer5_t;
typedef ap_fixed<6,5> weight5_t;
typedef ap_fixed<4,2> bias5_t;
typedef ap_ufixed<1,0> sigmoid_default_t;
typedef ap_ufixed<1,0> result_t;

#endif
