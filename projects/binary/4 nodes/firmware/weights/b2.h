//Numpy array shape [4]
//Min -0.093750000000
//Max 0.109375000000
//Number of zeros 0

#ifndef B2_H_
#define B2_H_

#ifndef __SYNTHESIS__
bias2_t b2[4];
#else
bias2_t b2[4] = {-0.093750, 0.109375, 0.062500, 0.109375};
#endif

#endif
