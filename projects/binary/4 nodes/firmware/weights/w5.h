//Numpy array shape [4, 2]
//Min -0.562500000000
//Max 0.500000000000
//Number of zeros 2

#ifndef W5_H_
#define W5_H_

#ifndef __SYNTHESIS__
weight5_t w5[8];
#else
weight5_t w5[8] = {-0.2500, -0.4375, -0.5625, 0.3750, -0.1250, 0.0000, 0.0000, 0.5000};
#endif

#endif
