// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2020.1
// Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config10_s (
        ap_ready,
        data_11_V_read,
        data_12_V_read,
        data_13_V_read,
        data_17_V_read,
        data_20_V_read,
        data_21_V_read,
        data_26_V_read,
        data_30_V_read,
        ap_return_0,
        ap_return_1,
        ap_return_2,
        ap_return_3,
        ap_return_4,
        ap_return_5,
        ap_return_6,
        ap_return_7
);


output   ap_ready;
input  [15:0] data_11_V_read;
input  [15:0] data_12_V_read;
input  [15:0] data_13_V_read;
input  [15:0] data_17_V_read;
input  [15:0] data_20_V_read;
input  [15:0] data_21_V_read;
input  [15:0] data_26_V_read;
input  [15:0] data_30_V_read;
output  [2:0] ap_return_0;
output  [2:0] ap_return_1;
output  [2:0] ap_return_2;
output  [2:0] ap_return_3;
output  [2:0] ap_return_4;
output  [2:0] ap_return_5;
output  [2:0] ap_return_6;
output  [2:0] ap_return_7;

wire   [0:0] tmp_59_fu_140_p3;
wire   [2:0] zext_ln415_fu_148_p1;
wire   [2:0] trunc_ln_fu_122_p4;
wire   [2:0] add_ln415_fu_152_p2;
wire   [0:0] tmp_60_fu_158_p3;
wire   [0:0] tmp_58_fu_132_p3;
wire   [0:0] xor_ln416_fu_166_p2;
wire   [4:0] p_Result_10_s_fu_178_p4;
wire   [0:0] and_ln416_fu_172_p2;
wire   [0:0] icmp_ln879_fu_188_p2;
wire   [0:0] icmp_ln768_fu_194_p2;
wire   [0:0] select_ln777_fu_200_p3;
wire   [0:0] icmp_ln1494_11_fu_116_p2;
wire   [2:0] select_ln340_11_fu_208_p3;
wire   [0:0] tmp_62_fu_248_p3;
wire   [2:0] zext_ln415_17_fu_256_p1;
wire   [2:0] trunc_ln708_s_fu_230_p4;
wire   [2:0] add_ln415_17_fu_260_p2;
wire   [0:0] tmp_63_fu_266_p3;
wire   [0:0] tmp_61_fu_240_p3;
wire   [0:0] xor_ln416_17_fu_274_p2;
wire   [4:0] p_Result_10_1_fu_286_p4;
wire   [0:0] and_ln416_17_fu_280_p2;
wire   [0:0] icmp_ln879_17_fu_296_p2;
wire   [0:0] icmp_ln768_17_fu_302_p2;
wire   [0:0] select_ln777_17_fu_308_p3;
wire   [0:0] icmp_ln1494_12_fu_224_p2;
wire   [2:0] select_ln340_12_fu_316_p3;
wire   [0:0] tmp_65_fu_356_p3;
wire   [2:0] zext_ln415_18_fu_364_p1;
wire   [2:0] trunc_ln708_12_fu_338_p4;
wire   [2:0] add_ln415_18_fu_368_p2;
wire   [0:0] tmp_66_fu_374_p3;
wire   [0:0] tmp_64_fu_348_p3;
wire   [0:0] xor_ln416_18_fu_382_p2;
wire   [4:0] p_Result_10_2_fu_394_p4;
wire   [0:0] and_ln416_18_fu_388_p2;
wire   [0:0] icmp_ln879_18_fu_404_p2;
wire   [0:0] icmp_ln768_18_fu_410_p2;
wire   [0:0] select_ln777_18_fu_416_p3;
wire   [0:0] icmp_ln1494_13_fu_332_p2;
wire   [2:0] select_ln340_13_fu_424_p3;
wire   [0:0] tmp_68_fu_464_p3;
wire   [2:0] zext_ln415_19_fu_472_p1;
wire   [2:0] trunc_ln708_13_fu_446_p4;
wire   [2:0] add_ln415_19_fu_476_p2;
wire   [0:0] tmp_69_fu_482_p3;
wire   [0:0] tmp_67_fu_456_p3;
wire   [0:0] xor_ln416_19_fu_490_p2;
wire   [4:0] p_Result_10_3_fu_502_p4;
wire   [0:0] and_ln416_19_fu_496_p2;
wire   [0:0] icmp_ln879_19_fu_512_p2;
wire   [0:0] icmp_ln768_19_fu_518_p2;
wire   [0:0] select_ln777_19_fu_524_p3;
wire   [0:0] icmp_ln1494_fu_440_p2;
wire   [2:0] select_ln340_fu_532_p3;
wire   [0:0] tmp_71_fu_572_p3;
wire   [2:0] zext_ln415_20_fu_580_p1;
wire   [2:0] trunc_ln708_14_fu_554_p4;
wire   [2:0] add_ln415_20_fu_584_p2;
wire   [0:0] tmp_72_fu_590_p3;
wire   [0:0] tmp_70_fu_564_p3;
wire   [0:0] xor_ln416_20_fu_598_p2;
wire   [4:0] p_Result_10_4_fu_610_p4;
wire   [0:0] and_ln416_20_fu_604_p2;
wire   [0:0] icmp_ln879_20_fu_620_p2;
wire   [0:0] icmp_ln768_20_fu_626_p2;
wire   [0:0] select_ln777_20_fu_632_p3;
wire   [0:0] icmp_ln1494_17_fu_548_p2;
wire   [2:0] select_ln340_17_fu_640_p3;
wire   [0:0] tmp_74_fu_680_p3;
wire   [2:0] zext_ln415_21_fu_688_p1;
wire   [2:0] trunc_ln708_15_fu_662_p4;
wire   [2:0] add_ln415_21_fu_692_p2;
wire   [0:0] tmp_75_fu_698_p3;
wire   [0:0] tmp_73_fu_672_p3;
wire   [0:0] xor_ln416_21_fu_706_p2;
wire   [4:0] p_Result_10_5_fu_718_p4;
wire   [0:0] and_ln416_21_fu_712_p2;
wire   [0:0] icmp_ln879_21_fu_728_p2;
wire   [0:0] icmp_ln768_21_fu_734_p2;
wire   [0:0] select_ln777_21_fu_740_p3;
wire   [0:0] icmp_ln1494_18_fu_656_p2;
wire   [2:0] select_ln340_18_fu_748_p3;
wire   [0:0] tmp_77_fu_788_p3;
wire   [2:0] zext_ln415_22_fu_796_p1;
wire   [2:0] trunc_ln708_16_fu_770_p4;
wire   [2:0] add_ln415_22_fu_800_p2;
wire   [0:0] tmp_78_fu_806_p3;
wire   [0:0] tmp_76_fu_780_p3;
wire   [0:0] xor_ln416_22_fu_814_p2;
wire   [4:0] p_Result_10_6_fu_826_p4;
wire   [0:0] and_ln416_22_fu_820_p2;
wire   [0:0] icmp_ln879_22_fu_836_p2;
wire   [0:0] icmp_ln768_22_fu_842_p2;
wire   [0:0] select_ln777_22_fu_848_p3;
wire   [0:0] icmp_ln1494_19_fu_764_p2;
wire   [2:0] select_ln340_19_fu_856_p3;
wire   [0:0] tmp_80_fu_896_p3;
wire   [2:0] zext_ln415_23_fu_904_p1;
wire   [2:0] trunc_ln708_17_fu_878_p4;
wire   [2:0] add_ln415_23_fu_908_p2;
wire   [0:0] tmp_81_fu_914_p3;
wire   [0:0] tmp_79_fu_888_p3;
wire   [0:0] xor_ln416_23_fu_922_p2;
wire   [4:0] p_Result_10_7_fu_934_p4;
wire   [0:0] and_ln416_23_fu_928_p2;
wire   [0:0] icmp_ln879_23_fu_944_p2;
wire   [0:0] icmp_ln768_23_fu_950_p2;
wire   [0:0] select_ln777_23_fu_956_p3;
wire   [0:0] icmp_ln1494_20_fu_872_p2;
wire   [2:0] select_ln340_20_fu_964_p3;
wire   [2:0] select_ln1494_fu_216_p3;
wire   [2:0] select_ln1494_17_fu_324_p3;
wire   [2:0] select_ln1494_18_fu_432_p3;
wire   [2:0] select_ln1494_19_fu_540_p3;
wire   [2:0] select_ln1494_20_fu_648_p3;
wire   [2:0] select_ln1494_21_fu_756_p3;
wire   [2:0] select_ln1494_22_fu_864_p3;
wire   [2:0] select_ln1494_23_fu_972_p3;

assign add_ln415_17_fu_260_p2 = (zext_ln415_17_fu_256_p1 + trunc_ln708_s_fu_230_p4);

assign add_ln415_18_fu_368_p2 = (zext_ln415_18_fu_364_p1 + trunc_ln708_12_fu_338_p4);

assign add_ln415_19_fu_476_p2 = (zext_ln415_19_fu_472_p1 + trunc_ln708_13_fu_446_p4);

assign add_ln415_20_fu_584_p2 = (zext_ln415_20_fu_580_p1 + trunc_ln708_14_fu_554_p4);

assign add_ln415_21_fu_692_p2 = (zext_ln415_21_fu_688_p1 + trunc_ln708_15_fu_662_p4);

assign add_ln415_22_fu_800_p2 = (zext_ln415_22_fu_796_p1 + trunc_ln708_16_fu_770_p4);

assign add_ln415_23_fu_908_p2 = (zext_ln415_23_fu_904_p1 + trunc_ln708_17_fu_878_p4);

assign add_ln415_fu_152_p2 = (zext_ln415_fu_148_p1 + trunc_ln_fu_122_p4);

assign and_ln416_17_fu_280_p2 = (xor_ln416_17_fu_274_p2 & tmp_61_fu_240_p3);

assign and_ln416_18_fu_388_p2 = (xor_ln416_18_fu_382_p2 & tmp_64_fu_348_p3);

assign and_ln416_19_fu_496_p2 = (xor_ln416_19_fu_490_p2 & tmp_67_fu_456_p3);

assign and_ln416_20_fu_604_p2 = (xor_ln416_20_fu_598_p2 & tmp_70_fu_564_p3);

assign and_ln416_21_fu_712_p2 = (xor_ln416_21_fu_706_p2 & tmp_73_fu_672_p3);

assign and_ln416_22_fu_820_p2 = (xor_ln416_22_fu_814_p2 & tmp_76_fu_780_p3);

assign and_ln416_23_fu_928_p2 = (xor_ln416_23_fu_922_p2 & tmp_79_fu_888_p3);

assign and_ln416_fu_172_p2 = (xor_ln416_fu_166_p2 & tmp_58_fu_132_p3);

assign ap_ready = 1'b1;

assign ap_return_0 = select_ln1494_fu_216_p3;

assign ap_return_1 = select_ln1494_17_fu_324_p3;

assign ap_return_2 = select_ln1494_18_fu_432_p3;

assign ap_return_3 = select_ln1494_19_fu_540_p3;

assign ap_return_4 = select_ln1494_20_fu_648_p3;

assign ap_return_5 = select_ln1494_21_fu_756_p3;

assign ap_return_6 = select_ln1494_22_fu_864_p3;

assign ap_return_7 = select_ln1494_23_fu_972_p3;

assign icmp_ln1494_11_fu_116_p2 = (($signed(data_11_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln1494_12_fu_224_p2 = (($signed(data_12_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln1494_13_fu_332_p2 = (($signed(data_13_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln1494_17_fu_548_p2 = (($signed(data_20_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln1494_18_fu_656_p2 = (($signed(data_21_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln1494_19_fu_764_p2 = (($signed(data_26_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln1494_20_fu_872_p2 = (($signed(data_30_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln1494_fu_440_p2 = (($signed(data_17_V_read) > $signed(16'd0)) ? 1'b1 : 1'b0);

assign icmp_ln768_17_fu_302_p2 = ((p_Result_10_1_fu_286_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln768_18_fu_410_p2 = ((p_Result_10_2_fu_394_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln768_19_fu_518_p2 = ((p_Result_10_3_fu_502_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln768_20_fu_626_p2 = ((p_Result_10_4_fu_610_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln768_21_fu_734_p2 = ((p_Result_10_5_fu_718_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln768_22_fu_842_p2 = ((p_Result_10_6_fu_826_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln768_23_fu_950_p2 = ((p_Result_10_7_fu_934_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln768_fu_194_p2 = ((p_Result_10_s_fu_178_p4 == 5'd0) ? 1'b1 : 1'b0);

assign icmp_ln879_17_fu_296_p2 = ((p_Result_10_1_fu_286_p4 == 5'd31) ? 1'b1 : 1'b0);

assign icmp_ln879_18_fu_404_p2 = ((p_Result_10_2_fu_394_p4 == 5'd31) ? 1'b1 : 1'b0);

assign icmp_ln879_19_fu_512_p2 = ((p_Result_10_3_fu_502_p4 == 5'd31) ? 1'b1 : 1'b0);

assign icmp_ln879_20_fu_620_p2 = ((p_Result_10_4_fu_610_p4 == 5'd31) ? 1'b1 : 1'b0);

assign icmp_ln879_21_fu_728_p2 = ((p_Result_10_5_fu_718_p4 == 5'd31) ? 1'b1 : 1'b0);

assign icmp_ln879_22_fu_836_p2 = ((p_Result_10_6_fu_826_p4 == 5'd31) ? 1'b1 : 1'b0);

assign icmp_ln879_23_fu_944_p2 = ((p_Result_10_7_fu_934_p4 == 5'd31) ? 1'b1 : 1'b0);

assign icmp_ln879_fu_188_p2 = ((p_Result_10_s_fu_178_p4 == 5'd31) ? 1'b1 : 1'b0);

assign p_Result_10_1_fu_286_p4 = {{data_12_V_read[15:11]}};

assign p_Result_10_2_fu_394_p4 = {{data_13_V_read[15:11]}};

assign p_Result_10_3_fu_502_p4 = {{data_17_V_read[15:11]}};

assign p_Result_10_4_fu_610_p4 = {{data_20_V_read[15:11]}};

assign p_Result_10_5_fu_718_p4 = {{data_21_V_read[15:11]}};

assign p_Result_10_6_fu_826_p4 = {{data_26_V_read[15:11]}};

assign p_Result_10_7_fu_934_p4 = {{data_30_V_read[15:11]}};

assign p_Result_10_s_fu_178_p4 = {{data_11_V_read[15:11]}};

assign select_ln1494_17_fu_324_p3 = ((icmp_ln1494_12_fu_224_p2[0:0] === 1'b1) ? select_ln340_12_fu_316_p3 : 3'd0);

assign select_ln1494_18_fu_432_p3 = ((icmp_ln1494_13_fu_332_p2[0:0] === 1'b1) ? select_ln340_13_fu_424_p3 : 3'd0);

assign select_ln1494_19_fu_540_p3 = ((icmp_ln1494_fu_440_p2[0:0] === 1'b1) ? select_ln340_fu_532_p3 : 3'd0);

assign select_ln1494_20_fu_648_p3 = ((icmp_ln1494_17_fu_548_p2[0:0] === 1'b1) ? select_ln340_17_fu_640_p3 : 3'd0);

assign select_ln1494_21_fu_756_p3 = ((icmp_ln1494_18_fu_656_p2[0:0] === 1'b1) ? select_ln340_18_fu_748_p3 : 3'd0);

assign select_ln1494_22_fu_864_p3 = ((icmp_ln1494_19_fu_764_p2[0:0] === 1'b1) ? select_ln340_19_fu_856_p3 : 3'd0);

assign select_ln1494_23_fu_972_p3 = ((icmp_ln1494_20_fu_872_p2[0:0] === 1'b1) ? select_ln340_20_fu_964_p3 : 3'd0);

assign select_ln1494_fu_216_p3 = ((icmp_ln1494_11_fu_116_p2[0:0] === 1'b1) ? select_ln340_11_fu_208_p3 : 3'd0);

assign select_ln340_11_fu_208_p3 = ((select_ln777_fu_200_p3[0:0] === 1'b1) ? add_ln415_fu_152_p2 : 3'd7);

assign select_ln340_12_fu_316_p3 = ((select_ln777_17_fu_308_p3[0:0] === 1'b1) ? add_ln415_17_fu_260_p2 : 3'd7);

assign select_ln340_13_fu_424_p3 = ((select_ln777_18_fu_416_p3[0:0] === 1'b1) ? add_ln415_18_fu_368_p2 : 3'd7);

assign select_ln340_17_fu_640_p3 = ((select_ln777_20_fu_632_p3[0:0] === 1'b1) ? add_ln415_20_fu_584_p2 : 3'd7);

assign select_ln340_18_fu_748_p3 = ((select_ln777_21_fu_740_p3[0:0] === 1'b1) ? add_ln415_21_fu_692_p2 : 3'd7);

assign select_ln340_19_fu_856_p3 = ((select_ln777_22_fu_848_p3[0:0] === 1'b1) ? add_ln415_22_fu_800_p2 : 3'd7);

assign select_ln340_20_fu_964_p3 = ((select_ln777_23_fu_956_p3[0:0] === 1'b1) ? add_ln415_23_fu_908_p2 : 3'd7);

assign select_ln340_fu_532_p3 = ((select_ln777_19_fu_524_p3[0:0] === 1'b1) ? add_ln415_19_fu_476_p2 : 3'd7);

assign select_ln777_17_fu_308_p3 = ((and_ln416_17_fu_280_p2[0:0] === 1'b1) ? icmp_ln879_17_fu_296_p2 : icmp_ln768_17_fu_302_p2);

assign select_ln777_18_fu_416_p3 = ((and_ln416_18_fu_388_p2[0:0] === 1'b1) ? icmp_ln879_18_fu_404_p2 : icmp_ln768_18_fu_410_p2);

assign select_ln777_19_fu_524_p3 = ((and_ln416_19_fu_496_p2[0:0] === 1'b1) ? icmp_ln879_19_fu_512_p2 : icmp_ln768_19_fu_518_p2);

assign select_ln777_20_fu_632_p3 = ((and_ln416_20_fu_604_p2[0:0] === 1'b1) ? icmp_ln879_20_fu_620_p2 : icmp_ln768_20_fu_626_p2);

assign select_ln777_21_fu_740_p3 = ((and_ln416_21_fu_712_p2[0:0] === 1'b1) ? icmp_ln879_21_fu_728_p2 : icmp_ln768_21_fu_734_p2);

assign select_ln777_22_fu_848_p3 = ((and_ln416_22_fu_820_p2[0:0] === 1'b1) ? icmp_ln879_22_fu_836_p2 : icmp_ln768_22_fu_842_p2);

assign select_ln777_23_fu_956_p3 = ((and_ln416_23_fu_928_p2[0:0] === 1'b1) ? icmp_ln879_23_fu_944_p2 : icmp_ln768_23_fu_950_p2);

assign select_ln777_fu_200_p3 = ((and_ln416_fu_172_p2[0:0] === 1'b1) ? icmp_ln879_fu_188_p2 : icmp_ln768_fu_194_p2);

assign tmp_58_fu_132_p3 = data_11_V_read[32'd10];

assign tmp_59_fu_140_p3 = data_11_V_read[32'd7];

assign tmp_60_fu_158_p3 = add_ln415_fu_152_p2[32'd2];

assign tmp_61_fu_240_p3 = data_12_V_read[32'd10];

assign tmp_62_fu_248_p3 = data_12_V_read[32'd7];

assign tmp_63_fu_266_p3 = add_ln415_17_fu_260_p2[32'd2];

assign tmp_64_fu_348_p3 = data_13_V_read[32'd10];

assign tmp_65_fu_356_p3 = data_13_V_read[32'd7];

assign tmp_66_fu_374_p3 = add_ln415_18_fu_368_p2[32'd2];

assign tmp_67_fu_456_p3 = data_17_V_read[32'd10];

assign tmp_68_fu_464_p3 = data_17_V_read[32'd7];

assign tmp_69_fu_482_p3 = add_ln415_19_fu_476_p2[32'd2];

assign tmp_70_fu_564_p3 = data_20_V_read[32'd10];

assign tmp_71_fu_572_p3 = data_20_V_read[32'd7];

assign tmp_72_fu_590_p3 = add_ln415_20_fu_584_p2[32'd2];

assign tmp_73_fu_672_p3 = data_21_V_read[32'd10];

assign tmp_74_fu_680_p3 = data_21_V_read[32'd7];

assign tmp_75_fu_698_p3 = add_ln415_21_fu_692_p2[32'd2];

assign tmp_76_fu_780_p3 = data_26_V_read[32'd10];

assign tmp_77_fu_788_p3 = data_26_V_read[32'd7];

assign tmp_78_fu_806_p3 = add_ln415_22_fu_800_p2[32'd2];

assign tmp_79_fu_888_p3 = data_30_V_read[32'd10];

assign tmp_80_fu_896_p3 = data_30_V_read[32'd7];

assign tmp_81_fu_914_p3 = add_ln415_23_fu_908_p2[32'd2];

assign trunc_ln708_12_fu_338_p4 = {{data_13_V_read[10:8]}};

assign trunc_ln708_13_fu_446_p4 = {{data_17_V_read[10:8]}};

assign trunc_ln708_14_fu_554_p4 = {{data_20_V_read[10:8]}};

assign trunc_ln708_15_fu_662_p4 = {{data_21_V_read[10:8]}};

assign trunc_ln708_16_fu_770_p4 = {{data_26_V_read[10:8]}};

assign trunc_ln708_17_fu_878_p4 = {{data_30_V_read[10:8]}};

assign trunc_ln708_s_fu_230_p4 = {{data_12_V_read[10:8]}};

assign trunc_ln_fu_122_p4 = {{data_11_V_read[10:8]}};

assign xor_ln416_17_fu_274_p2 = (tmp_63_fu_266_p3 ^ 1'd1);

assign xor_ln416_18_fu_382_p2 = (tmp_66_fu_374_p3 ^ 1'd1);

assign xor_ln416_19_fu_490_p2 = (tmp_69_fu_482_p3 ^ 1'd1);

assign xor_ln416_20_fu_598_p2 = (tmp_72_fu_590_p3 ^ 1'd1);

assign xor_ln416_21_fu_706_p2 = (tmp_75_fu_698_p3 ^ 1'd1);

assign xor_ln416_22_fu_814_p2 = (tmp_78_fu_806_p3 ^ 1'd1);

assign xor_ln416_23_fu_922_p2 = (tmp_81_fu_914_p3 ^ 1'd1);

assign xor_ln416_fu_166_p2 = (tmp_60_fu_158_p3 ^ 1'd1);

assign zext_ln415_17_fu_256_p1 = tmp_62_fu_248_p3;

assign zext_ln415_18_fu_364_p1 = tmp_65_fu_356_p3;

assign zext_ln415_19_fu_472_p1 = tmp_68_fu_464_p3;

assign zext_ln415_20_fu_580_p1 = tmp_71_fu_572_p3;

assign zext_ln415_21_fu_688_p1 = tmp_74_fu_680_p3;

assign zext_ln415_22_fu_796_p1 = tmp_77_fu_788_p3;

assign zext_ln415_23_fu_904_p1 = tmp_80_fu_896_p3;

assign zext_ln415_fu_148_p1 = tmp_59_fu_140_p3;

endmodule //relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config10_s
