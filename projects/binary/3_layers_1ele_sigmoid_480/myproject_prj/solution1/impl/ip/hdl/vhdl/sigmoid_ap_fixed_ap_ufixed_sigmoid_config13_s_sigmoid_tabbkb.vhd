-- ==============================================================
-- Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2020.1 (64-bit)
-- Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
-- ==============================================================
library ieee; 
use ieee.std_logic_1164.all; 
use ieee.std_logic_unsigned.all;

entity sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_rom is 
    generic(
             DWIDTH     : integer := 8; 
             AWIDTH     : integer := 9; 
             MEM_SIZE    : integer := 512
    ); 
    port (
          addr0      : in std_logic_vector(AWIDTH-1 downto 0); 
          ce0       : in std_logic; 
          q0         : out std_logic_vector(DWIDTH-1 downto 0);
          clk       : in std_logic
    ); 
end entity; 


architecture rtl of sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_rom is 

signal addr0_tmp : std_logic_vector(AWIDTH-1 downto 0); 
type mem_array is array (0 to MEM_SIZE-1) of std_logic_vector (DWIDTH-1 downto 0); 
signal mem : mem_array := (
    0 to 78=> "00000000", 79 to 100=> "00000001", 101 to 114=> "00000010", 115 to 123=> "00000011", 
    124 to 130=> "00000100", 131 to 136=> "00000101", 137 to 141=> "00000110", 142 to 146=> "00000111", 
    147 to 150=> "00001000", 151 to 153=> "00001001", 154 to 156=> "00001010", 157 to 159=> "00001011", 
    160 to 162=> "00001100", 163 to 164=> "00001101", 165 to 167=> "00001110", 168 to 169=> "00001111", 
    170 to 171=> "00010000", 172 to 173=> "00010001", 174 to 175=> "00010010", 176 to 177=> "00010011", 
    178 => "00010100", 179 to 180=> "00010101", 181 => "00010110", 182 to 183=> "00010111", 
    184 => "00011000", 185 to 186=> "00011001", 187 => "00011010", 188 => "00011011", 
    189 to 190=> "00011100", 191 => "00011101", 192 => "00011110", 193 => "00011111", 
    194 => "00100000", 195 => "00100001", 196 to 197=> "00100010", 198 => "00100011", 
    199 => "00100100", 200 => "00100101", 201 => "00100110", 202 => "00100111", 
    203 => "00101001", 204 => "00101010", 205 => "00101011", 206 => "00101100", 
    207 => "00101101", 208 => "00101110", 209 => "00101111", 210 => "00110001", 
    211 => "00110010", 212 => "00110011", 213 => "00110100", 214 => "00110110", 
    215 => "00110111", 216 => "00111001", 217 => "00111010", 218 => "00111011", 
    219 => "00111101", 220 => "00111110", 221 => "01000000", 222 => "01000001", 
    223 => "01000011", 224 => "01000100", 225 => "01000110", 226 => "01001000", 
    227 => "01001001", 228 => "01001011", 229 => "01001100", 230 => "01001110", 
    231 => "01010000", 232 => "01010010", 233 => "01010011", 234 => "01010101", 
    235 => "01010111", 236 => "01011001", 237 => "01011011", 238 => "01011100", 
    239 => "01011110", 240 => "01100000", 241 => "01100010", 242 => "01100100", 
    243 => "01100110", 244 => "01101000", 245 => "01101010", 246 => "01101100", 
    247 => "01101110", 248 => "01110000", 249 => "01110010", 250 => "01110100", 
    251 => "01110110", 252 => "01111000", 253 => "01111010", 254 => "01111100", 
    255 => "01111110", 256 => "10000000", 257 => "10000001", 258 => "10000011", 
    259 => "10000101", 260 => "10000111", 261 => "10001001", 262 => "10001011", 
    263 => "10001101", 264 => "10001111", 265 => "10010001", 266 => "10010011", 
    267 => "10010101", 268 => "10010111", 269 => "10011001", 270 => "10011011", 
    271 => "10011101", 272 => "10011111", 273 => "10100001", 274 => "10100011", 
    275 => "10100100", 276 => "10100110", 277 => "10101000", 278 => "10101010", 
    279 => "10101100", 280 => "10101101", 281 => "10101111", 282 => "10110001", 
    283 => "10110011", 284 => "10110100", 285 => "10110110", 286 => "10110111", 
    287 => "10111001", 288 => "10111011", 289 => "10111100", 290 => "10111110", 
    291 => "10111111", 292 => "11000001", 293 => "11000010", 294 => "11000100", 
    295 => "11000101", 296 => "11000110", 297 => "11001000", 298 => "11001001", 
    299 => "11001011", 300 => "11001100", 301 => "11001101", 302 => "11001110", 
    303 => "11010000", 304 => "11010001", 305 => "11010010", 306 => "11010011", 
    307 => "11010100", 308 => "11010101", 309 => "11010110", 310 => "11011000", 
    311 => "11011001", 312 => "11011010", 313 => "11011011", 314 => "11011100", 
    315 to 316=> "11011101", 317 => "11011110", 318 => "11011111", 319 => "11100000", 
    320 => "11100001", 321 => "11100010", 322 to 323=> "11100011", 324 => "11100100", 
    325 => "11100101", 326 to 327=> "11100110", 328 => "11100111", 329 to 330=> "11101000", 
    331 => "11101001", 332 to 333=> "11101010", 334 => "11101011", 335 to 336=> "11101100", 
    337 to 338=> "11101101", 339 to 340=> "11101110", 341 to 342=> "11101111", 343 to 344=> "11110000", 
    345 to 347=> "11110001", 348 to 349=> "11110010", 350 to 352=> "11110011", 353 to 355=> "11110100", 
    356 to 358=> "11110101", 359 to 361=> "11110110", 362 to 365=> "11110111", 366 to 370=> "11111000", 
    371 to 375=> "11111001", 376 to 381=> "11111010", 382 to 388=> "11111011", 389 to 397=> "11111100", 
    398 to 411=> "11111101", 412 to 433=> "11111110", 434 to 511=> "11111111" );


begin 


memory_access_guard_0: process (addr0) 
begin
      addr0_tmp <= addr0;
--synthesis translate_off
      if (CONV_INTEGER(addr0) > mem_size-1) then
           addr0_tmp <= (others => '0');
      else 
           addr0_tmp <= addr0;
      end if;
--synthesis translate_on
end process;

p_rom_access: process (clk)  
begin 
    if (clk'event and clk = '1') then
        if (ce0 = '1') then 
            q0 <= mem(CONV_INTEGER(addr0_tmp)); 
        end if;
    end if;
end process;

end rtl;

Library IEEE;
use IEEE.std_logic_1164.all;

entity sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb is
    generic (
        DataWidth : INTEGER := 8;
        AddressRange : INTEGER := 512;
        AddressWidth : INTEGER := 9);
    port (
        reset : IN STD_LOGIC;
        clk : IN STD_LOGIC;
        address0 : IN STD_LOGIC_VECTOR(AddressWidth - 1 DOWNTO 0);
        ce0 : IN STD_LOGIC;
        q0 : OUT STD_LOGIC_VECTOR(DataWidth - 1 DOWNTO 0));
end entity;

architecture arch of sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb is
    component sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_rom is
        port (
            clk : IN STD_LOGIC;
            addr0 : IN STD_LOGIC_VECTOR;
            ce0 : IN STD_LOGIC;
            q0 : OUT STD_LOGIC_VECTOR);
    end component;



begin
    sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_rom_U :  component sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_rom
    port map (
        clk => clk,
        addr0 => address0,
        ce0 => ce0,
        q0 => q0);

end architecture;


