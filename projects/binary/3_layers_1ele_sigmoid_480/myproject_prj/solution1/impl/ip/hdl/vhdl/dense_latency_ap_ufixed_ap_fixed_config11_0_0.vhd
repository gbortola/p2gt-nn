-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
-- Version: 2020.1
-- Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dense_latency_ap_ufixed_ap_fixed_config11_0_0 is
port (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    data_11_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_12_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_13_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_17_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_20_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_21_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_26_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_30_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    ap_return : OUT STD_LOGIC_VECTOR (15 downto 0);
    ap_ce : IN STD_LOGIC );
end;


architecture behav of dense_latency_ap_ufixed_ap_fixed_config11_0_0 is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_const_boolean_1 : BOOLEAN := true;
    constant ap_const_boolean_0 : BOOLEAN := false;
    constant ap_const_lv2_0 : STD_LOGIC_VECTOR (1 downto 0) := "00";
    constant ap_const_lv4_0 : STD_LOGIC_VECTOR (3 downto 0) := "0000";
    constant ap_const_lv7_0 : STD_LOGIC_VECTOR (6 downto 0) := "0000000";
    constant ap_const_lv8_0 : STD_LOGIC_VECTOR (7 downto 0) := "00000000";
    constant ap_const_lv13_1F00 : STD_LOGIC_VECTOR (12 downto 0) := "1111100000000";

    signal data_26_V_read11_reg_387 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_block_state1_pp0_stage0_iter0 : BOOLEAN;
    signal ap_block_state2_pp0_stage0_iter1 : BOOLEAN;
    signal ap_block_state3_pp0_stage0_iter2 : BOOLEAN;
    signal ap_block_pp0_stage0_11001 : BOOLEAN;
    signal data_21_V_read_3_reg_393 : STD_LOGIC_VECTOR (2 downto 0);
    signal data_20_V_read_3_reg_399 : STD_LOGIC_VECTOR (2 downto 0);
    signal data_13_V_read_3_reg_404 : STD_LOGIC_VECTOR (2 downto 0);
    signal sub_ln1118_1_fu_126_p2 : STD_LOGIC_VECTOR (5 downto 0);
    signal sub_ln1118_1_reg_409 : STD_LOGIC_VECTOR (5 downto 0);
    signal sub_ln1118_fu_136_p2 : STD_LOGIC_VECTOR (3 downto 0);
    signal sub_ln1118_reg_414 : STD_LOGIC_VECTOR (3 downto 0);
    signal sub_ln1118_3_fu_158_p2 : STD_LOGIC_VECTOR (5 downto 0);
    signal sub_ln1118_3_reg_419 : STD_LOGIC_VECTOR (5 downto 0);
    signal sub_ln1118_6_fu_180_p2 : STD_LOGIC_VECTOR (5 downto 0);
    signal sub_ln1118_6_reg_424 : STD_LOGIC_VECTOR (5 downto 0);
    signal add_ln703_2_fu_334_p2 : STD_LOGIC_VECTOR (13 downto 0);
    signal add_ln703_2_reg_429 : STD_LOGIC_VECTOR (13 downto 0);
    signal add_ln703_3_fu_340_p2 : STD_LOGIC_VECTOR (13 downto 0);
    signal add_ln703_3_reg_434 : STD_LOGIC_VECTOR (13 downto 0);
    signal add_ln703_5_fu_356_p2 : STD_LOGIC_VECTOR (13 downto 0);
    signal add_ln703_5_reg_439 : STD_LOGIC_VECTOR (13 downto 0);
    signal ap_block_pp0_stage0 : BOOLEAN;
    signal tmp_1_fu_114_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal zext_ln1118_fu_110_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal zext_ln1118_1_fu_122_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal zext_ln1118_2_fu_132_p1 : STD_LOGIC_VECTOR (3 downto 0);
    signal tmp_2_fu_146_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal zext_ln1118_3_fu_142_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal zext_ln1118_4_fu_154_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal tmp_5_fu_168_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal zext_ln1118_10_fu_164_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal zext_ln1118_11_fu_176_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal shl_ln728_1_fu_193_p3 : STD_LOGIC_VECTOR (10 downto 0);
    signal shl_ln728_2_fu_204_p3 : STD_LOGIC_VECTOR (10 downto 0);
    signal zext_ln1118_5_fu_222_p1 : STD_LOGIC_VECTOR (3 downto 0);
    signal sub_ln1118_2_fu_225_p2 : STD_LOGIC_VECTOR (3 downto 0);
    signal shl_ln728_4_fu_231_p3 : STD_LOGIC_VECTOR (10 downto 0);
    signal shl_ln1118_s_fu_246_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal zext_ln1118_7_fu_253_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal zext_ln1118_6_fu_243_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal sub_ln1118_4_fu_257_p2 : STD_LOGIC_VECTOR (5 downto 0);
    signal tmp_3_fu_263_p3 : STD_LOGIC_VECTOR (12 downto 0);
    signal shl_ln1118_1_fu_278_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal zext_ln1118_9_fu_285_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal zext_ln1118_8_fu_275_p1 : STD_LOGIC_VECTOR (5 downto 0);
    signal sub_ln1118_5_fu_289_p2 : STD_LOGIC_VECTOR (5 downto 0);
    signal tmp_4_fu_295_p3 : STD_LOGIC_VECTOR (12 downto 0);
    signal sext_ln728_fu_200_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal shl_ln_fu_186_p3 : STD_LOGIC_VECTOR (12 downto 0);
    signal add_ln703_fu_314_p2 : STD_LOGIC_VECTOR (12 downto 0);
    signal shl_ln728_3_fu_215_p3 : STD_LOGIC_VECTOR (12 downto 0);
    signal zext_ln728_fu_211_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal add_ln703_1_fu_324_p2 : STD_LOGIC_VECTOR (12 downto 0);
    signal sext_ln703_fu_320_p1 : STD_LOGIC_VECTOR (13 downto 0);
    signal sext_ln703_1_fu_330_p1 : STD_LOGIC_VECTOR (13 downto 0);
    signal sext_ln1118_1_fu_271_p1 : STD_LOGIC_VECTOR (13 downto 0);
    signal sext_ln1118_fu_239_p1 : STD_LOGIC_VECTOR (13 downto 0);
    signal shl_ln728_7_fu_307_p3 : STD_LOGIC_VECTOR (12 downto 0);
    signal add_ln703_4_fu_346_p2 : STD_LOGIC_VECTOR (12 downto 0);
    signal sext_ln1118_2_fu_303_p1 : STD_LOGIC_VECTOR (13 downto 0);
    signal sext_ln703_4_fu_352_p1 : STD_LOGIC_VECTOR (13 downto 0);
    signal sext_ln703_3_fu_365_p1 : STD_LOGIC_VECTOR (14 downto 0);
    signal sext_ln703_5_fu_368_p1 : STD_LOGIC_VECTOR (14 downto 0);
    signal add_ln703_6_fu_371_p2 : STD_LOGIC_VECTOR (14 downto 0);
    signal sext_ln703_2_fu_362_p1 : STD_LOGIC_VECTOR (15 downto 0);
    signal sext_ln703_6_fu_377_p1 : STD_LOGIC_VECTOR (15 downto 0);
    signal add_ln703_7_fu_381_p2 : STD_LOGIC_VECTOR (15 downto 0);
    signal ap_ce_reg : STD_LOGIC;
    signal data_11_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal data_12_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal data_13_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal data_17_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal data_20_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal data_21_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal data_26_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal data_30_V_read_int_reg : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_return_int_reg : STD_LOGIC_VECTOR (15 downto 0);


begin




    ap_ce_reg_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            ap_ce_reg <= ap_ce;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_boolean_0 = ap_block_pp0_stage0_11001) and (ap_const_logic_1 = ap_ce_reg))) then
                    add_ln703_2_reg_429(13 downto 7) <= add_ln703_2_fu_334_p2(13 downto 7);
                    add_ln703_3_reg_434(13 downto 7) <= add_ln703_3_fu_340_p2(13 downto 7);
                    add_ln703_5_reg_439(13 downto 7) <= add_ln703_5_fu_356_p2(13 downto 7);
                data_13_V_read_3_reg_404 <= data_13_V_read_int_reg;
                data_20_V_read_3_reg_399 <= data_20_V_read_int_reg;
                data_21_V_read_3_reg_393 <= data_21_V_read_int_reg;
                data_26_V_read11_reg_387 <= data_26_V_read_int_reg;
                sub_ln1118_1_reg_409 <= sub_ln1118_1_fu_126_p2;
                sub_ln1118_3_reg_419 <= sub_ln1118_3_fu_158_p2;
                sub_ln1118_6_reg_424 <= sub_ln1118_6_fu_180_p2;
                sub_ln1118_reg_414 <= sub_ln1118_fu_136_p2;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_const_logic_1 = ap_ce_reg)) then
                    ap_return_int_reg(15 downto 7) <= add_ln703_7_fu_381_p2(15 downto 7);
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_const_logic_1 = ap_ce)) then
                data_11_V_read_int_reg <= data_11_V_read;
                data_12_V_read_int_reg <= data_12_V_read;
                data_13_V_read_int_reg <= data_13_V_read;
                data_17_V_read_int_reg <= data_17_V_read;
                data_20_V_read_int_reg <= data_20_V_read;
                data_21_V_read_int_reg <= data_21_V_read;
                data_26_V_read_int_reg <= data_26_V_read;
                data_30_V_read_int_reg <= data_30_V_read;
            end if;
        end if;
    end process;
    add_ln703_2_reg_429(6 downto 0) <= "0000000";
    add_ln703_3_reg_434(6 downto 0) <= "0000000";
    add_ln703_5_reg_439(6 downto 0) <= "0000000";
    ap_return_int_reg(6 downto 0) <= "0000000";
    add_ln703_1_fu_324_p2 <= std_logic_vector(unsigned(shl_ln728_3_fu_215_p3) + unsigned(zext_ln728_fu_211_p1));
    add_ln703_2_fu_334_p2 <= std_logic_vector(signed(sext_ln703_fu_320_p1) + signed(sext_ln703_1_fu_330_p1));
    add_ln703_3_fu_340_p2 <= std_logic_vector(signed(sext_ln1118_1_fu_271_p1) + signed(sext_ln1118_fu_239_p1));
    add_ln703_4_fu_346_p2 <= std_logic_vector(unsigned(shl_ln728_7_fu_307_p3) + unsigned(ap_const_lv13_1F00));
    add_ln703_5_fu_356_p2 <= std_logic_vector(signed(sext_ln1118_2_fu_303_p1) + signed(sext_ln703_4_fu_352_p1));
    add_ln703_6_fu_371_p2 <= std_logic_vector(signed(sext_ln703_3_fu_365_p1) + signed(sext_ln703_5_fu_368_p1));
    add_ln703_7_fu_381_p2 <= std_logic_vector(signed(sext_ln703_2_fu_362_p1) + signed(sext_ln703_6_fu_377_p1));
    add_ln703_fu_314_p2 <= std_logic_vector(signed(sext_ln728_fu_200_p1) + signed(shl_ln_fu_186_p3));
        ap_block_pp0_stage0 <= not((ap_const_boolean_1 = ap_const_boolean_1));
        ap_block_pp0_stage0_11001 <= not((ap_const_boolean_1 = ap_const_boolean_1));
        ap_block_state1_pp0_stage0_iter0 <= not((ap_const_boolean_1 = ap_const_boolean_1));
        ap_block_state2_pp0_stage0_iter1 <= not((ap_const_boolean_1 = ap_const_boolean_1));
        ap_block_state3_pp0_stage0_iter2 <= not((ap_const_boolean_1 = ap_const_boolean_1));

    ap_return_assign_proc : process(add_ln703_7_fu_381_p2, ap_ce_reg, ap_return_int_reg)
    begin
        if ((ap_const_logic_0 = ap_ce_reg)) then 
            ap_return <= ap_return_int_reg;
        elsif ((ap_const_logic_1 = ap_ce_reg)) then 
            ap_return <= add_ln703_7_fu_381_p2;
        end if; 
    end process;

        sext_ln1118_1_fu_271_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(tmp_3_fu_263_p3),14));

        sext_ln1118_2_fu_303_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(tmp_4_fu_295_p3),14));

        sext_ln1118_fu_239_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(shl_ln728_4_fu_231_p3),14));

        sext_ln703_1_fu_330_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_1_fu_324_p2),14));

        sext_ln703_2_fu_362_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_2_reg_429),16));

        sext_ln703_3_fu_365_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_3_reg_434),15));

        sext_ln703_4_fu_352_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_4_fu_346_p2),14));

        sext_ln703_5_fu_368_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_5_reg_439),15));

        sext_ln703_6_fu_377_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_6_fu_371_p2),16));

        sext_ln703_fu_320_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_fu_314_p2),14));

        sext_ln728_fu_200_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(shl_ln728_1_fu_193_p3),13));

    shl_ln1118_1_fu_278_p3 <= (data_26_V_read11_reg_387 & ap_const_lv2_0);
    shl_ln1118_s_fu_246_p3 <= (data_21_V_read_3_reg_393 & ap_const_lv2_0);
    shl_ln728_1_fu_193_p3 <= (sub_ln1118_reg_414 & ap_const_lv7_0);
    shl_ln728_2_fu_204_p3 <= (data_13_V_read_3_reg_404 & ap_const_lv8_0);
    shl_ln728_3_fu_215_p3 <= (sub_ln1118_3_reg_419 & ap_const_lv7_0);
    shl_ln728_4_fu_231_p3 <= (sub_ln1118_2_fu_225_p2 & ap_const_lv7_0);
    shl_ln728_7_fu_307_p3 <= (sub_ln1118_6_reg_424 & ap_const_lv7_0);
    shl_ln_fu_186_p3 <= (sub_ln1118_1_reg_409 & ap_const_lv7_0);
    sub_ln1118_1_fu_126_p2 <= std_logic_vector(unsigned(zext_ln1118_fu_110_p1) - unsigned(zext_ln1118_1_fu_122_p1));
    sub_ln1118_2_fu_225_p2 <= std_logic_vector(unsigned(ap_const_lv4_0) - unsigned(zext_ln1118_5_fu_222_p1));
    sub_ln1118_3_fu_158_p2 <= std_logic_vector(unsigned(zext_ln1118_3_fu_142_p1) - unsigned(zext_ln1118_4_fu_154_p1));
    sub_ln1118_4_fu_257_p2 <= std_logic_vector(unsigned(zext_ln1118_7_fu_253_p1) - unsigned(zext_ln1118_6_fu_243_p1));
    sub_ln1118_5_fu_289_p2 <= std_logic_vector(unsigned(zext_ln1118_9_fu_285_p1) - unsigned(zext_ln1118_8_fu_275_p1));
    sub_ln1118_6_fu_180_p2 <= std_logic_vector(unsigned(zext_ln1118_10_fu_164_p1) - unsigned(zext_ln1118_11_fu_176_p1));
    sub_ln1118_fu_136_p2 <= std_logic_vector(unsigned(ap_const_lv4_0) - unsigned(zext_ln1118_2_fu_132_p1));
    tmp_1_fu_114_p3 <= (data_11_V_read_int_reg & ap_const_lv2_0);
    tmp_2_fu_146_p3 <= (data_17_V_read_int_reg & ap_const_lv2_0);
    tmp_3_fu_263_p3 <= (sub_ln1118_4_fu_257_p2 & ap_const_lv7_0);
    tmp_4_fu_295_p3 <= (sub_ln1118_5_fu_289_p2 & ap_const_lv7_0);
    tmp_5_fu_168_p3 <= (data_30_V_read_int_reg & ap_const_lv2_0);
    zext_ln1118_10_fu_164_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_30_V_read_int_reg),6));
    zext_ln1118_11_fu_176_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(tmp_5_fu_168_p3),6));
    zext_ln1118_1_fu_122_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(tmp_1_fu_114_p3),6));
    zext_ln1118_2_fu_132_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_12_V_read_int_reg),4));
    zext_ln1118_3_fu_142_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_17_V_read_int_reg),6));
    zext_ln1118_4_fu_154_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(tmp_2_fu_146_p3),6));
    zext_ln1118_5_fu_222_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_20_V_read_3_reg_399),4));
    zext_ln1118_6_fu_243_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_21_V_read_3_reg_393),6));
    zext_ln1118_7_fu_253_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_s_fu_246_p3),6));
    zext_ln1118_8_fu_275_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_26_V_read11_reg_387),6));
    zext_ln1118_9_fu_285_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_1_fu_278_p3),6));
    zext_ln1118_fu_110_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_11_V_read_int_reg),6));
    zext_ln728_fu_211_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln728_2_fu_204_p3),13));
end behav;
