// ==============================================================
// Vivado(TM) HLS - High-Level Synthesis from C, C++ and SystemC v2020.1 (64-bit)
// Copyright 1986-2020 Xilinx, Inc. All Rights Reserved.
// ==============================================================
#ifndef __sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_H__
#define __sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_H__


#include <systemc>
using namespace sc_core;
using namespace sc_dt;




#include <iostream>
#include <fstream>

struct sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_ram : public sc_core::sc_module {

  static const unsigned DataWidth = 8;
  static const unsigned AddressRange = 512;
  static const unsigned AddressWidth = 9;

//latency = 1
//input_reg = 1
//output_reg = 0
sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in <sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


sc_lv<DataWidth> ram[AddressRange];


   SC_CTOR(sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_ram) {
        for (unsigned i = 0; i < 79 ; i = i + 1) {
            ram[i] = "0b00000000";
        }
        for (unsigned i = 79; i < 101 ; i = i + 1) {
            ram[i] = "0b00000001";
        }
        for (unsigned i = 101; i < 115 ; i = i + 1) {
            ram[i] = "0b00000010";
        }
        for (unsigned i = 115; i < 124 ; i = i + 1) {
            ram[i] = "0b00000011";
        }
        for (unsigned i = 124; i < 131 ; i = i + 1) {
            ram[i] = "0b00000100";
        }
        for (unsigned i = 131; i < 137 ; i = i + 1) {
            ram[i] = "0b00000101";
        }
        for (unsigned i = 137; i < 142 ; i = i + 1) {
            ram[i] = "0b00000110";
        }
        for (unsigned i = 142; i < 147 ; i = i + 1) {
            ram[i] = "0b00000111";
        }
        ram[147] = "0b00001000";
        ram[148] = "0b00001000";
        ram[149] = "0b00001000";
        ram[150] = "0b00001000";
        ram[151] = "0b00001001";
        ram[152] = "0b00001001";
        ram[153] = "0b00001001";
        ram[154] = "0b00001010";
        ram[155] = "0b00001010";
        ram[156] = "0b00001010";
        ram[157] = "0b00001011";
        ram[158] = "0b00001011";
        ram[159] = "0b00001011";
        ram[160] = "0b00001100";
        ram[161] = "0b00001100";
        ram[162] = "0b00001100";
        ram[163] = "0b00001101";
        ram[164] = "0b00001101";
        ram[165] = "0b00001110";
        ram[166] = "0b00001110";
        ram[167] = "0b00001110";
        ram[168] = "0b00001111";
        ram[169] = "0b00001111";
        ram[170] = "0b00010000";
        ram[171] = "0b00010000";
        ram[172] = "0b00010001";
        ram[173] = "0b00010001";
        ram[174] = "0b00010010";
        ram[175] = "0b00010010";
        ram[176] = "0b00010011";
        ram[177] = "0b00010011";
        ram[178] = "0b00010100";
        ram[179] = "0b00010101";
        ram[180] = "0b00010101";
        ram[181] = "0b00010110";
        ram[182] = "0b00010111";
        ram[183] = "0b00010111";
        ram[184] = "0b00011000";
        ram[185] = "0b00011001";
        ram[186] = "0b00011001";
        ram[187] = "0b00011010";
        ram[188] = "0b00011011";
        ram[189] = "0b00011100";
        ram[190] = "0b00011100";
        ram[191] = "0b00011101";
        ram[192] = "0b00011110";
        ram[193] = "0b00011111";
        ram[194] = "0b00100000";
        ram[195] = "0b00100001";
        ram[196] = "0b00100010";
        ram[197] = "0b00100010";
        ram[198] = "0b00100011";
        ram[199] = "0b00100100";
        ram[200] = "0b00100101";
        ram[201] = "0b00100110";
        ram[202] = "0b00100111";
        ram[203] = "0b00101001";
        ram[204] = "0b00101010";
        ram[205] = "0b00101011";
        ram[206] = "0b00101100";
        ram[207] = "0b00101101";
        ram[208] = "0b00101110";
        ram[209] = "0b00101111";
        ram[210] = "0b00110001";
        ram[211] = "0b00110010";
        ram[212] = "0b00110011";
        ram[213] = "0b00110100";
        ram[214] = "0b00110110";
        ram[215] = "0b00110111";
        ram[216] = "0b00111001";
        ram[217] = "0b00111010";
        ram[218] = "0b00111011";
        ram[219] = "0b00111101";
        ram[220] = "0b00111110";
        ram[221] = "0b01000000";
        ram[222] = "0b01000001";
        ram[223] = "0b01000011";
        ram[224] = "0b01000100";
        ram[225] = "0b01000110";
        ram[226] = "0b01001000";
        ram[227] = "0b01001001";
        ram[228] = "0b01001011";
        ram[229] = "0b01001100";
        ram[230] = "0b01001110";
        ram[231] = "0b01010000";
        ram[232] = "0b01010010";
        ram[233] = "0b01010011";
        ram[234] = "0b01010101";
        ram[235] = "0b01010111";
        ram[236] = "0b01011001";
        ram[237] = "0b01011011";
        ram[238] = "0b01011100";
        ram[239] = "0b01011110";
        ram[240] = "0b01100000";
        ram[241] = "0b01100010";
        ram[242] = "0b01100100";
        ram[243] = "0b01100110";
        ram[244] = "0b01101000";
        ram[245] = "0b01101010";
        ram[246] = "0b01101100";
        ram[247] = "0b01101110";
        ram[248] = "0b01110000";
        ram[249] = "0b01110010";
        ram[250] = "0b01110100";
        ram[251] = "0b01110110";
        ram[252] = "0b01111000";
        ram[253] = "0b01111010";
        ram[254] = "0b01111100";
        ram[255] = "0b01111110";
        ram[256] = "0b10000000";
        ram[257] = "0b10000001";
        ram[258] = "0b10000011";
        ram[259] = "0b10000101";
        ram[260] = "0b10000111";
        ram[261] = "0b10001001";
        ram[262] = "0b10001011";
        ram[263] = "0b10001101";
        ram[264] = "0b10001111";
        ram[265] = "0b10010001";
        ram[266] = "0b10010011";
        ram[267] = "0b10010101";
        ram[268] = "0b10010111";
        ram[269] = "0b10011001";
        ram[270] = "0b10011011";
        ram[271] = "0b10011101";
        ram[272] = "0b10011111";
        ram[273] = "0b10100001";
        ram[274] = "0b10100011";
        ram[275] = "0b10100100";
        ram[276] = "0b10100110";
        ram[277] = "0b10101000";
        ram[278] = "0b10101010";
        ram[279] = "0b10101100";
        ram[280] = "0b10101101";
        ram[281] = "0b10101111";
        ram[282] = "0b10110001";
        ram[283] = "0b10110011";
        ram[284] = "0b10110100";
        ram[285] = "0b10110110";
        ram[286] = "0b10110111";
        ram[287] = "0b10111001";
        ram[288] = "0b10111011";
        ram[289] = "0b10111100";
        ram[290] = "0b10111110";
        ram[291] = "0b10111111";
        ram[292] = "0b11000001";
        ram[293] = "0b11000010";
        ram[294] = "0b11000100";
        ram[295] = "0b11000101";
        ram[296] = "0b11000110";
        ram[297] = "0b11001000";
        ram[298] = "0b11001001";
        ram[299] = "0b11001011";
        ram[300] = "0b11001100";
        ram[301] = "0b11001101";
        ram[302] = "0b11001110";
        ram[303] = "0b11010000";
        ram[304] = "0b11010001";
        ram[305] = "0b11010010";
        ram[306] = "0b11010011";
        ram[307] = "0b11010100";
        ram[308] = "0b11010101";
        ram[309] = "0b11010110";
        ram[310] = "0b11011000";
        ram[311] = "0b11011001";
        ram[312] = "0b11011010";
        ram[313] = "0b11011011";
        ram[314] = "0b11011100";
        ram[315] = "0b11011101";
        ram[316] = "0b11011101";
        ram[317] = "0b11011110";
        ram[318] = "0b11011111";
        ram[319] = "0b11100000";
        ram[320] = "0b11100001";
        ram[321] = "0b11100010";
        ram[322] = "0b11100011";
        ram[323] = "0b11100011";
        ram[324] = "0b11100100";
        ram[325] = "0b11100101";
        ram[326] = "0b11100110";
        ram[327] = "0b11100110";
        ram[328] = "0b11100111";
        ram[329] = "0b11101000";
        ram[330] = "0b11101000";
        ram[331] = "0b11101001";
        ram[332] = "0b11101010";
        ram[333] = "0b11101010";
        ram[334] = "0b11101011";
        ram[335] = "0b11101100";
        ram[336] = "0b11101100";
        ram[337] = "0b11101101";
        ram[338] = "0b11101101";
        ram[339] = "0b11101110";
        ram[340] = "0b11101110";
        ram[341] = "0b11101111";
        ram[342] = "0b11101111";
        ram[343] = "0b11110000";
        ram[344] = "0b11110000";
        ram[345] = "0b11110001";
        ram[346] = "0b11110001";
        ram[347] = "0b11110001";
        ram[348] = "0b11110010";
        ram[349] = "0b11110010";
        ram[350] = "0b11110011";
        ram[351] = "0b11110011";
        ram[352] = "0b11110011";
        ram[353] = "0b11110100";
        ram[354] = "0b11110100";
        ram[355] = "0b11110100";
        ram[356] = "0b11110101";
        ram[357] = "0b11110101";
        ram[358] = "0b11110101";
        ram[359] = "0b11110110";
        ram[360] = "0b11110110";
        ram[361] = "0b11110110";
        ram[362] = "0b11110111";
        ram[363] = "0b11110111";
        ram[364] = "0b11110111";
        ram[365] = "0b11110111";
        for (unsigned i = 366; i < 371 ; i = i + 1) {
            ram[i] = "0b11111000";
        }
        for (unsigned i = 371; i < 376 ; i = i + 1) {
            ram[i] = "0b11111001";
        }
        for (unsigned i = 376; i < 382 ; i = i + 1) {
            ram[i] = "0b11111010";
        }
        for (unsigned i = 382; i < 389 ; i = i + 1) {
            ram[i] = "0b11111011";
        }
        for (unsigned i = 389; i < 398 ; i = i + 1) {
            ram[i] = "0b11111100";
        }
        for (unsigned i = 398; i < 412 ; i = i + 1) {
            ram[i] = "0b11111101";
        }
        for (unsigned i = 412; i < 434 ; i = i + 1) {
            ram[i] = "0b11111110";
        }
        for (unsigned i = 434; i < 512 ; i = i + 1) {
            ram[i] = "0b11111111";
        }


SC_METHOD(prc_write_0);
  sensitive<<clk.pos();
   }


void prc_write_0()
{
    if (ce0.read() == sc_dt::Log_1) 
    {
            if(address0.read().is_01() && address0.read().to_uint()<AddressRange)
              q0 = ram[address0.read().to_uint()];
            else
              q0 = sc_lv<DataWidth>();
    }
}


}; //endmodule


SC_MODULE(sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb) {


static const unsigned DataWidth = 8;
static const unsigned AddressRange = 512;
static const unsigned AddressWidth = 9;

sc_core::sc_in <sc_lv<AddressWidth> > address0;
sc_core::sc_in<sc_logic> ce0;
sc_core::sc_out <sc_lv<DataWidth> > q0;
sc_core::sc_in<sc_logic> reset;
sc_core::sc_in<bool> clk;


sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_ram* meminst;


SC_CTOR(sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb) {
meminst = new sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_ram("sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb_ram");
meminst->address0(address0);
meminst->ce0(ce0);
meminst->q0(q0);

meminst->reset(reset);
meminst->clk(clk);
}
~sigmoid_ap_fixed_ap_ufixed_sigmoid_config13_s_sigmoid_tabbkb() {
    delete meminst;
}


};//endmodule
#endif
