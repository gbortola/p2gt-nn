#ifndef DEFINES_H_
#define DEFINES_H_

#include "ap_int.h"
#include "ap_fixed.h"
#include "nnet_utils/nnet_types.h"
#include <cstddef>
#include <cstdio>

//hls-fpga-machine-learning insert numbers
#define N_INPUT_1_1 17
#define N_LAYER_2 64
#define N_LAYER_5 32
#define N_LAYER_8 32
#define N_LAYER_11 1

//hls-fpga-machine-learning insert layer-precision
typedef ap_fixed<16,6> model_default_t;
typedef ap_fixed<16,6> input_t;
typedef ap_fixed<16,6> layer2_t;
typedef ap_fixed<6,3> weight2_t;
typedef ap_fixed<6,3> bias2_t;
typedef ap_fixed<16,6,AP_RND,AP_SAT> layer3_t;
typedef ap_ufixed<3,1,AP_RND,AP_SAT> layer4_t;
typedef ap_fixed<16,6> layer5_t;
typedef ap_fixed<7,2> weight5_t;
typedef ap_fixed<7,2> bias5_t;
typedef ap_fixed<16,6,AP_RND,AP_SAT> layer6_t;
typedef ap_ufixed<3,1,AP_RND,AP_SAT> layer7_t;
typedef ap_fixed<16,6> layer8_t;
typedef ap_fixed<5,2> weight8_t;
typedef ap_fixed<5,2> bias8_t;
typedef ap_fixed<16,6,AP_RND,AP_SAT> layer9_t;
typedef ap_ufixed<3,1,AP_RND,AP_SAT> layer10_t;
typedef ap_fixed<16,6> layer11_t;
typedef ap_fixed<6,5> weight11_t;
typedef ap_fixed<5,2> bias11_t;
typedef ap_fixed<16,6,AP_RND,AP_SAT> layer12_t;
typedef ap_ufixed<8,0> sigmoid_default_t;
typedef ap_ufixed<8,0,AP_RND,AP_SAT> result_t;

#endif
