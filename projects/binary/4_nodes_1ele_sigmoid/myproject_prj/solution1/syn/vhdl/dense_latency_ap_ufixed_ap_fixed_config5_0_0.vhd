-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
-- Version: 2020.1
-- Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dense_latency_ap_ufixed_ap_fixed_config5_0_0 is
port (
    ap_ready : OUT STD_LOGIC;
    data_0_V_read : IN STD_LOGIC_VECTOR (3 downto 0);
    data_2_V_read : IN STD_LOGIC_VECTOR (3 downto 0);
    data_3_V_read : IN STD_LOGIC_VECTOR (3 downto 0);
    ap_return : OUT STD_LOGIC_VECTOR (12 downto 0) );
end;


architecture behav of dense_latency_ap_ufixed_ap_fixed_config5_0_0 is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_boolean_1 : BOOLEAN := true;
    constant ap_const_lv12_64 : STD_LOGIC_VECTOR (11 downto 0) := "000001100100";
    constant ap_const_lv12_FB7 : STD_LOGIC_VECTOR (11 downto 0) := "111110110111";
    constant ap_const_lv6_0 : STD_LOGIC_VECTOR (5 downto 0) := "000000";
    constant ap_const_lv11_0 : STD_LOGIC_VECTOR (10 downto 0) := "00000000000";
    constant ap_const_lv12_F98 : STD_LOGIC_VECTOR (11 downto 0) := "111110011000";
    constant ap_const_logic_0 : STD_LOGIC := '0';

    signal mul_ln1118_1_fu_64_p0 : STD_LOGIC_VECTOR (3 downto 0);
    signal mul_ln1118_fu_66_p0 : STD_LOGIC_VECTOR (3 downto 0);
    signal shl_ln_fu_110_p3 : STD_LOGIC_VECTOR (9 downto 0);
    signal zext_ln1118_1_fu_118_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal sub_ln1118_fu_122_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal zext_ln1118_fu_106_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal sub_ln1118_1_fu_128_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal mul_ln1118_fu_66_p2 : STD_LOGIC_VECTOR (11 downto 0);
    signal sext_ln1116_1_fu_143_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal sext_ln1116_fu_134_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal mul_ln1118_1_fu_64_p2 : STD_LOGIC_VECTOR (11 downto 0);
    signal add_ln703_1_fu_158_p2 : STD_LOGIC_VECTOR (11 downto 0);
    signal add_ln703_fu_152_p2 : STD_LOGIC_VECTOR (12 downto 0);
    signal sext_ln703_fu_164_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal mul_ln1118_1_fu_64_p00 : STD_LOGIC_VECTOR (11 downto 0);
    signal mul_ln1118_fu_66_p00 : STD_LOGIC_VECTOR (11 downto 0);


begin



    add_ln703_1_fu_158_p2 <= std_logic_vector(unsigned(mul_ln1118_1_fu_64_p2) + unsigned(ap_const_lv12_F98));
    add_ln703_fu_152_p2 <= std_logic_vector(signed(sext_ln1116_1_fu_143_p1) + signed(sext_ln1116_fu_134_p1));
    ap_ready <= ap_const_logic_1;
    ap_return <= std_logic_vector(unsigned(add_ln703_fu_152_p2) + unsigned(sext_ln703_fu_164_p1));
    mul_ln1118_1_fu_64_p0 <= mul_ln1118_1_fu_64_p00(4 - 1 downto 0);
    mul_ln1118_1_fu_64_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_3_V_read),12));
    mul_ln1118_1_fu_64_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln1118_1_fu_64_p0) * unsigned(ap_const_lv12_64), 12));
    mul_ln1118_fu_66_p0 <= mul_ln1118_fu_66_p00(4 - 1 downto 0);
    mul_ln1118_fu_66_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_2_V_read),12));
    mul_ln1118_fu_66_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(std_logic_vector(signed('0' &mul_ln1118_fu_66_p0) * signed(ap_const_lv12_FB7))), 12));
        sext_ln1116_1_fu_143_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(mul_ln1118_fu_66_p2),13));

        sext_ln1116_fu_134_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(sub_ln1118_1_fu_128_p2),13));

        sext_ln703_fu_164_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_1_fu_158_p2),13));

    shl_ln_fu_110_p3 <= (data_0_V_read & ap_const_lv6_0);
    sub_ln1118_1_fu_128_p2 <= std_logic_vector(unsigned(sub_ln1118_fu_122_p2) - unsigned(zext_ln1118_fu_106_p1));
    sub_ln1118_fu_122_p2 <= std_logic_vector(unsigned(ap_const_lv11_0) - unsigned(zext_ln1118_1_fu_118_p1));
    zext_ln1118_1_fu_118_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln_fu_110_p3),11));
    zext_ln1118_fu_106_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_0_V_read),11));
end behav;
