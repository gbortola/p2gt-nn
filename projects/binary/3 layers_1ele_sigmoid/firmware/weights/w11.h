//Numpy array shape [32, 1]
//Min -1.500000000000
//Max 1.000000000000
//Number of zeros 24

#ifndef W11_H_
#define W11_H_

#ifndef __SYNTHESIS__
weight11_t w11[32];
#else
weight11_t w11[32] = {0.0, 0.0, 0.0, -1.0, 0.0, 0.0, 0.0, -1.0, 0.0, -1.5, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.5, 0.0, 0.0, 1.0, -1.5, 0.0, 0.0, 0.0, 1.0};
#endif

#endif
