// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2020.1
// Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

#ifndef _relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config7_s_HH_
#define _relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config7_s_HH_

#include "systemc.h"
#include "AESL_pkg.h"


namespace ap_rtl {

struct relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config7_s : public sc_module {
    // Port declarations 11
    sc_out< sc_logic > ap_ready;
    sc_in< sc_lv<16> > data_9_V_read;
    sc_in< sc_lv<16> > data_11_V_read;
    sc_in< sc_lv<16> > data_14_V_read;
    sc_in< sc_lv<16> > data_24_V_read;
    sc_in< sc_lv<16> > data_26_V_read;
    sc_out< sc_lv<3> > ap_return_0;
    sc_out< sc_lv<3> > ap_return_1;
    sc_out< sc_lv<3> > ap_return_2;
    sc_out< sc_lv<3> > ap_return_3;
    sc_out< sc_lv<3> > ap_return_4;


    // Module declarations
    relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config7_s(sc_module_name name);
    SC_HAS_PROCESS(relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config7_s);

    ~relu_ap_fixed_ap_ufixed_3_1_0_0_0_relu_config7_s();

    sc_trace_file* mVcdFile;

    sc_signal< sc_lv<1> > tmp_5_fu_116_p3;
    sc_signal< sc_lv<3> > zext_ln415_fu_124_p1;
    sc_signal< sc_lv<3> > trunc_ln_fu_98_p4;
    sc_signal< sc_lv<3> > add_ln415_fu_128_p2;
    sc_signal< sc_lv<1> > tmp_6_fu_134_p3;
    sc_signal< sc_lv<1> > tmp_4_fu_108_p3;
    sc_signal< sc_lv<1> > xor_ln416_fu_142_p2;
    sc_signal< sc_lv<7> > p_Result_6_9_fu_154_p4;
    sc_signal< sc_lv<1> > and_ln416_fu_148_p2;
    sc_signal< sc_lv<1> > icmp_ln879_fu_164_p2;
    sc_signal< sc_lv<1> > icmp_ln768_fu_170_p2;
    sc_signal< sc_lv<1> > select_ln777_fu_176_p3;
    sc_signal< sc_lv<1> > icmp_ln1494_fu_92_p2;
    sc_signal< sc_lv<3> > select_ln340_fu_184_p3;
    sc_signal< sc_lv<1> > tmp_8_fu_224_p3;
    sc_signal< sc_lv<3> > zext_ln415_1_fu_232_p1;
    sc_signal< sc_lv<3> > trunc_ln708_1_fu_206_p4;
    sc_signal< sc_lv<3> > add_ln415_1_fu_236_p2;
    sc_signal< sc_lv<1> > tmp_9_fu_242_p3;
    sc_signal< sc_lv<1> > tmp_7_fu_216_p3;
    sc_signal< sc_lv<1> > xor_ln416_1_fu_250_p2;
    sc_signal< sc_lv<7> > p_Result_6_s_fu_262_p4;
    sc_signal< sc_lv<1> > and_ln416_1_fu_256_p2;
    sc_signal< sc_lv<1> > icmp_ln879_1_fu_272_p2;
    sc_signal< sc_lv<1> > icmp_ln768_1_fu_278_p2;
    sc_signal< sc_lv<1> > select_ln777_1_fu_284_p3;
    sc_signal< sc_lv<1> > icmp_ln1494_1_fu_200_p2;
    sc_signal< sc_lv<3> > select_ln340_1_fu_292_p3;
    sc_signal< sc_lv<1> > tmp_11_fu_332_p3;
    sc_signal< sc_lv<3> > zext_ln415_2_fu_340_p1;
    sc_signal< sc_lv<3> > trunc_ln708_2_fu_314_p4;
    sc_signal< sc_lv<3> > add_ln415_2_fu_344_p2;
    sc_signal< sc_lv<1> > tmp_12_fu_350_p3;
    sc_signal< sc_lv<1> > tmp_10_fu_324_p3;
    sc_signal< sc_lv<1> > xor_ln416_2_fu_358_p2;
    sc_signal< sc_lv<7> > p_Result_6_1_fu_370_p4;
    sc_signal< sc_lv<1> > and_ln416_2_fu_364_p2;
    sc_signal< sc_lv<1> > icmp_ln879_2_fu_380_p2;
    sc_signal< sc_lv<1> > icmp_ln768_2_fu_386_p2;
    sc_signal< sc_lv<1> > select_ln777_2_fu_392_p3;
    sc_signal< sc_lv<1> > icmp_ln1494_2_fu_308_p2;
    sc_signal< sc_lv<3> > select_ln340_2_fu_400_p3;
    sc_signal< sc_lv<1> > tmp_14_fu_440_p3;
    sc_signal< sc_lv<3> > zext_ln415_3_fu_448_p1;
    sc_signal< sc_lv<3> > trunc_ln708_3_fu_422_p4;
    sc_signal< sc_lv<3> > add_ln415_3_fu_452_p2;
    sc_signal< sc_lv<1> > tmp_15_fu_458_p3;
    sc_signal< sc_lv<1> > tmp_13_fu_432_p3;
    sc_signal< sc_lv<1> > xor_ln416_3_fu_466_p2;
    sc_signal< sc_lv<7> > p_Result_6_2_fu_478_p4;
    sc_signal< sc_lv<1> > and_ln416_3_fu_472_p2;
    sc_signal< sc_lv<1> > icmp_ln879_3_fu_488_p2;
    sc_signal< sc_lv<1> > icmp_ln768_3_fu_494_p2;
    sc_signal< sc_lv<1> > select_ln777_3_fu_500_p3;
    sc_signal< sc_lv<1> > icmp_ln1494_3_fu_416_p2;
    sc_signal< sc_lv<3> > select_ln340_3_fu_508_p3;
    sc_signal< sc_lv<1> > tmp_17_fu_548_p3;
    sc_signal< sc_lv<3> > zext_ln415_4_fu_556_p1;
    sc_signal< sc_lv<3> > trunc_ln708_4_fu_530_p4;
    sc_signal< sc_lv<3> > add_ln415_4_fu_560_p2;
    sc_signal< sc_lv<1> > tmp_18_fu_566_p3;
    sc_signal< sc_lv<1> > tmp_16_fu_540_p3;
    sc_signal< sc_lv<1> > xor_ln416_4_fu_574_p2;
    sc_signal< sc_lv<7> > p_Result_6_3_fu_586_p4;
    sc_signal< sc_lv<1> > and_ln416_4_fu_580_p2;
    sc_signal< sc_lv<1> > icmp_ln879_4_fu_596_p2;
    sc_signal< sc_lv<1> > icmp_ln768_4_fu_602_p2;
    sc_signal< sc_lv<1> > select_ln777_4_fu_608_p3;
    sc_signal< sc_lv<1> > icmp_ln1494_4_fu_524_p2;
    sc_signal< sc_lv<3> > select_ln340_4_fu_616_p3;
    sc_signal< sc_lv<3> > select_ln1494_fu_192_p3;
    sc_signal< sc_lv<3> > select_ln1494_1_fu_300_p3;
    sc_signal< sc_lv<3> > select_ln1494_2_fu_408_p3;
    sc_signal< sc_lv<3> > select_ln1494_3_fu_516_p3;
    sc_signal< sc_lv<3> > select_ln1494_4_fu_624_p3;
    static const sc_logic ap_const_logic_1;
    static const bool ap_const_boolean_1;
    static const sc_lv<16> ap_const_lv16_0;
    static const sc_lv<32> ap_const_lv32_6;
    static const sc_lv<32> ap_const_lv32_8;
    static const sc_lv<32> ap_const_lv32_5;
    static const sc_lv<32> ap_const_lv32_2;
    static const sc_lv<1> ap_const_lv1_1;
    static const sc_lv<32> ap_const_lv32_9;
    static const sc_lv<32> ap_const_lv32_F;
    static const sc_lv<7> ap_const_lv7_7F;
    static const sc_lv<7> ap_const_lv7_0;
    static const sc_lv<3> ap_const_lv3_7;
    static const sc_lv<3> ap_const_lv3_0;
    static const sc_lv<1> ap_const_lv1_0;
    static const sc_logic ap_const_logic_0;
    // Thread declarations
    void thread_add_ln415_1_fu_236_p2();
    void thread_add_ln415_2_fu_344_p2();
    void thread_add_ln415_3_fu_452_p2();
    void thread_add_ln415_4_fu_560_p2();
    void thread_add_ln415_fu_128_p2();
    void thread_and_ln416_1_fu_256_p2();
    void thread_and_ln416_2_fu_364_p2();
    void thread_and_ln416_3_fu_472_p2();
    void thread_and_ln416_4_fu_580_p2();
    void thread_and_ln416_fu_148_p2();
    void thread_ap_ready();
    void thread_ap_return_0();
    void thread_ap_return_1();
    void thread_ap_return_2();
    void thread_ap_return_3();
    void thread_ap_return_4();
    void thread_icmp_ln1494_1_fu_200_p2();
    void thread_icmp_ln1494_2_fu_308_p2();
    void thread_icmp_ln1494_3_fu_416_p2();
    void thread_icmp_ln1494_4_fu_524_p2();
    void thread_icmp_ln1494_fu_92_p2();
    void thread_icmp_ln768_1_fu_278_p2();
    void thread_icmp_ln768_2_fu_386_p2();
    void thread_icmp_ln768_3_fu_494_p2();
    void thread_icmp_ln768_4_fu_602_p2();
    void thread_icmp_ln768_fu_170_p2();
    void thread_icmp_ln879_1_fu_272_p2();
    void thread_icmp_ln879_2_fu_380_p2();
    void thread_icmp_ln879_3_fu_488_p2();
    void thread_icmp_ln879_4_fu_596_p2();
    void thread_icmp_ln879_fu_164_p2();
    void thread_p_Result_6_1_fu_370_p4();
    void thread_p_Result_6_2_fu_478_p4();
    void thread_p_Result_6_3_fu_586_p4();
    void thread_p_Result_6_9_fu_154_p4();
    void thread_p_Result_6_s_fu_262_p4();
    void thread_select_ln1494_1_fu_300_p3();
    void thread_select_ln1494_2_fu_408_p3();
    void thread_select_ln1494_3_fu_516_p3();
    void thread_select_ln1494_4_fu_624_p3();
    void thread_select_ln1494_fu_192_p3();
    void thread_select_ln340_1_fu_292_p3();
    void thread_select_ln340_2_fu_400_p3();
    void thread_select_ln340_3_fu_508_p3();
    void thread_select_ln340_4_fu_616_p3();
    void thread_select_ln340_fu_184_p3();
    void thread_select_ln777_1_fu_284_p3();
    void thread_select_ln777_2_fu_392_p3();
    void thread_select_ln777_3_fu_500_p3();
    void thread_select_ln777_4_fu_608_p3();
    void thread_select_ln777_fu_176_p3();
    void thread_tmp_10_fu_324_p3();
    void thread_tmp_11_fu_332_p3();
    void thread_tmp_12_fu_350_p3();
    void thread_tmp_13_fu_432_p3();
    void thread_tmp_14_fu_440_p3();
    void thread_tmp_15_fu_458_p3();
    void thread_tmp_16_fu_540_p3();
    void thread_tmp_17_fu_548_p3();
    void thread_tmp_18_fu_566_p3();
    void thread_tmp_4_fu_108_p3();
    void thread_tmp_5_fu_116_p3();
    void thread_tmp_6_fu_134_p3();
    void thread_tmp_7_fu_216_p3();
    void thread_tmp_8_fu_224_p3();
    void thread_tmp_9_fu_242_p3();
    void thread_trunc_ln708_1_fu_206_p4();
    void thread_trunc_ln708_2_fu_314_p4();
    void thread_trunc_ln708_3_fu_422_p4();
    void thread_trunc_ln708_4_fu_530_p4();
    void thread_trunc_ln_fu_98_p4();
    void thread_xor_ln416_1_fu_250_p2();
    void thread_xor_ln416_2_fu_358_p2();
    void thread_xor_ln416_3_fu_466_p2();
    void thread_xor_ln416_4_fu_574_p2();
    void thread_xor_ln416_fu_142_p2();
    void thread_zext_ln415_1_fu_232_p1();
    void thread_zext_ln415_2_fu_340_p1();
    void thread_zext_ln415_3_fu_448_p1();
    void thread_zext_ln415_4_fu_556_p1();
    void thread_zext_ln415_fu_124_p1();
};

}

using namespace ap_rtl;

#endif
