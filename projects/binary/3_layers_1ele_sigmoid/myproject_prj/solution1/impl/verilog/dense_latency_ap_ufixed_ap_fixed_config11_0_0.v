// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2020.1
// Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module dense_latency_ap_ufixed_ap_fixed_config11_0_0 (
        ap_ready,
        data_11_V_read,
        data_12_V_read,
        data_13_V_read,
        data_17_V_read,
        data_20_V_read,
        data_21_V_read,
        data_26_V_read,
        data_30_V_read,
        ap_return
);


output   ap_ready;
input  [2:0] data_11_V_read;
input  [2:0] data_12_V_read;
input  [2:0] data_13_V_read;
input  [2:0] data_17_V_read;
input  [2:0] data_20_V_read;
input  [2:0] data_21_V_read;
input  [2:0] data_26_V_read;
input  [2:0] data_30_V_read;
output  [15:0] ap_return;

wire   [4:0] tmp_1_fu_114_p3;
wire   [5:0] zext_ln1118_fu_110_p1;
wire   [5:0] zext_ln1118_1_fu_122_p1;
wire   [5:0] sub_ln1118_1_fu_126_p2;
wire   [3:0] zext_ln1118_2_fu_140_p1;
wire   [3:0] sub_ln1118_fu_144_p2;
wire   [8:0] shl_ln728_1_fu_150_p3;
wire   [8:0] shl_ln728_2_fu_162_p3;
wire   [4:0] tmp_2_fu_178_p3;
wire   [5:0] zext_ln1118_3_fu_174_p1;
wire   [5:0] zext_ln1118_4_fu_186_p1;
wire   [5:0] sub_ln1118_3_fu_190_p2;
wire   [3:0] zext_ln1118_5_fu_204_p1;
wire   [3:0] sub_ln1118_2_fu_208_p2;
wire   [8:0] shl_ln728_4_fu_214_p3;
wire   [4:0] shl_ln1118_s_fu_230_p3;
wire   [5:0] zext_ln1118_7_fu_238_p1;
wire   [5:0] zext_ln1118_6_fu_226_p1;
wire   [5:0] sub_ln1118_4_fu_242_p2;
wire   [10:0] tmp_3_fu_248_p3;
wire  signed [13:0] sext_ln728_2_fu_256_p1;
wire   [4:0] shl_ln1118_1_fu_268_p3;
wire   [5:0] zext_ln1118_9_fu_276_p1;
wire   [5:0] zext_ln1118_8_fu_264_p1;
wire   [5:0] sub_ln1118_5_fu_280_p2;
wire   [10:0] tmp_4_fu_286_p3;
wire  signed [13:0] sext_ln728_3_fu_294_p1;
wire   [4:0] tmp_5_fu_306_p3;
wire   [5:0] zext_ln1118_10_fu_302_p1;
wire   [5:0] zext_ln1118_11_fu_314_p1;
wire   [5:0] sub_ln1118_6_fu_318_p2;
wire   [10:0] shl_ln_fu_132_p3;
wire  signed [10:0] sext_ln728_fu_158_p1;
wire   [10:0] add_ln703_fu_332_p2;
wire   [10:0] zext_ln728_fu_170_p1;
wire   [10:0] shl_ln728_3_fu_196_p3;
wire   [10:0] add_ln703_1_fu_342_p2;
wire  signed [11:0] sext_ln703_1_fu_348_p1;
wire  signed [11:0] sext_ln703_fu_338_p1;
wire   [11:0] add_ln703_2_fu_352_p2;
wire  signed [14:0] sext_ln728_1_fu_222_p1;
wire   [14:0] zext_ln728_1_fu_260_p1;
wire   [14:0] add_ln703_3_fu_362_p2;
wire   [10:0] shl_ln728_7_fu_324_p3;
wire   [10:0] add_ln703_4_fu_372_p2;
wire  signed [14:0] sext_ln703_4_fu_378_p1;
wire   [14:0] zext_ln728_2_fu_298_p1;
wire   [14:0] add_ln703_5_fu_382_p2;
wire  signed [15:0] sext_ln703_5_fu_388_p1;
wire  signed [15:0] sext_ln703_3_fu_368_p1;
wire   [15:0] add_ln703_6_fu_392_p2;
wire  signed [15:0] sext_ln703_2_fu_358_p1;

assign add_ln703_1_fu_342_p2 = (zext_ln728_fu_170_p1 + shl_ln728_3_fu_196_p3);

assign add_ln703_2_fu_352_p2 = ($signed(sext_ln703_1_fu_348_p1) + $signed(sext_ln703_fu_338_p1));

assign add_ln703_3_fu_362_p2 = ($signed(sext_ln728_1_fu_222_p1) + $signed(zext_ln728_1_fu_260_p1));

assign add_ln703_4_fu_372_p2 = ($signed(shl_ln728_7_fu_324_p3) + $signed(11'd1984));

assign add_ln703_5_fu_382_p2 = ($signed(sext_ln703_4_fu_378_p1) + $signed(zext_ln728_2_fu_298_p1));

assign add_ln703_6_fu_392_p2 = ($signed(sext_ln703_5_fu_388_p1) + $signed(sext_ln703_3_fu_368_p1));

assign add_ln703_fu_332_p2 = ($signed(shl_ln_fu_132_p3) + $signed(sext_ln728_fu_158_p1));

assign ap_ready = 1'b1;

assign ap_return = ($signed(add_ln703_6_fu_392_p2) + $signed(sext_ln703_2_fu_358_p1));

assign sext_ln703_1_fu_348_p1 = $signed(add_ln703_1_fu_342_p2);

assign sext_ln703_2_fu_358_p1 = $signed(add_ln703_2_fu_352_p2);

assign sext_ln703_3_fu_368_p1 = $signed(add_ln703_3_fu_362_p2);

assign sext_ln703_4_fu_378_p1 = $signed(add_ln703_4_fu_372_p2);

assign sext_ln703_5_fu_388_p1 = $signed(add_ln703_5_fu_382_p2);

assign sext_ln703_fu_338_p1 = $signed(add_ln703_fu_332_p2);

assign sext_ln728_1_fu_222_p1 = $signed(shl_ln728_4_fu_214_p3);

assign sext_ln728_2_fu_256_p1 = $signed(tmp_3_fu_248_p3);

assign sext_ln728_3_fu_294_p1 = $signed(tmp_4_fu_286_p3);

assign sext_ln728_fu_158_p1 = $signed(shl_ln728_1_fu_150_p3);

assign shl_ln1118_1_fu_268_p3 = {{data_26_V_read}, {2'd0}};

assign shl_ln1118_s_fu_230_p3 = {{data_21_V_read}, {2'd0}};

assign shl_ln728_1_fu_150_p3 = {{sub_ln1118_fu_144_p2}, {5'd0}};

assign shl_ln728_2_fu_162_p3 = {{data_13_V_read}, {6'd0}};

assign shl_ln728_3_fu_196_p3 = {{sub_ln1118_3_fu_190_p2}, {5'd0}};

assign shl_ln728_4_fu_214_p3 = {{sub_ln1118_2_fu_208_p2}, {5'd0}};

assign shl_ln728_7_fu_324_p3 = {{sub_ln1118_6_fu_318_p2}, {5'd0}};

assign shl_ln_fu_132_p3 = {{sub_ln1118_1_fu_126_p2}, {5'd0}};

assign sub_ln1118_1_fu_126_p2 = (zext_ln1118_fu_110_p1 - zext_ln1118_1_fu_122_p1);

assign sub_ln1118_2_fu_208_p2 = (4'd0 - zext_ln1118_5_fu_204_p1);

assign sub_ln1118_3_fu_190_p2 = (zext_ln1118_3_fu_174_p1 - zext_ln1118_4_fu_186_p1);

assign sub_ln1118_4_fu_242_p2 = (zext_ln1118_7_fu_238_p1 - zext_ln1118_6_fu_226_p1);

assign sub_ln1118_5_fu_280_p2 = (zext_ln1118_9_fu_276_p1 - zext_ln1118_8_fu_264_p1);

assign sub_ln1118_6_fu_318_p2 = (zext_ln1118_10_fu_302_p1 - zext_ln1118_11_fu_314_p1);

assign sub_ln1118_fu_144_p2 = (4'd0 - zext_ln1118_2_fu_140_p1);

assign tmp_1_fu_114_p3 = {{data_11_V_read}, {2'd0}};

assign tmp_2_fu_178_p3 = {{data_17_V_read}, {2'd0}};

assign tmp_3_fu_248_p3 = {{sub_ln1118_4_fu_242_p2}, {5'd0}};

assign tmp_4_fu_286_p3 = {{sub_ln1118_5_fu_280_p2}, {5'd0}};

assign tmp_5_fu_306_p3 = {{data_30_V_read}, {2'd0}};

assign zext_ln1118_10_fu_302_p1 = data_30_V_read;

assign zext_ln1118_11_fu_314_p1 = tmp_5_fu_306_p3;

assign zext_ln1118_1_fu_122_p1 = tmp_1_fu_114_p3;

assign zext_ln1118_2_fu_140_p1 = data_12_V_read;

assign zext_ln1118_3_fu_174_p1 = data_17_V_read;

assign zext_ln1118_4_fu_186_p1 = tmp_2_fu_178_p3;

assign zext_ln1118_5_fu_204_p1 = data_20_V_read;

assign zext_ln1118_6_fu_226_p1 = data_21_V_read;

assign zext_ln1118_7_fu_238_p1 = shl_ln1118_s_fu_230_p3;

assign zext_ln1118_8_fu_264_p1 = data_26_V_read;

assign zext_ln1118_9_fu_276_p1 = shl_ln1118_1_fu_268_p3;

assign zext_ln1118_fu_110_p1 = data_11_V_read;

assign zext_ln728_1_fu_260_p1 = $unsigned(sext_ln728_2_fu_256_p1);

assign zext_ln728_2_fu_298_p1 = $unsigned(sext_ln728_3_fu_294_p1);

assign zext_ln728_fu_170_p1 = shl_ln728_2_fu_162_p3;

endmodule //dense_latency_ap_ufixed_ap_fixed_config11_0_0
