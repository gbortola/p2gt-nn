-- ==============================================================
-- RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
-- Version: 2020.1
-- Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
-- 
-- ===========================================================

library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;

entity dense_latency_ap_ufixed_ap_fixed_config5_0_0 is
port (
    ap_clk : IN STD_LOGIC;
    ap_rst : IN STD_LOGIC;
    data_0_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_1_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_3_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_7_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_15_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_16_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_20_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_32_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_40_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_43_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_45_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_47_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_48_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_53_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    data_58_V_read : IN STD_LOGIC_VECTOR (2 downto 0);
    ap_return : OUT STD_LOGIC_VECTOR (12 downto 0);
    ap_ce : IN STD_LOGIC );
end;


architecture behav of dense_latency_ap_ufixed_ap_fixed_config5_0_0 is 
    constant ap_const_logic_1 : STD_LOGIC := '1';
    constant ap_const_logic_0 : STD_LOGIC := '0';
    constant ap_const_boolean_1 : BOOLEAN := true;
    constant ap_const_boolean_0 : BOOLEAN := false;
    constant ap_const_lv10_3D9 : STD_LOGIC_VECTOR (9 downto 0) := "1111011001";
    constant ap_const_lv9_34 : STD_LOGIC_VECTOR (8 downto 0) := "000110100";
    constant ap_const_lv9_2E : STD_LOGIC_VECTOR (8 downto 0) := "000101110";
    constant ap_const_lv10_3CF : STD_LOGIC_VECTOR (9 downto 0) := "1111001111";
    constant ap_const_lv9_3A : STD_LOGIC_VECTOR (8 downto 0) := "000111010";
    constant ap_const_lv10_64 : STD_LOGIC_VECTOR (9 downto 0) := "0001100100";
    constant ap_const_lv9_1E3 : STD_LOGIC_VECTOR (8 downto 0) := "111100011";
    constant ap_const_lv9_32 : STD_LOGIC_VECTOR (8 downto 0) := "000110010";
    constant ap_const_lv1_0 : STD_LOGIC_VECTOR (0 downto 0) := "0";
    constant ap_const_lv4_0 : STD_LOGIC_VECTOR (3 downto 0) := "0000";
    constant ap_const_lv5_0 : STD_LOGIC_VECTOR (4 downto 0) := "00000";
    constant ap_const_lv2_0 : STD_LOGIC_VECTOR (1 downto 0) := "00";
    constant ap_const_lv3_0 : STD_LOGIC_VECTOR (2 downto 0) := "000";
    constant ap_const_lv7_0 : STD_LOGIC_VECTOR (6 downto 0) := "0000000";
    constant ap_const_lv10_3D8 : STD_LOGIC_VECTOR (9 downto 0) := "1111011000";
    constant ap_const_lv9_0 : STD_LOGIC_VECTOR (8 downto 0) := "000000000";

    signal data_32_V_read_2_reg_1170 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_block_state1_pp0_stage0_iter0 : BOOLEAN;
    signal ap_block_state2_pp0_stage0_iter1 : BOOLEAN;
    signal ap_block_pp0_stage0_11001 : BOOLEAN;
    signal data_20_V_read_3_reg_1175 : STD_LOGIC_VECTOR (2 downto 0);
    signal data_16_V_read_3_reg_1180 : STD_LOGIC_VECTOR (2 downto 0);
    signal data_15_V_read_3_reg_1186 : STD_LOGIC_VECTOR (2 downto 0);
    signal add_ln703_2_fu_946_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_2_reg_1192 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_13_fu_1012_p2 : STD_LOGIC_VECTOR (11 downto 0);
    signal add_ln703_13_reg_1197 : STD_LOGIC_VECTOR (11 downto 0);
    signal mul_ln1118_fu_200_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal ap_block_pp0_stage0 : BOOLEAN;
    signal mul_ln728_3_fu_201_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal mul_ln728_4_fu_202_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal mul_ln1118_1_fu_204_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal mul_ln728_1_fu_206_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal mul_ln728_fu_208_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal mul_ln1118_2_fu_211_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal mul_ln728_2_fu_212_p0 : STD_LOGIC_VECTOR (2 downto 0);
    signal mul_ln1118_fu_200_p2 : STD_LOGIC_VECTOR (9 downto 0);
    signal zext_ln1118_1_fu_715_p1 : STD_LOGIC_VECTOR (3 downto 0);
    signal sub_ln1118_fu_719_p2 : STD_LOGIC_VECTOR (3 downto 0);
    signal shl_ln728_1_fu_725_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal shl_ln1_fu_737_p3 : STD_LOGIC_VECTOR (7 downto 0);
    signal shl_ln1118_1_fu_749_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal zext_ln1118_3_fu_757_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal zext_ln1118_2_fu_745_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal sub_ln1118_1_fu_761_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal shl_ln728_2_fu_767_p3 : STD_LOGIC_VECTOR (9 downto 0);
    signal mul_ln728_fu_208_p2 : STD_LOGIC_VECTOR (9 downto 0);
    signal shl_ln1118_5_fu_788_p3 : STD_LOGIC_VECTOR (7 downto 0);
    signal shl_ln1118_6_fu_800_p3 : STD_LOGIC_VECTOR (4 downto 0);
    signal zext_ln1118_12_fu_808_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal zext_ln1118_11_fu_796_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal sub_ln1118_6_fu_812_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln728_2_fu_212_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln728_3_fu_201_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal shl_ln1118_7_fu_844_p3 : STD_LOGIC_VECTOR (5 downto 0);
    signal zext_ln1118_15_fu_852_p1 : STD_LOGIC_VECTOR (6 downto 0);
    signal sub_ln1118_7_fu_856_p2 : STD_LOGIC_VECTOR (6 downto 0);
    signal shl_ln1118_8_fu_866_p3 : STD_LOGIC_VECTOR (3 downto 0);
    signal sext_ln1118_fu_862_p1 : STD_LOGIC_VECTOR (7 downto 0);
    signal zext_ln1118_16_fu_874_p1 : STD_LOGIC_VECTOR (7 downto 0);
    signal sub_ln1118_8_fu_878_p2 : STD_LOGIC_VECTOR (7 downto 0);
    signal shl_ln728_7_fu_884_p3 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln1118_2_fu_211_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal shl_ln728_9_fu_909_p5 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln728_4_fu_202_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal sext_ln728_fu_733_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal shl_ln_fu_707_p3 : STD_LOGIC_VECTOR (10 downto 0);
    signal zext_ln728_fu_784_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal sext_ln728_1_fu_775_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_fu_934_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_1_fu_940_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal zext_ln728_2_fu_831_p1 : STD_LOGIC_VECTOR (9 downto 0);
    signal shl_ln728_6_fu_818_p3 : STD_LOGIC_VECTOR (9 downto 0);
    signal add_ln703_7_fu_952_p2 : STD_LOGIC_VECTOR (9 downto 0);
    signal sext_ln728_4_fu_892_p1 : STD_LOGIC_VECTOR (9 downto 0);
    signal zext_ln728_3_fu_840_p1 : STD_LOGIC_VECTOR (9 downto 0);
    signal add_ln703_8_fu_962_p2 : STD_LOGIC_VECTOR (9 downto 0);
    signal sext_ln703_4_fu_958_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal sext_ln703_5_fu_968_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_9_fu_972_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal zext_ln728_4_fu_921_p1 : STD_LOGIC_VECTOR (9 downto 0);
    signal shl_ln728_8_fu_901_p3 : STD_LOGIC_VECTOR (9 downto 0);
    signal add_ln703_10_fu_982_p2 : STD_LOGIC_VECTOR (9 downto 0);
    signal zext_ln728_5_fu_930_p1 : STD_LOGIC_VECTOR (9 downto 0);
    signal add_ln703_11_fu_992_p2 : STD_LOGIC_VECTOR (9 downto 0);
    signal sext_ln703_7_fu_988_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal sext_ln703_8_fu_998_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_12_fu_1002_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal sext_ln703_6_fu_978_p1 : STD_LOGIC_VECTOR (11 downto 0);
    signal sext_ln703_9_fu_1008_p1 : STD_LOGIC_VECTOR (11 downto 0);
    signal shl_ln1118_2_fu_1021_p3 : STD_LOGIC_VECTOR (7 downto 0);
    signal zext_ln1118_6_fu_1028_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal sub_ln1118_2_fu_1032_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal zext_ln1118_5_fu_1018_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal sub_ln1118_3_fu_1038_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal shl_ln728_3_fu_1044_p3 : STD_LOGIC_VECTOR (9 downto 0);
    signal shl_ln1118_3_fu_1056_p3 : STD_LOGIC_VECTOR (7 downto 0);
    signal zext_ln1118_7_fu_1063_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal shl_ln1118_4_fu_1073_p3 : STD_LOGIC_VECTOR (3 downto 0);
    signal sub_ln1118_4_fu_1067_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal zext_ln1118_8_fu_1080_p1 : STD_LOGIC_VECTOR (8 downto 0);
    signal sub_ln1118_5_fu_1084_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal shl_ln728_4_fu_1090_p3 : STD_LOGIC_VECTOR (9 downto 0);
    signal mul_ln1118_1_fu_204_p2 : STD_LOGIC_VECTOR (9 downto 0);
    signal mul_ln728_1_fu_206_p2 : STD_LOGIC_VECTOR (8 downto 0);
    signal sext_ln728_3_fu_1098_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal sext_ln728_2_fu_1052_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_3_fu_1125_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal zext_ln728_1_fu_1118_p1 : STD_LOGIC_VECTOR (10 downto 0);
    signal shl_ln728_5_fu_1106_p3 : STD_LOGIC_VECTOR (10 downto 0);
    signal add_ln703_4_fu_1135_p2 : STD_LOGIC_VECTOR (10 downto 0);
    signal sext_ln703_1_fu_1131_p1 : STD_LOGIC_VECTOR (11 downto 0);
    signal sext_ln703_2_fu_1141_p1 : STD_LOGIC_VECTOR (11 downto 0);
    signal add_ln703_5_fu_1145_p2 : STD_LOGIC_VECTOR (11 downto 0);
    signal sext_ln703_fu_1122_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal sext_ln703_3_fu_1151_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal add_ln703_6_fu_1155_p2 : STD_LOGIC_VECTOR (12 downto 0);
    signal sext_ln703_10_fu_1161_p1 : STD_LOGIC_VECTOR (12 downto 0);
    signal add_ln703_14_fu_1164_p2 : STD_LOGIC_VECTOR (12 downto 0);
    signal ap_ce_reg : STD_LOGIC;
    signal ap_return_int_reg : STD_LOGIC_VECTOR (12 downto 0);
    signal mul_ln1118_1_fu_204_p00 : STD_LOGIC_VECTOR (9 downto 0);
    signal mul_ln1118_2_fu_211_p00 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln1118_fu_200_p00 : STD_LOGIC_VECTOR (9 downto 0);
    signal mul_ln728_1_fu_206_p00 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln728_2_fu_212_p00 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln728_3_fu_201_p00 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln728_4_fu_202_p00 : STD_LOGIC_VECTOR (8 downto 0);
    signal mul_ln728_fu_208_p00 : STD_LOGIC_VECTOR (9 downto 0);


begin




    ap_ce_reg_assign_proc : process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            ap_ce_reg <= ap_ce;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if (((ap_const_logic_1 = ap_const_logic_1) and (ap_const_boolean_0 = ap_block_pp0_stage0_11001))) then
                    add_ln703_13_reg_1197(11 downto 1) <= add_ln703_13_fu_1012_p2(11 downto 1);
                    add_ln703_2_reg_1192(10 downto 1) <= add_ln703_2_fu_946_p2(10 downto 1);
                data_15_V_read_3_reg_1186 <= data_15_V_read;
                data_16_V_read_3_reg_1180 <= data_16_V_read;
                data_20_V_read_3_reg_1175 <= data_20_V_read;
                data_32_V_read_2_reg_1170 <= data_32_V_read;
            end if;
        end if;
    end process;
    process (ap_clk)
    begin
        if (ap_clk'event and ap_clk = '1') then
            if ((ap_const_logic_1 = ap_ce_reg)) then
                    ap_return_int_reg(12 downto 1) <= add_ln703_14_fu_1164_p2(12 downto 1);
            end if;
        end if;
    end process;
    add_ln703_2_reg_1192(0) <= '0';
    add_ln703_13_reg_1197(0) <= '0';
    ap_return_int_reg(0) <= '0';
    add_ln703_10_fu_982_p2 <= std_logic_vector(unsigned(zext_ln728_4_fu_921_p1) + unsigned(shl_ln728_8_fu_901_p3));
    add_ln703_11_fu_992_p2 <= std_logic_vector(unsigned(zext_ln728_5_fu_930_p1) + unsigned(ap_const_lv10_3D8));
    add_ln703_12_fu_1002_p2 <= std_logic_vector(signed(sext_ln703_7_fu_988_p1) + signed(sext_ln703_8_fu_998_p1));
    add_ln703_13_fu_1012_p2 <= std_logic_vector(signed(sext_ln703_6_fu_978_p1) + signed(sext_ln703_9_fu_1008_p1));
    add_ln703_14_fu_1164_p2 <= std_logic_vector(unsigned(add_ln703_6_fu_1155_p2) + unsigned(sext_ln703_10_fu_1161_p1));
    add_ln703_1_fu_940_p2 <= std_logic_vector(unsigned(zext_ln728_fu_784_p1) + unsigned(sext_ln728_1_fu_775_p1));
    add_ln703_2_fu_946_p2 <= std_logic_vector(unsigned(add_ln703_fu_934_p2) + unsigned(add_ln703_1_fu_940_p2));
    add_ln703_3_fu_1125_p2 <= std_logic_vector(signed(sext_ln728_3_fu_1098_p1) + signed(sext_ln728_2_fu_1052_p1));
    add_ln703_4_fu_1135_p2 <= std_logic_vector(unsigned(zext_ln728_1_fu_1118_p1) + unsigned(shl_ln728_5_fu_1106_p3));
    add_ln703_5_fu_1145_p2 <= std_logic_vector(signed(sext_ln703_1_fu_1131_p1) + signed(sext_ln703_2_fu_1141_p1));
    add_ln703_6_fu_1155_p2 <= std_logic_vector(signed(sext_ln703_fu_1122_p1) + signed(sext_ln703_3_fu_1151_p1));
    add_ln703_7_fu_952_p2 <= std_logic_vector(unsigned(zext_ln728_2_fu_831_p1) + unsigned(shl_ln728_6_fu_818_p3));
    add_ln703_8_fu_962_p2 <= std_logic_vector(signed(sext_ln728_4_fu_892_p1) + signed(zext_ln728_3_fu_840_p1));
    add_ln703_9_fu_972_p2 <= std_logic_vector(signed(sext_ln703_4_fu_958_p1) + signed(sext_ln703_5_fu_968_p1));
    add_ln703_fu_934_p2 <= std_logic_vector(signed(sext_ln728_fu_733_p1) + signed(shl_ln_fu_707_p3));
        ap_block_pp0_stage0 <= not((ap_const_boolean_1 = ap_const_boolean_1));
        ap_block_pp0_stage0_11001 <= not((ap_const_boolean_1 = ap_const_boolean_1));
        ap_block_state1_pp0_stage0_iter0 <= not((ap_const_boolean_1 = ap_const_boolean_1));
        ap_block_state2_pp0_stage0_iter1 <= not((ap_const_boolean_1 = ap_const_boolean_1));

    ap_return_assign_proc : process(add_ln703_14_fu_1164_p2, ap_ce_reg, ap_return_int_reg)
    begin
        if ((ap_const_logic_0 = ap_ce_reg)) then 
            ap_return <= ap_return_int_reg;
        elsif ((ap_const_logic_1 = ap_ce_reg)) then 
            ap_return <= add_ln703_14_fu_1164_p2;
        end if; 
    end process;

    mul_ln1118_1_fu_204_p0 <= mul_ln1118_1_fu_204_p00(3 - 1 downto 0);
    mul_ln1118_1_fu_204_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_20_V_read_3_reg_1175),10));
    mul_ln1118_1_fu_204_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(std_logic_vector(signed('0' &mul_ln1118_1_fu_204_p0) * signed(ap_const_lv10_3CF))), 10));
    mul_ln1118_2_fu_211_p0 <= mul_ln1118_2_fu_211_p00(3 - 1 downto 0);
    mul_ln1118_2_fu_211_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_48_V_read),9));
    mul_ln1118_2_fu_211_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(std_logic_vector(signed('0' &mul_ln1118_2_fu_211_p0) * signed(ap_const_lv9_1E3))), 9));
    mul_ln1118_fu_200_p0 <= mul_ln1118_fu_200_p00(3 - 1 downto 0);
    mul_ln1118_fu_200_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_0_V_read),10));
    mul_ln1118_fu_200_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(std_logic_vector(signed('0' &mul_ln1118_fu_200_p0) * signed(ap_const_lv10_3D9))), 10));
    mul_ln728_1_fu_206_p0 <= mul_ln728_1_fu_206_p00(3 - 1 downto 0);
    mul_ln728_1_fu_206_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_32_V_read_2_reg_1170),9));
    mul_ln728_1_fu_206_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_1_fu_206_p0) * unsigned(ap_const_lv9_3A), 9));
    mul_ln728_2_fu_212_p0 <= mul_ln728_2_fu_212_p00(3 - 1 downto 0);
    mul_ln728_2_fu_212_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_43_V_read),9));
    mul_ln728_2_fu_212_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_2_fu_212_p0) * unsigned(ap_const_lv9_32), 9));
    mul_ln728_3_fu_201_p0 <= mul_ln728_3_fu_201_p00(3 - 1 downto 0);
    mul_ln728_3_fu_201_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_45_V_read),9));
    mul_ln728_3_fu_201_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_3_fu_201_p0) * unsigned(ap_const_lv9_34), 9));
    mul_ln728_4_fu_202_p0 <= mul_ln728_4_fu_202_p00(3 - 1 downto 0);
    mul_ln728_4_fu_202_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_58_V_read),9));
    mul_ln728_4_fu_202_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_4_fu_202_p0) * unsigned(ap_const_lv9_2E), 9));
    mul_ln728_fu_208_p0 <= mul_ln728_fu_208_p00(3 - 1 downto 0);
    mul_ln728_fu_208_p00 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_7_V_read),10));
    mul_ln728_fu_208_p2 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_fu_208_p0) * unsigned(ap_const_lv10_64), 10));
        sext_ln1118_fu_862_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(sub_ln1118_7_fu_856_p2),8));

        sext_ln703_10_fu_1161_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_13_reg_1197),13));

        sext_ln703_1_fu_1131_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_3_fu_1125_p2),12));

        sext_ln703_2_fu_1141_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_4_fu_1135_p2),12));

        sext_ln703_3_fu_1151_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_5_fu_1145_p2),13));

        sext_ln703_4_fu_958_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_7_fu_952_p2),11));

        sext_ln703_5_fu_968_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_8_fu_962_p2),11));

        sext_ln703_6_fu_978_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_9_fu_972_p2),12));

        sext_ln703_7_fu_988_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_10_fu_982_p2),11));

        sext_ln703_8_fu_998_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_11_fu_992_p2),11));

        sext_ln703_9_fu_1008_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_12_fu_1002_p2),12));

        sext_ln703_fu_1122_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(add_ln703_2_reg_1192),13));

        sext_ln728_1_fu_775_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(shl_ln728_2_fu_767_p3),11));

        sext_ln728_2_fu_1052_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(shl_ln728_3_fu_1044_p3),11));

        sext_ln728_3_fu_1098_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(shl_ln728_4_fu_1090_p3),11));

        sext_ln728_4_fu_892_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(shl_ln728_7_fu_884_p3),10));

        sext_ln728_fu_733_p1 <= std_logic_vector(IEEE.numeric_std.resize(signed(shl_ln728_1_fu_725_p3),11));

    shl_ln1118_1_fu_749_p3 <= (data_3_V_read & ap_const_lv2_0);
    shl_ln1118_2_fu_1021_p3 <= (data_15_V_read_3_reg_1186 & ap_const_lv5_0);
    shl_ln1118_3_fu_1056_p3 <= (data_16_V_read_3_reg_1180 & ap_const_lv5_0);
    shl_ln1118_4_fu_1073_p3 <= (data_16_V_read_3_reg_1180 & ap_const_lv1_0);
    shl_ln1118_5_fu_788_p3 <= (data_40_V_read & ap_const_lv5_0);
    shl_ln1118_6_fu_800_p3 <= (data_40_V_read & ap_const_lv2_0);
    shl_ln1118_7_fu_844_p3 <= (data_47_V_read & ap_const_lv3_0);
    shl_ln1118_8_fu_866_p3 <= (data_47_V_read & ap_const_lv1_0);
    shl_ln1_fu_737_p3 <= (data_3_V_read & ap_const_lv5_0);
    shl_ln728_1_fu_725_p3 <= (sub_ln1118_fu_719_p2 & ap_const_lv1_0);
    shl_ln728_2_fu_767_p3 <= (sub_ln1118_1_fu_761_p2 & ap_const_lv1_0);
    shl_ln728_3_fu_1044_p3 <= (sub_ln1118_3_fu_1038_p2 & ap_const_lv1_0);
    shl_ln728_4_fu_1090_p3 <= (sub_ln1118_5_fu_1084_p2 & ap_const_lv1_0);
    shl_ln728_5_fu_1106_p3 <= (mul_ln1118_1_fu_204_p2 & ap_const_lv1_0);
    shl_ln728_6_fu_818_p3 <= (sub_ln1118_6_fu_812_p2 & ap_const_lv1_0);
    shl_ln728_7_fu_884_p3 <= (sub_ln1118_8_fu_878_p2 & ap_const_lv1_0);
    shl_ln728_8_fu_901_p3 <= (mul_ln1118_2_fu_211_p2 & ap_const_lv1_0);
    shl_ln728_9_fu_909_p5 <= (((data_53_V_read & ap_const_lv1_0) & data_53_V_read) & ap_const_lv2_0);
    shl_ln_fu_707_p3 <= (mul_ln1118_fu_200_p2 & ap_const_lv1_0);
    sub_ln1118_1_fu_761_p2 <= std_logic_vector(unsigned(zext_ln1118_3_fu_757_p1) - unsigned(zext_ln1118_2_fu_745_p1));
    sub_ln1118_2_fu_1032_p2 <= std_logic_vector(unsigned(ap_const_lv9_0) - unsigned(zext_ln1118_6_fu_1028_p1));
    sub_ln1118_3_fu_1038_p2 <= std_logic_vector(unsigned(sub_ln1118_2_fu_1032_p2) - unsigned(zext_ln1118_5_fu_1018_p1));
    sub_ln1118_4_fu_1067_p2 <= std_logic_vector(unsigned(ap_const_lv9_0) - unsigned(zext_ln1118_7_fu_1063_p1));
    sub_ln1118_5_fu_1084_p2 <= std_logic_vector(unsigned(sub_ln1118_4_fu_1067_p2) - unsigned(zext_ln1118_8_fu_1080_p1));
    sub_ln1118_6_fu_812_p2 <= std_logic_vector(unsigned(zext_ln1118_12_fu_808_p1) - unsigned(zext_ln1118_11_fu_796_p1));
    sub_ln1118_7_fu_856_p2 <= std_logic_vector(unsigned(ap_const_lv7_0) - unsigned(zext_ln1118_15_fu_852_p1));
    sub_ln1118_8_fu_878_p2 <= std_logic_vector(signed(sext_ln1118_fu_862_p1) - signed(zext_ln1118_16_fu_874_p1));
    sub_ln1118_fu_719_p2 <= std_logic_vector(unsigned(ap_const_lv4_0) - unsigned(zext_ln1118_1_fu_715_p1));
    zext_ln1118_11_fu_796_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_5_fu_788_p3),9));
    zext_ln1118_12_fu_808_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_6_fu_800_p3),9));
    zext_ln1118_15_fu_852_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_7_fu_844_p3),7));
    zext_ln1118_16_fu_874_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_8_fu_866_p3),8));
    zext_ln1118_1_fu_715_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_1_V_read),4));
    zext_ln1118_2_fu_745_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1_fu_737_p3),9));
    zext_ln1118_3_fu_757_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_1_fu_749_p3),9));
    zext_ln1118_5_fu_1018_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(data_15_V_read_3_reg_1186),9));
    zext_ln1118_6_fu_1028_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_2_fu_1021_p3),9));
    zext_ln1118_7_fu_1063_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_3_fu_1056_p3),9));
    zext_ln1118_8_fu_1080_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln1118_4_fu_1073_p3),9));
    zext_ln728_1_fu_1118_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_1_fu_206_p2),11));
    zext_ln728_2_fu_831_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_2_fu_212_p2),10));
    zext_ln728_3_fu_840_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_3_fu_201_p2),10));
    zext_ln728_4_fu_921_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(shl_ln728_9_fu_909_p5),10));
    zext_ln728_5_fu_930_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_4_fu_202_p2),10));
    zext_ln728_fu_784_p1 <= std_logic_vector(IEEE.numeric_std.resize(unsigned(mul_ln728_fu_208_p2),11));
end behav;
