// ==============================================================
// RTL generated by Vivado(TM) HLS - High-Level Synthesis from C, C++ and OpenCL
// Version: 2020.1
// Copyright (C) 1986-2020 Xilinx, Inc. All Rights Reserved.
// 
// ===========================================================

`timescale 1 ns / 1 ps 

module dense_latency_ap_ufixed_ap_fixed_config5_0_0 (
        ap_clk,
        ap_rst,
        data_0_V_read,
        data_1_V_read,
        data_3_V_read,
        data_7_V_read,
        data_15_V_read,
        data_16_V_read,
        data_20_V_read,
        data_32_V_read,
        data_40_V_read,
        data_43_V_read,
        data_45_V_read,
        data_47_V_read,
        data_48_V_read,
        data_53_V_read,
        data_58_V_read,
        ap_return,
        ap_ce
);


input   ap_clk;
input   ap_rst;
input  [2:0] data_0_V_read;
input  [2:0] data_1_V_read;
input  [2:0] data_3_V_read;
input  [2:0] data_7_V_read;
input  [2:0] data_15_V_read;
input  [2:0] data_16_V_read;
input  [2:0] data_20_V_read;
input  [2:0] data_32_V_read;
input  [2:0] data_40_V_read;
input  [2:0] data_43_V_read;
input  [2:0] data_45_V_read;
input  [2:0] data_47_V_read;
input  [2:0] data_48_V_read;
input  [2:0] data_53_V_read;
input  [2:0] data_58_V_read;
output  [14:0] ap_return;
input   ap_ce;

reg[14:0] ap_return;

wire   [9:0] mul_ln1118_fu_201_p2;
reg   [9:0] mul_ln1118_reg_725;
wire    ap_block_state1_pp0_stage0_iter0;
wire    ap_block_state2_pp0_stage0_iter1;
wire    ap_block_state3_pp0_stage0_iter2;
wire    ap_block_pp0_stage0_11001;
wire   [8:0] mul_ln728_fu_204_p2;
reg   [8:0] mul_ln728_reg_729;
wire   [9:0] mul_ln1118_1_fu_203_p2;
reg   [9:0] mul_ln1118_1_reg_733;
wire   [7:0] mul_ln728_1_fu_208_p2;
reg   [7:0] mul_ln728_1_reg_737;
wire   [7:0] mul_ln728_2_fu_209_p2;
reg   [7:0] mul_ln728_2_reg_741;
wire   [7:0] mul_ln728_3_fu_212_p2;
reg   [7:0] mul_ln728_3_reg_745;
wire   [8:0] mul_ln1118_2_fu_202_p2;
reg   [8:0] mul_ln1118_2_reg_749;
wire   [7:0] mul_ln728_4_fu_210_p2;
reg   [7:0] mul_ln728_4_reg_753;
reg   [2:0] data_53_V_read_3_reg_1262;
reg   [2:0] data_3_V_read_3_reg_1268;
wire   [3:0] sub_ln1118_fu_767_p2;
reg   [3:0] sub_ln1118_reg_1274;
wire   [8:0] sub_ln1118_6_fu_900_p2;
reg   [8:0] sub_ln1118_6_reg_1279;
wire   [7:0] sub_ln1118_8_fu_950_p2;
reg   [7:0] sub_ln1118_8_reg_1284;
wire   [12:0] add_ln703_3_fu_966_p2;
reg   [12:0] add_ln703_3_reg_1289;
wire   [12:0] add_ln703_2_fu_1151_p2;
reg   [12:0] add_ln703_2_reg_1294;
wire   [13:0] add_ln703_5_fu_1170_p2;
reg   [13:0] add_ln703_5_reg_1299;
wire   [12:0] add_ln703_9_fu_1196_p2;
reg   [12:0] add_ln703_9_reg_1304;
wire   [12:0] add_ln703_12_fu_1222_p2;
reg   [12:0] add_ln703_12_reg_1309;
wire   [2:0] mul_ln1118_fu_201_p0;
wire    ap_block_pp0_stage0;
wire   [2:0] mul_ln1118_2_fu_202_p0;
wire   [2:0] mul_ln1118_1_fu_203_p0;
wire   [2:0] mul_ln728_fu_204_p0;
wire   [2:0] mul_ln728_1_fu_208_p0;
wire   [2:0] mul_ln728_2_fu_209_p0;
wire   [2:0] mul_ln728_4_fu_210_p0;
wire   [2:0] mul_ln728_3_fu_212_p0;
wire   [3:0] zext_ln1118_1_fu_763_p1;
wire   [7:0] shl_ln1118_2_fu_782_p3;
wire   [8:0] zext_ln1118_5_fu_790_p1;
wire   [8:0] sub_ln1118_2_fu_794_p2;
wire   [8:0] zext_ln1118_4_fu_778_p1;
wire   [8:0] sub_ln1118_3_fu_800_p2;
wire   [11:0] shl_ln728_4_fu_806_p3;
wire   [7:0] shl_ln1118_3_fu_818_p3;
wire   [8:0] zext_ln1118_6_fu_826_p1;
wire   [3:0] shl_ln1118_4_fu_836_p3;
wire   [8:0] sub_ln1118_4_fu_830_p2;
wire   [8:0] zext_ln1118_7_fu_844_p1;
wire   [8:0] sub_ln1118_5_fu_848_p2;
wire   [11:0] shl_ln728_5_fu_854_p3;
wire   [7:0] shl_ln1118_5_fu_876_p3;
wire   [4:0] shl_ln1118_6_fu_888_p3;
wire   [8:0] zext_ln1118_10_fu_896_p1;
wire   [8:0] zext_ln1118_9_fu_884_p1;
wire   [5:0] shl_ln1118_7_fu_916_p3;
wire   [6:0] zext_ln1118_11_fu_924_p1;
wire   [6:0] sub_ln1118_7_fu_928_p2;
wire   [3:0] shl_ln1118_8_fu_938_p3;
wire  signed [7:0] sext_ln1118_fu_934_p1;
wire   [7:0] zext_ln1118_12_fu_946_p1;
wire  signed [12:0] sext_ln728_3_fu_862_p1;
wire  signed [12:0] sext_ln728_2_fu_814_p1;
wire   [6:0] shl_ln728_1_fu_980_p3;
wire   [7:0] shl_ln1_fu_991_p3;
wire   [4:0] shl_ln1118_1_fu_1002_p3;
wire   [8:0] zext_ln1118_3_fu_1009_p1;
wire   [8:0] zext_ln1118_2_fu_998_p1;
wire   [8:0] sub_ln1118_1_fu_1013_p2;
wire   [11:0] shl_ln728_2_fu_1019_p3;
wire   [11:0] shl_ln728_3_fu_1031_p3;
wire   [10:0] shl_ln728_7_fu_1051_p3;
wire   [10:0] shl_ln728_9_fu_1070_p3;
wire   [10:0] shl_ln728_s_fu_1082_p3;
wire   [10:0] shl_ln728_10_fu_1094_p3;
wire   [10:0] shl_ln728_12_fu_1113_p5;
wire   [10:0] shl_ln728_13_fu_1127_p3;
wire  signed [12:0] sext_ln728_fu_987_p1;
wire   [12:0] shl_ln_fu_972_p3;
wire   [12:0] zext_ln728_1_fu_1039_p1;
wire  signed [12:0] sext_ln728_1_fu_1027_p1;
wire   [12:0] add_ln703_fu_1139_p2;
wire   [12:0] add_ln703_1_fu_1145_p2;
wire   [12:0] zext_ln728_3_fu_1059_p1;
wire   [12:0] shl_ln728_6_fu_1043_p3;
wire   [12:0] add_ln703_4_fu_1160_p2;
wire  signed [13:0] sext_ln703_1_fu_1157_p1;
wire  signed [13:0] sext_ln703_2_fu_1166_p1;
wire   [11:0] zext_ln728_5_fu_1078_p1;
wire   [11:0] shl_ln728_8_fu_1063_p3;
wire   [11:0] add_ln703_7_fu_1176_p2;
wire  signed [11:0] sext_ln728_4_fu_1101_p1;
wire   [11:0] zext_ln728_7_fu_1090_p1;
wire   [11:0] add_ln703_8_fu_1186_p2;
wire  signed [12:0] sext_ln703_4_fu_1182_p1;
wire  signed [12:0] sext_ln703_5_fu_1192_p1;
wire   [11:0] zext_ln728_8_fu_1123_p1;
wire   [11:0] shl_ln728_11_fu_1105_p3;
wire   [11:0] add_ln703_10_fu_1202_p2;
wire   [11:0] zext_ln728_10_fu_1135_p1;
wire   [11:0] add_ln703_11_fu_1212_p2;
wire  signed [12:0] sext_ln703_7_fu_1208_p1;
wire  signed [12:0] sext_ln703_8_fu_1218_p1;
wire  signed [14:0] sext_ln703_fu_1228_p1;
wire  signed [14:0] sext_ln703_3_fu_1231_p1;
wire  signed [13:0] sext_ln703_6_fu_1240_p1;
wire  signed [13:0] sext_ln703_9_fu_1243_p1;
wire   [13:0] add_ln703_13_fu_1246_p2;
wire   [14:0] add_ln703_6_fu_1234_p2;
wire  signed [14:0] sext_ln703_10_fu_1252_p1;
wire   [14:0] add_ln703_14_fu_1256_p2;
reg    ap_ce_reg;
reg   [2:0] data_0_V_read_int_reg;
reg   [2:0] data_1_V_read_int_reg;
reg   [2:0] data_3_V_read_int_reg;
reg   [2:0] data_7_V_read_int_reg;
reg   [2:0] data_15_V_read_int_reg;
reg   [2:0] data_16_V_read_int_reg;
reg   [2:0] data_20_V_read_int_reg;
reg   [2:0] data_32_V_read_int_reg;
reg   [2:0] data_40_V_read_int_reg;
reg   [2:0] data_43_V_read_int_reg;
reg   [2:0] data_45_V_read_int_reg;
reg   [2:0] data_47_V_read_int_reg;
reg   [2:0] data_48_V_read_int_reg;
reg   [2:0] data_53_V_read_int_reg;
reg   [2:0] data_58_V_read_int_reg;
reg   [14:0] ap_return_int_reg;
wire   [9:0] mul_ln1118_1_fu_203_p00;
wire   [8:0] mul_ln1118_2_fu_202_p00;
wire   [9:0] mul_ln1118_fu_201_p00;
wire   [7:0] mul_ln728_1_fu_208_p00;
wire   [7:0] mul_ln728_2_fu_209_p00;
wire   [7:0] mul_ln728_3_fu_212_p00;
wire   [7:0] mul_ln728_4_fu_210_p00;
wire   [8:0] mul_ln728_fu_204_p00;

always @ (posedge ap_clk) begin
    ap_ce_reg <= ap_ce;
end

always @ (posedge ap_clk) begin
    if (((1'b1 == ap_ce_reg) & (1'b0 == ap_block_pp0_stage0_11001))) begin
        add_ln703_12_reg_1309[12 : 3] <= add_ln703_12_fu_1222_p2[12 : 3];
        add_ln703_2_reg_1294[12 : 3] <= add_ln703_2_fu_1151_p2[12 : 3];
        add_ln703_3_reg_1289[12 : 3] <= add_ln703_3_fu_966_p2[12 : 3];
        add_ln703_5_reg_1299[13 : 3] <= add_ln703_5_fu_1170_p2[13 : 3];
        add_ln703_9_reg_1304[12 : 3] <= add_ln703_9_fu_1196_p2[12 : 3];
        data_3_V_read_3_reg_1268 <= data_3_V_read_int_reg;
        data_53_V_read_3_reg_1262 <= data_53_V_read_int_reg;
        mul_ln1118_1_reg_733 <= mul_ln1118_1_fu_203_p2;
        mul_ln1118_2_reg_749 <= mul_ln1118_2_fu_202_p2;
        mul_ln1118_reg_725 <= mul_ln1118_fu_201_p2;
        mul_ln728_1_reg_737 <= mul_ln728_1_fu_208_p2;
        mul_ln728_2_reg_741 <= mul_ln728_2_fu_209_p2;
        mul_ln728_3_reg_745[7 : 1] <= mul_ln728_3_fu_212_p2[7 : 1];
        mul_ln728_4_reg_753 <= mul_ln728_4_fu_210_p2;
        mul_ln728_reg_729[8 : 1] <= mul_ln728_fu_204_p2[8 : 1];
        sub_ln1118_6_reg_1279[8 : 2] <= sub_ln1118_6_fu_900_p2[8 : 2];
        sub_ln1118_8_reg_1284[7 : 1] <= sub_ln1118_8_fu_950_p2[7 : 1];
        sub_ln1118_reg_1274 <= sub_ln1118_fu_767_p2;
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_ce_reg)) begin
        ap_return_int_reg[14 : 3] <= add_ln703_14_fu_1256_p2[14 : 3];
    end
end

always @ (posedge ap_clk) begin
    if ((1'b1 == ap_ce)) begin
        data_0_V_read_int_reg <= data_0_V_read;
        data_15_V_read_int_reg <= data_15_V_read;
        data_16_V_read_int_reg <= data_16_V_read;
        data_1_V_read_int_reg <= data_1_V_read;
        data_20_V_read_int_reg <= data_20_V_read;
        data_32_V_read_int_reg <= data_32_V_read;
        data_3_V_read_int_reg <= data_3_V_read;
        data_40_V_read_int_reg <= data_40_V_read;
        data_43_V_read_int_reg <= data_43_V_read;
        data_45_V_read_int_reg <= data_45_V_read;
        data_47_V_read_int_reg <= data_47_V_read;
        data_48_V_read_int_reg <= data_48_V_read;
        data_53_V_read_int_reg <= data_53_V_read;
        data_58_V_read_int_reg <= data_58_V_read;
        data_7_V_read_int_reg <= data_7_V_read;
    end
end

always @ (*) begin
    if ((1'b0 == ap_ce_reg)) begin
        ap_return = ap_return_int_reg;
    end else if ((1'b1 == ap_ce_reg)) begin
        ap_return = add_ln703_14_fu_1256_p2;
    end
end

assign add_ln703_10_fu_1202_p2 = (zext_ln728_8_fu_1123_p1 + shl_ln728_11_fu_1105_p3);

assign add_ln703_11_fu_1212_p2 = ($signed(zext_ln728_10_fu_1135_p1) + $signed(12'd3936));

assign add_ln703_12_fu_1222_p2 = ($signed(sext_ln703_7_fu_1208_p1) + $signed(sext_ln703_8_fu_1218_p1));

assign add_ln703_13_fu_1246_p2 = ($signed(sext_ln703_6_fu_1240_p1) + $signed(sext_ln703_9_fu_1243_p1));

assign add_ln703_14_fu_1256_p2 = ($signed(add_ln703_6_fu_1234_p2) + $signed(sext_ln703_10_fu_1252_p1));

assign add_ln703_1_fu_1145_p2 = ($signed(zext_ln728_1_fu_1039_p1) + $signed(sext_ln728_1_fu_1027_p1));

assign add_ln703_2_fu_1151_p2 = (add_ln703_fu_1139_p2 + add_ln703_1_fu_1145_p2);

assign add_ln703_3_fu_966_p2 = ($signed(sext_ln728_3_fu_862_p1) + $signed(sext_ln728_2_fu_814_p1));

assign add_ln703_4_fu_1160_p2 = (zext_ln728_3_fu_1059_p1 + shl_ln728_6_fu_1043_p3);

assign add_ln703_5_fu_1170_p2 = ($signed(sext_ln703_1_fu_1157_p1) + $signed(sext_ln703_2_fu_1166_p1));

assign add_ln703_6_fu_1234_p2 = ($signed(sext_ln703_fu_1228_p1) + $signed(sext_ln703_3_fu_1231_p1));

assign add_ln703_7_fu_1176_p2 = (zext_ln728_5_fu_1078_p1 + shl_ln728_8_fu_1063_p3);

assign add_ln703_8_fu_1186_p2 = ($signed(sext_ln728_4_fu_1101_p1) + $signed(zext_ln728_7_fu_1090_p1));

assign add_ln703_9_fu_1196_p2 = ($signed(sext_ln703_4_fu_1182_p1) + $signed(sext_ln703_5_fu_1192_p1));

assign add_ln703_fu_1139_p2 = ($signed(sext_ln728_fu_987_p1) + $signed(shl_ln_fu_972_p3));

assign ap_block_pp0_stage0 = ~(1'b1 == 1'b1);

assign ap_block_pp0_stage0_11001 = ~(1'b1 == 1'b1);

assign ap_block_state1_pp0_stage0_iter0 = ~(1'b1 == 1'b1);

assign ap_block_state2_pp0_stage0_iter1 = ~(1'b1 == 1'b1);

assign ap_block_state3_pp0_stage0_iter2 = ~(1'b1 == 1'b1);

assign mul_ln1118_1_fu_203_p0 = mul_ln1118_1_fu_203_p00;

assign mul_ln1118_1_fu_203_p00 = data_20_V_read_int_reg;

assign mul_ln1118_1_fu_203_p2 = ($signed({{1'b0}, {mul_ln1118_1_fu_203_p0}}) * $signed(-'h31));

assign mul_ln1118_2_fu_202_p0 = mul_ln1118_2_fu_202_p00;

assign mul_ln1118_2_fu_202_p00 = data_48_V_read_int_reg;

assign mul_ln1118_2_fu_202_p2 = ($signed({{1'b0}, {mul_ln1118_2_fu_202_p0}}) * $signed(-'h1D));

assign mul_ln1118_fu_201_p0 = mul_ln1118_fu_201_p00;

assign mul_ln1118_fu_201_p00 = data_0_V_read_int_reg;

assign mul_ln1118_fu_201_p2 = ($signed({{1'b0}, {mul_ln1118_fu_201_p0}}) * $signed(-'h27));

assign mul_ln728_1_fu_208_p0 = mul_ln728_1_fu_208_p00;

assign mul_ln728_1_fu_208_p00 = data_32_V_read_int_reg;

assign mul_ln728_1_fu_208_p2 = (mul_ln728_1_fu_208_p0 * $signed('h1D));

assign mul_ln728_2_fu_209_p0 = mul_ln728_2_fu_209_p00;

assign mul_ln728_2_fu_209_p00 = data_43_V_read_int_reg;

assign mul_ln728_2_fu_209_p2 = (mul_ln728_2_fu_209_p0 * $signed('h19));

assign mul_ln728_3_fu_212_p0 = mul_ln728_3_fu_212_p00;

assign mul_ln728_3_fu_212_p00 = data_45_V_read_int_reg;

assign mul_ln728_3_fu_212_p2 = (mul_ln728_3_fu_212_p0 * $signed('h1A));

assign mul_ln728_4_fu_210_p0 = mul_ln728_4_fu_210_p00;

assign mul_ln728_4_fu_210_p00 = data_58_V_read_int_reg;

assign mul_ln728_4_fu_210_p2 = (mul_ln728_4_fu_210_p0 * $signed('h17));

assign mul_ln728_fu_204_p0 = mul_ln728_fu_204_p00;

assign mul_ln728_fu_204_p00 = data_7_V_read_int_reg;

assign mul_ln728_fu_204_p2 = (mul_ln728_fu_204_p0 * $signed('h32));

assign sext_ln1118_fu_934_p1 = $signed(sub_ln1118_7_fu_928_p2);

assign sext_ln703_10_fu_1252_p1 = $signed(add_ln703_13_fu_1246_p2);

assign sext_ln703_1_fu_1157_p1 = $signed(add_ln703_3_reg_1289);

assign sext_ln703_2_fu_1166_p1 = $signed(add_ln703_4_fu_1160_p2);

assign sext_ln703_3_fu_1231_p1 = $signed(add_ln703_5_reg_1299);

assign sext_ln703_4_fu_1182_p1 = $signed(add_ln703_7_fu_1176_p2);

assign sext_ln703_5_fu_1192_p1 = $signed(add_ln703_8_fu_1186_p2);

assign sext_ln703_6_fu_1240_p1 = $signed(add_ln703_9_reg_1304);

assign sext_ln703_7_fu_1208_p1 = $signed(add_ln703_10_fu_1202_p2);

assign sext_ln703_8_fu_1218_p1 = $signed(add_ln703_11_fu_1212_p2);

assign sext_ln703_9_fu_1243_p1 = $signed(add_ln703_12_reg_1309);

assign sext_ln703_fu_1228_p1 = $signed(add_ln703_2_reg_1294);

assign sext_ln728_1_fu_1027_p1 = $signed(shl_ln728_2_fu_1019_p3);

assign sext_ln728_2_fu_814_p1 = $signed(shl_ln728_4_fu_806_p3);

assign sext_ln728_3_fu_862_p1 = $signed(shl_ln728_5_fu_854_p3);

assign sext_ln728_4_fu_1101_p1 = $signed(shl_ln728_10_fu_1094_p3);

assign sext_ln728_fu_987_p1 = $signed(shl_ln728_1_fu_980_p3);

assign shl_ln1118_1_fu_1002_p3 = {{data_3_V_read_3_reg_1268}, {2'd0}};

assign shl_ln1118_2_fu_782_p3 = {{data_15_V_read_int_reg}, {5'd0}};

assign shl_ln1118_3_fu_818_p3 = {{data_16_V_read_int_reg}, {5'd0}};

assign shl_ln1118_4_fu_836_p3 = {{data_16_V_read_int_reg}, {1'd0}};

assign shl_ln1118_5_fu_876_p3 = {{data_40_V_read_int_reg}, {5'd0}};

assign shl_ln1118_6_fu_888_p3 = {{data_40_V_read_int_reg}, {2'd0}};

assign shl_ln1118_7_fu_916_p3 = {{data_47_V_read_int_reg}, {3'd0}};

assign shl_ln1118_8_fu_938_p3 = {{data_47_V_read_int_reg}, {1'd0}};

assign shl_ln1_fu_991_p3 = {{data_3_V_read_3_reg_1268}, {5'd0}};

assign shl_ln728_10_fu_1094_p3 = {{sub_ln1118_8_reg_1284}, {3'd0}};

assign shl_ln728_11_fu_1105_p3 = {{mul_ln1118_2_reg_749}, {3'd0}};

assign shl_ln728_12_fu_1113_p5 = {{{{data_53_V_read_3_reg_1262}, {1'd0}}, {data_53_V_read_3_reg_1262}}, {4'd0}};

assign shl_ln728_13_fu_1127_p3 = {{mul_ln728_4_reg_753}, {3'd0}};

assign shl_ln728_1_fu_980_p3 = {{sub_ln1118_reg_1274}, {3'd0}};

assign shl_ln728_2_fu_1019_p3 = {{sub_ln1118_1_fu_1013_p2}, {3'd0}};

assign shl_ln728_3_fu_1031_p3 = {{mul_ln728_reg_729}, {3'd0}};

assign shl_ln728_4_fu_806_p3 = {{sub_ln1118_3_fu_800_p2}, {3'd0}};

assign shl_ln728_5_fu_854_p3 = {{sub_ln1118_5_fu_848_p2}, {3'd0}};

assign shl_ln728_6_fu_1043_p3 = {{mul_ln1118_1_reg_733}, {3'd0}};

assign shl_ln728_7_fu_1051_p3 = {{mul_ln728_1_reg_737}, {3'd0}};

assign shl_ln728_8_fu_1063_p3 = {{sub_ln1118_6_reg_1279}, {3'd0}};

assign shl_ln728_9_fu_1070_p3 = {{mul_ln728_2_reg_741}, {3'd0}};

assign shl_ln728_s_fu_1082_p3 = {{mul_ln728_3_reg_745}, {3'd0}};

assign shl_ln_fu_972_p3 = {{mul_ln1118_reg_725}, {3'd0}};

assign sub_ln1118_1_fu_1013_p2 = (zext_ln1118_3_fu_1009_p1 - zext_ln1118_2_fu_998_p1);

assign sub_ln1118_2_fu_794_p2 = (9'd0 - zext_ln1118_5_fu_790_p1);

assign sub_ln1118_3_fu_800_p2 = (sub_ln1118_2_fu_794_p2 - zext_ln1118_4_fu_778_p1);

assign sub_ln1118_4_fu_830_p2 = (9'd0 - zext_ln1118_6_fu_826_p1);

assign sub_ln1118_5_fu_848_p2 = (sub_ln1118_4_fu_830_p2 - zext_ln1118_7_fu_844_p1);

assign sub_ln1118_6_fu_900_p2 = (zext_ln1118_10_fu_896_p1 - zext_ln1118_9_fu_884_p1);

assign sub_ln1118_7_fu_928_p2 = (7'd0 - zext_ln1118_11_fu_924_p1);

assign sub_ln1118_8_fu_950_p2 = ($signed(sext_ln1118_fu_934_p1) - $signed(zext_ln1118_12_fu_946_p1));

assign sub_ln1118_fu_767_p2 = (4'd0 - zext_ln1118_1_fu_763_p1);

assign zext_ln1118_10_fu_896_p1 = shl_ln1118_6_fu_888_p3;

assign zext_ln1118_11_fu_924_p1 = shl_ln1118_7_fu_916_p3;

assign zext_ln1118_12_fu_946_p1 = shl_ln1118_8_fu_938_p3;

assign zext_ln1118_1_fu_763_p1 = data_1_V_read_int_reg;

assign zext_ln1118_2_fu_998_p1 = shl_ln1_fu_991_p3;

assign zext_ln1118_3_fu_1009_p1 = shl_ln1118_1_fu_1002_p3;

assign zext_ln1118_4_fu_778_p1 = data_15_V_read_int_reg;

assign zext_ln1118_5_fu_790_p1 = shl_ln1118_2_fu_782_p3;

assign zext_ln1118_6_fu_826_p1 = shl_ln1118_3_fu_818_p3;

assign zext_ln1118_7_fu_844_p1 = shl_ln1118_4_fu_836_p3;

assign zext_ln1118_9_fu_884_p1 = shl_ln1118_5_fu_876_p3;

assign zext_ln728_10_fu_1135_p1 = shl_ln728_13_fu_1127_p3;

assign zext_ln728_1_fu_1039_p1 = shl_ln728_3_fu_1031_p3;

assign zext_ln728_3_fu_1059_p1 = shl_ln728_7_fu_1051_p3;

assign zext_ln728_5_fu_1078_p1 = shl_ln728_9_fu_1070_p3;

assign zext_ln728_7_fu_1090_p1 = shl_ln728_s_fu_1082_p3;

assign zext_ln728_8_fu_1123_p1 = shl_ln728_12_fu_1113_p5;

always @ (posedge ap_clk) begin
    mul_ln728_reg_729[0] <= 1'b0;
    mul_ln728_3_reg_745[0] <= 1'b0;
    sub_ln1118_6_reg_1279[1:0] <= 2'b00;
    sub_ln1118_8_reg_1284[0] <= 1'b0;
    add_ln703_3_reg_1289[2:0] <= 3'b000;
    add_ln703_2_reg_1294[2:0] <= 3'b000;
    add_ln703_5_reg_1299[2:0] <= 3'b000;
    add_ln703_9_reg_1304[2:0] <= 3'b000;
    add_ln703_12_reg_1309[2:0] <= 3'b000;
    ap_return_int_reg[2:0] <= 3'b000;
end

endmodule //dense_latency_ap_ufixed_ap_fixed_config5_0_0
