#ifndef DEFINES_H_
#define DEFINES_H_

#include "ap_int.h"
#include "ap_fixed.h"
#include "nnet_utils/nnet_types.h"
#include <cstddef>
#include <cstdio>

//hls-fpga-machine-learning insert numbers
#define N_INPUT_1_1 26
#define N_LAYER_2 64
#define N_LAYER_5 32
#define N_LAYER_8 1

//hls-fpga-machine-learning insert layer-precision
typedef ap_fixed<16,6> model_default_t;
typedef ap_fixed<16,6> input_t;
typedef ap_fixed<16,6> layer2_t;
typedef ap_fixed<5,1> weight2_t;
typedef ap_fixed<6,1> bias2_t;
typedef ap_ufixed<3,1> layer4_t;
typedef ap_fixed<16,6> layer5_t;
typedef ap_fixed<5,1> weight5_t;
typedef ap_fixed<6,1> bias5_t;
typedef ap_ufixed<3,1> layer7_t;
typedef ap_fixed<16,6> layer8_t;
typedef ap_fixed<6,6> weight8_t;
typedef ap_fixed<4,3> bias8_t;
typedef ap_fixed<2,1> binary_default_t;
typedef ap_fixed<2,1> result_t;

#endif
