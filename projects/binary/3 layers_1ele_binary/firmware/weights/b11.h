//Numpy array shape [1]
//Min 0.000000000000
//Max 0.000000000000
//Number of zeros 1

#ifndef B11_H_
#define B11_H_

#ifndef __SYNTHESIS__
bias11_t b11[1];
#else
bias11_t b11[1] = {0.0};
#endif

#endif
