//Numpy array shape [1]
//Min -0.500000000000
//Max -0.500000000000
//Number of zeros 0

#ifndef B8_H_
#define B8_H_

#ifndef __SYNTHESIS__
bias8_t b8[1];
#else
bias8_t b8[1] = {-0.5};
#endif

#endif
